package com.group3.finmansys.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.group3.finmansys.IntegrationTest;
import com.group3.finmansys.domain.LoaiQuy;
import com.group3.finmansys.repository.LoaiQuyRepository;
import com.group3.finmansys.service.criteria.LoaiQuyCriteria;
import com.group3.finmansys.service.dto.LoaiQuyDTO;
import com.group3.finmansys.service.mapper.LoaiQuyMapper;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link LoaiQuyResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class LoaiQuyResourceIT {

    private static final String DEFAULT_MA = "AAAAAAAAAA";
    private static final String UPDATED_MA = "BBBBBBBBBB";

    private static final String DEFAULT_TEN = "AAAAAAAAAA";
    private static final String UPDATED_TEN = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/loai-quies";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private LoaiQuyRepository loaiQuyRepository;

    @Autowired
    private LoaiQuyMapper loaiQuyMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restLoaiQuyMockMvc;

    private LoaiQuy loaiQuy;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static LoaiQuy createEntity(EntityManager em) {
        LoaiQuy loaiQuy = new LoaiQuy().ma(DEFAULT_MA).ten(DEFAULT_TEN);
        return loaiQuy;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static LoaiQuy createUpdatedEntity(EntityManager em) {
        LoaiQuy loaiQuy = new LoaiQuy().ma(UPDATED_MA).ten(UPDATED_TEN);
        return loaiQuy;
    }

    @BeforeEach
    public void initTest() {
        loaiQuy = createEntity(em);
    }

    @Test
    @Transactional
    void createLoaiQuy() throws Exception {
        int databaseSizeBeforeCreate = loaiQuyRepository.findAll().size();
        // Create the LoaiQuy
        LoaiQuyDTO loaiQuyDTO = loaiQuyMapper.toDto(loaiQuy);
        restLoaiQuyMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(loaiQuyDTO)))
            .andExpect(status().isCreated());

        // Validate the LoaiQuy in the database
        List<LoaiQuy> loaiQuyList = loaiQuyRepository.findAll();
        assertThat(loaiQuyList).hasSize(databaseSizeBeforeCreate + 1);
        LoaiQuy testLoaiQuy = loaiQuyList.get(loaiQuyList.size() - 1);
        assertThat(testLoaiQuy.getMa()).isEqualTo(DEFAULT_MA);
        assertThat(testLoaiQuy.getTen()).isEqualTo(DEFAULT_TEN);
    }

    @Test
    @Transactional
    void createLoaiQuyWithExistingId() throws Exception {
        // Create the LoaiQuy with an existing ID
        loaiQuy.setId(1L);
        LoaiQuyDTO loaiQuyDTO = loaiQuyMapper.toDto(loaiQuy);

        int databaseSizeBeforeCreate = loaiQuyRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restLoaiQuyMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(loaiQuyDTO)))
            .andExpect(status().isBadRequest());

        // Validate the LoaiQuy in the database
        List<LoaiQuy> loaiQuyList = loaiQuyRepository.findAll();
        assertThat(loaiQuyList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkMaIsRequired() throws Exception {
        int databaseSizeBeforeTest = loaiQuyRepository.findAll().size();
        // set the field null
        loaiQuy.setMa(null);

        // Create the LoaiQuy, which fails.
        LoaiQuyDTO loaiQuyDTO = loaiQuyMapper.toDto(loaiQuy);

        restLoaiQuyMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(loaiQuyDTO)))
            .andExpect(status().isBadRequest());

        List<LoaiQuy> loaiQuyList = loaiQuyRepository.findAll();
        assertThat(loaiQuyList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkTenIsRequired() throws Exception {
        int databaseSizeBeforeTest = loaiQuyRepository.findAll().size();
        // set the field null
        loaiQuy.setTen(null);

        // Create the LoaiQuy, which fails.
        LoaiQuyDTO loaiQuyDTO = loaiQuyMapper.toDto(loaiQuy);

        restLoaiQuyMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(loaiQuyDTO)))
            .andExpect(status().isBadRequest());

        List<LoaiQuy> loaiQuyList = loaiQuyRepository.findAll();
        assertThat(loaiQuyList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllLoaiQuies() throws Exception {
        // Initialize the database
        loaiQuyRepository.saveAndFlush(loaiQuy);

        // Get all the loaiQuyList
        restLoaiQuyMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(loaiQuy.getId().intValue())))
            .andExpect(jsonPath("$.[*].ma").value(hasItem(DEFAULT_MA)))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)));
    }

    @Test
    @Transactional
    void getLoaiQuy() throws Exception {
        // Initialize the database
        loaiQuyRepository.saveAndFlush(loaiQuy);

        // Get the loaiQuy
        restLoaiQuyMockMvc
            .perform(get(ENTITY_API_URL_ID, loaiQuy.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(loaiQuy.getId().intValue()))
            .andExpect(jsonPath("$.ma").value(DEFAULT_MA))
            .andExpect(jsonPath("$.ten").value(DEFAULT_TEN));
    }

    @Test
    @Transactional
    void getLoaiQuiesByIdFiltering() throws Exception {
        // Initialize the database
        loaiQuyRepository.saveAndFlush(loaiQuy);

        Long id = loaiQuy.getId();

        defaultLoaiQuyShouldBeFound("id.equals=" + id);
        defaultLoaiQuyShouldNotBeFound("id.notEquals=" + id);

        defaultLoaiQuyShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultLoaiQuyShouldNotBeFound("id.greaterThan=" + id);

        defaultLoaiQuyShouldBeFound("id.lessThanOrEqual=" + id);
        defaultLoaiQuyShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllLoaiQuiesByMaIsEqualToSomething() throws Exception {
        // Initialize the database
        loaiQuyRepository.saveAndFlush(loaiQuy);

        // Get all the loaiQuyList where ma equals to DEFAULT_MA
        defaultLoaiQuyShouldBeFound("ma.equals=" + DEFAULT_MA);

        // Get all the loaiQuyList where ma equals to UPDATED_MA
        defaultLoaiQuyShouldNotBeFound("ma.equals=" + UPDATED_MA);
    }

    @Test
    @Transactional
    void getAllLoaiQuiesByMaIsNotEqualToSomething() throws Exception {
        // Initialize the database
        loaiQuyRepository.saveAndFlush(loaiQuy);

        // Get all the loaiQuyList where ma not equals to DEFAULT_MA
        defaultLoaiQuyShouldNotBeFound("ma.notEquals=" + DEFAULT_MA);

        // Get all the loaiQuyList where ma not equals to UPDATED_MA
        defaultLoaiQuyShouldBeFound("ma.notEquals=" + UPDATED_MA);
    }

    @Test
    @Transactional
    void getAllLoaiQuiesByMaIsInShouldWork() throws Exception {
        // Initialize the database
        loaiQuyRepository.saveAndFlush(loaiQuy);

        // Get all the loaiQuyList where ma in DEFAULT_MA or UPDATED_MA
        defaultLoaiQuyShouldBeFound("ma.in=" + DEFAULT_MA + "," + UPDATED_MA);

        // Get all the loaiQuyList where ma equals to UPDATED_MA
        defaultLoaiQuyShouldNotBeFound("ma.in=" + UPDATED_MA);
    }

    @Test
    @Transactional
    void getAllLoaiQuiesByMaIsNullOrNotNull() throws Exception {
        // Initialize the database
        loaiQuyRepository.saveAndFlush(loaiQuy);

        // Get all the loaiQuyList where ma is not null
        defaultLoaiQuyShouldBeFound("ma.specified=true");

        // Get all the loaiQuyList where ma is null
        defaultLoaiQuyShouldNotBeFound("ma.specified=false");
    }

    @Test
    @Transactional
    void getAllLoaiQuiesByMaContainsSomething() throws Exception {
        // Initialize the database
        loaiQuyRepository.saveAndFlush(loaiQuy);

        // Get all the loaiQuyList where ma contains DEFAULT_MA
        defaultLoaiQuyShouldBeFound("ma.contains=" + DEFAULT_MA);

        // Get all the loaiQuyList where ma contains UPDATED_MA
        defaultLoaiQuyShouldNotBeFound("ma.contains=" + UPDATED_MA);
    }

    @Test
    @Transactional
    void getAllLoaiQuiesByMaNotContainsSomething() throws Exception {
        // Initialize the database
        loaiQuyRepository.saveAndFlush(loaiQuy);

        // Get all the loaiQuyList where ma does not contain DEFAULT_MA
        defaultLoaiQuyShouldNotBeFound("ma.doesNotContain=" + DEFAULT_MA);

        // Get all the loaiQuyList where ma does not contain UPDATED_MA
        defaultLoaiQuyShouldBeFound("ma.doesNotContain=" + UPDATED_MA);
    }

    @Test
    @Transactional
    void getAllLoaiQuiesByTenIsEqualToSomething() throws Exception {
        // Initialize the database
        loaiQuyRepository.saveAndFlush(loaiQuy);

        // Get all the loaiQuyList where ten equals to DEFAULT_TEN
        defaultLoaiQuyShouldBeFound("ten.equals=" + DEFAULT_TEN);

        // Get all the loaiQuyList where ten equals to UPDATED_TEN
        defaultLoaiQuyShouldNotBeFound("ten.equals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    void getAllLoaiQuiesByTenIsNotEqualToSomething() throws Exception {
        // Initialize the database
        loaiQuyRepository.saveAndFlush(loaiQuy);

        // Get all the loaiQuyList where ten not equals to DEFAULT_TEN
        defaultLoaiQuyShouldNotBeFound("ten.notEquals=" + DEFAULT_TEN);

        // Get all the loaiQuyList where ten not equals to UPDATED_TEN
        defaultLoaiQuyShouldBeFound("ten.notEquals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    void getAllLoaiQuiesByTenIsInShouldWork() throws Exception {
        // Initialize the database
        loaiQuyRepository.saveAndFlush(loaiQuy);

        // Get all the loaiQuyList where ten in DEFAULT_TEN or UPDATED_TEN
        defaultLoaiQuyShouldBeFound("ten.in=" + DEFAULT_TEN + "," + UPDATED_TEN);

        // Get all the loaiQuyList where ten equals to UPDATED_TEN
        defaultLoaiQuyShouldNotBeFound("ten.in=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    void getAllLoaiQuiesByTenIsNullOrNotNull() throws Exception {
        // Initialize the database
        loaiQuyRepository.saveAndFlush(loaiQuy);

        // Get all the loaiQuyList where ten is not null
        defaultLoaiQuyShouldBeFound("ten.specified=true");

        // Get all the loaiQuyList where ten is null
        defaultLoaiQuyShouldNotBeFound("ten.specified=false");
    }

    @Test
    @Transactional
    void getAllLoaiQuiesByTenContainsSomething() throws Exception {
        // Initialize the database
        loaiQuyRepository.saveAndFlush(loaiQuy);

        // Get all the loaiQuyList where ten contains DEFAULT_TEN
        defaultLoaiQuyShouldBeFound("ten.contains=" + DEFAULT_TEN);

        // Get all the loaiQuyList where ten contains UPDATED_TEN
        defaultLoaiQuyShouldNotBeFound("ten.contains=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    void getAllLoaiQuiesByTenNotContainsSomething() throws Exception {
        // Initialize the database
        loaiQuyRepository.saveAndFlush(loaiQuy);

        // Get all the loaiQuyList where ten does not contain DEFAULT_TEN
        defaultLoaiQuyShouldNotBeFound("ten.doesNotContain=" + DEFAULT_TEN);

        // Get all the loaiQuyList where ten does not contain UPDATED_TEN
        defaultLoaiQuyShouldBeFound("ten.doesNotContain=" + UPDATED_TEN);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultLoaiQuyShouldBeFound(String filter) throws Exception {
        restLoaiQuyMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(loaiQuy.getId().intValue())))
            .andExpect(jsonPath("$.[*].ma").value(hasItem(DEFAULT_MA)))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)));

        // Check, that the count call also returns 1
        restLoaiQuyMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultLoaiQuyShouldNotBeFound(String filter) throws Exception {
        restLoaiQuyMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restLoaiQuyMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingLoaiQuy() throws Exception {
        // Get the loaiQuy
        restLoaiQuyMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewLoaiQuy() throws Exception {
        // Initialize the database
        loaiQuyRepository.saveAndFlush(loaiQuy);

        int databaseSizeBeforeUpdate = loaiQuyRepository.findAll().size();

        // Update the loaiQuy
        LoaiQuy updatedLoaiQuy = loaiQuyRepository.findById(loaiQuy.getId()).get();
        // Disconnect from session so that the updates on updatedLoaiQuy are not directly saved in db
        em.detach(updatedLoaiQuy);
        updatedLoaiQuy.ma(UPDATED_MA).ten(UPDATED_TEN);
        LoaiQuyDTO loaiQuyDTO = loaiQuyMapper.toDto(updatedLoaiQuy);

        restLoaiQuyMockMvc
            .perform(
                put(ENTITY_API_URL_ID, loaiQuyDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(loaiQuyDTO))
            )
            .andExpect(status().isOk());

        // Validate the LoaiQuy in the database
        List<LoaiQuy> loaiQuyList = loaiQuyRepository.findAll();
        assertThat(loaiQuyList).hasSize(databaseSizeBeforeUpdate);
        LoaiQuy testLoaiQuy = loaiQuyList.get(loaiQuyList.size() - 1);
        assertThat(testLoaiQuy.getMa()).isEqualTo(UPDATED_MA);
        assertThat(testLoaiQuy.getTen()).isEqualTo(UPDATED_TEN);
    }

    @Test
    @Transactional
    void putNonExistingLoaiQuy() throws Exception {
        int databaseSizeBeforeUpdate = loaiQuyRepository.findAll().size();
        loaiQuy.setId(count.incrementAndGet());

        // Create the LoaiQuy
        LoaiQuyDTO loaiQuyDTO = loaiQuyMapper.toDto(loaiQuy);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restLoaiQuyMockMvc
            .perform(
                put(ENTITY_API_URL_ID, loaiQuyDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(loaiQuyDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the LoaiQuy in the database
        List<LoaiQuy> loaiQuyList = loaiQuyRepository.findAll();
        assertThat(loaiQuyList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchLoaiQuy() throws Exception {
        int databaseSizeBeforeUpdate = loaiQuyRepository.findAll().size();
        loaiQuy.setId(count.incrementAndGet());

        // Create the LoaiQuy
        LoaiQuyDTO loaiQuyDTO = loaiQuyMapper.toDto(loaiQuy);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restLoaiQuyMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(loaiQuyDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the LoaiQuy in the database
        List<LoaiQuy> loaiQuyList = loaiQuyRepository.findAll();
        assertThat(loaiQuyList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamLoaiQuy() throws Exception {
        int databaseSizeBeforeUpdate = loaiQuyRepository.findAll().size();
        loaiQuy.setId(count.incrementAndGet());

        // Create the LoaiQuy
        LoaiQuyDTO loaiQuyDTO = loaiQuyMapper.toDto(loaiQuy);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restLoaiQuyMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(loaiQuyDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the LoaiQuy in the database
        List<LoaiQuy> loaiQuyList = loaiQuyRepository.findAll();
        assertThat(loaiQuyList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateLoaiQuyWithPatch() throws Exception {
        // Initialize the database
        loaiQuyRepository.saveAndFlush(loaiQuy);

        int databaseSizeBeforeUpdate = loaiQuyRepository.findAll().size();

        // Update the loaiQuy using partial update
        LoaiQuy partialUpdatedLoaiQuy = new LoaiQuy();
        partialUpdatedLoaiQuy.setId(loaiQuy.getId());

        restLoaiQuyMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedLoaiQuy.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedLoaiQuy))
            )
            .andExpect(status().isOk());

        // Validate the LoaiQuy in the database
        List<LoaiQuy> loaiQuyList = loaiQuyRepository.findAll();
        assertThat(loaiQuyList).hasSize(databaseSizeBeforeUpdate);
        LoaiQuy testLoaiQuy = loaiQuyList.get(loaiQuyList.size() - 1);
        assertThat(testLoaiQuy.getMa()).isEqualTo(DEFAULT_MA);
        assertThat(testLoaiQuy.getTen()).isEqualTo(DEFAULT_TEN);
    }

    @Test
    @Transactional
    void fullUpdateLoaiQuyWithPatch() throws Exception {
        // Initialize the database
        loaiQuyRepository.saveAndFlush(loaiQuy);

        int databaseSizeBeforeUpdate = loaiQuyRepository.findAll().size();

        // Update the loaiQuy using partial update
        LoaiQuy partialUpdatedLoaiQuy = new LoaiQuy();
        partialUpdatedLoaiQuy.setId(loaiQuy.getId());

        partialUpdatedLoaiQuy.ma(UPDATED_MA).ten(UPDATED_TEN);

        restLoaiQuyMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedLoaiQuy.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedLoaiQuy))
            )
            .andExpect(status().isOk());

        // Validate the LoaiQuy in the database
        List<LoaiQuy> loaiQuyList = loaiQuyRepository.findAll();
        assertThat(loaiQuyList).hasSize(databaseSizeBeforeUpdate);
        LoaiQuy testLoaiQuy = loaiQuyList.get(loaiQuyList.size() - 1);
        assertThat(testLoaiQuy.getMa()).isEqualTo(UPDATED_MA);
        assertThat(testLoaiQuy.getTen()).isEqualTo(UPDATED_TEN);
    }

    @Test
    @Transactional
    void patchNonExistingLoaiQuy() throws Exception {
        int databaseSizeBeforeUpdate = loaiQuyRepository.findAll().size();
        loaiQuy.setId(count.incrementAndGet());

        // Create the LoaiQuy
        LoaiQuyDTO loaiQuyDTO = loaiQuyMapper.toDto(loaiQuy);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restLoaiQuyMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, loaiQuyDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(loaiQuyDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the LoaiQuy in the database
        List<LoaiQuy> loaiQuyList = loaiQuyRepository.findAll();
        assertThat(loaiQuyList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchLoaiQuy() throws Exception {
        int databaseSizeBeforeUpdate = loaiQuyRepository.findAll().size();
        loaiQuy.setId(count.incrementAndGet());

        // Create the LoaiQuy
        LoaiQuyDTO loaiQuyDTO = loaiQuyMapper.toDto(loaiQuy);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restLoaiQuyMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(loaiQuyDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the LoaiQuy in the database
        List<LoaiQuy> loaiQuyList = loaiQuyRepository.findAll();
        assertThat(loaiQuyList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamLoaiQuy() throws Exception {
        int databaseSizeBeforeUpdate = loaiQuyRepository.findAll().size();
        loaiQuy.setId(count.incrementAndGet());

        // Create the LoaiQuy
        LoaiQuyDTO loaiQuyDTO = loaiQuyMapper.toDto(loaiQuy);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restLoaiQuyMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(loaiQuyDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the LoaiQuy in the database
        List<LoaiQuy> loaiQuyList = loaiQuyRepository.findAll();
        assertThat(loaiQuyList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteLoaiQuy() throws Exception {
        // Initialize the database
        loaiQuyRepository.saveAndFlush(loaiQuy);

        int databaseSizeBeforeDelete = loaiQuyRepository.findAll().size();

        // Delete the loaiQuy
        restLoaiQuyMockMvc
            .perform(delete(ENTITY_API_URL_ID, loaiQuy.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<LoaiQuy> loaiQuyList = loaiQuyRepository.findAll();
        assertThat(loaiQuyList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
