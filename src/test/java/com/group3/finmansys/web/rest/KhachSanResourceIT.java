package com.group3.finmansys.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.group3.finmansys.IntegrationTest;
import com.group3.finmansys.domain.KhachSan;
import com.group3.finmansys.repository.KhachSanRepository;
import com.group3.finmansys.service.criteria.KhachSanCriteria;
import com.group3.finmansys.service.dto.KhachSanDTO;
import com.group3.finmansys.service.mapper.KhachSanMapper;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link KhachSanResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class KhachSanResourceIT {

    private static final String DEFAULT_TEN = "AAAAAAAAAA";
    private static final String UPDATED_TEN = "BBBBBBBBBB";

    private static final String DEFAULT_DIA_CHI = "AAAAAAAAAA";
    private static final String UPDATED_DIA_CHI = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/khach-sans";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private KhachSanRepository khachSanRepository;

    @Autowired
    private KhachSanMapper khachSanMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restKhachSanMockMvc;

    private KhachSan khachSan;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static KhachSan createEntity(EntityManager em) {
        KhachSan khachSan = new KhachSan().ten(DEFAULT_TEN).diaChi(DEFAULT_DIA_CHI).email(DEFAULT_EMAIL);
        return khachSan;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static KhachSan createUpdatedEntity(EntityManager em) {
        KhachSan khachSan = new KhachSan().ten(UPDATED_TEN).diaChi(UPDATED_DIA_CHI).email(UPDATED_EMAIL);
        return khachSan;
    }

    @BeforeEach
    public void initTest() {
        khachSan = createEntity(em);
    }

    @Test
    @Transactional
    void createKhachSan() throws Exception {
        int databaseSizeBeforeCreate = khachSanRepository.findAll().size();
        // Create the KhachSan
        KhachSanDTO khachSanDTO = khachSanMapper.toDto(khachSan);
        restKhachSanMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(khachSanDTO)))
            .andExpect(status().isCreated());

        // Validate the KhachSan in the database
        List<KhachSan> khachSanList = khachSanRepository.findAll();
        assertThat(khachSanList).hasSize(databaseSizeBeforeCreate + 1);
        KhachSan testKhachSan = khachSanList.get(khachSanList.size() - 1);
        assertThat(testKhachSan.getTen()).isEqualTo(DEFAULT_TEN);
        assertThat(testKhachSan.getDiaChi()).isEqualTo(DEFAULT_DIA_CHI);
        assertThat(testKhachSan.getEmail()).isEqualTo(DEFAULT_EMAIL);
    }

    @Test
    @Transactional
    void createKhachSanWithExistingId() throws Exception {
        // Create the KhachSan with an existing ID
        khachSan.setId(1L);
        KhachSanDTO khachSanDTO = khachSanMapper.toDto(khachSan);

        int databaseSizeBeforeCreate = khachSanRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restKhachSanMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(khachSanDTO)))
            .andExpect(status().isBadRequest());

        // Validate the KhachSan in the database
        List<KhachSan> khachSanList = khachSanRepository.findAll();
        assertThat(khachSanList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkTenIsRequired() throws Exception {
        int databaseSizeBeforeTest = khachSanRepository.findAll().size();
        // set the field null
        khachSan.setTen(null);

        // Create the KhachSan, which fails.
        KhachSanDTO khachSanDTO = khachSanMapper.toDto(khachSan);

        restKhachSanMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(khachSanDTO)))
            .andExpect(status().isBadRequest());

        List<KhachSan> khachSanList = khachSanRepository.findAll();
        assertThat(khachSanList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllKhachSans() throws Exception {
        // Initialize the database
        khachSanRepository.saveAndFlush(khachSan);

        // Get all the khachSanList
        restKhachSanMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(khachSan.getId().intValue())))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)))
            .andExpect(jsonPath("$.[*].diaChi").value(hasItem(DEFAULT_DIA_CHI)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)));
    }

    @Test
    @Transactional
    void getKhachSan() throws Exception {
        // Initialize the database
        khachSanRepository.saveAndFlush(khachSan);

        // Get the khachSan
        restKhachSanMockMvc
            .perform(get(ENTITY_API_URL_ID, khachSan.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(khachSan.getId().intValue()))
            .andExpect(jsonPath("$.ten").value(DEFAULT_TEN))
            .andExpect(jsonPath("$.diaChi").value(DEFAULT_DIA_CHI))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL));
    }

    @Test
    @Transactional
    void getKhachSansByIdFiltering() throws Exception {
        // Initialize the database
        khachSanRepository.saveAndFlush(khachSan);

        Long id = khachSan.getId();

        defaultKhachSanShouldBeFound("id.equals=" + id);
        defaultKhachSanShouldNotBeFound("id.notEquals=" + id);

        defaultKhachSanShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultKhachSanShouldNotBeFound("id.greaterThan=" + id);

        defaultKhachSanShouldBeFound("id.lessThanOrEqual=" + id);
        defaultKhachSanShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllKhachSansByTenIsEqualToSomething() throws Exception {
        // Initialize the database
        khachSanRepository.saveAndFlush(khachSan);

        // Get all the khachSanList where ten equals to DEFAULT_TEN
        defaultKhachSanShouldBeFound("ten.equals=" + DEFAULT_TEN);

        // Get all the khachSanList where ten equals to UPDATED_TEN
        defaultKhachSanShouldNotBeFound("ten.equals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    void getAllKhachSansByTenIsNotEqualToSomething() throws Exception {
        // Initialize the database
        khachSanRepository.saveAndFlush(khachSan);

        // Get all the khachSanList where ten not equals to DEFAULT_TEN
        defaultKhachSanShouldNotBeFound("ten.notEquals=" + DEFAULT_TEN);

        // Get all the khachSanList where ten not equals to UPDATED_TEN
        defaultKhachSanShouldBeFound("ten.notEquals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    void getAllKhachSansByTenIsInShouldWork() throws Exception {
        // Initialize the database
        khachSanRepository.saveAndFlush(khachSan);

        // Get all the khachSanList where ten in DEFAULT_TEN or UPDATED_TEN
        defaultKhachSanShouldBeFound("ten.in=" + DEFAULT_TEN + "," + UPDATED_TEN);

        // Get all the khachSanList where ten equals to UPDATED_TEN
        defaultKhachSanShouldNotBeFound("ten.in=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    void getAllKhachSansByTenIsNullOrNotNull() throws Exception {
        // Initialize the database
        khachSanRepository.saveAndFlush(khachSan);

        // Get all the khachSanList where ten is not null
        defaultKhachSanShouldBeFound("ten.specified=true");

        // Get all the khachSanList where ten is null
        defaultKhachSanShouldNotBeFound("ten.specified=false");
    }

    @Test
    @Transactional
    void getAllKhachSansByTenContainsSomething() throws Exception {
        // Initialize the database
        khachSanRepository.saveAndFlush(khachSan);

        // Get all the khachSanList where ten contains DEFAULT_TEN
        defaultKhachSanShouldBeFound("ten.contains=" + DEFAULT_TEN);

        // Get all the khachSanList where ten contains UPDATED_TEN
        defaultKhachSanShouldNotBeFound("ten.contains=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    void getAllKhachSansByTenNotContainsSomething() throws Exception {
        // Initialize the database
        khachSanRepository.saveAndFlush(khachSan);

        // Get all the khachSanList where ten does not contain DEFAULT_TEN
        defaultKhachSanShouldNotBeFound("ten.doesNotContain=" + DEFAULT_TEN);

        // Get all the khachSanList where ten does not contain UPDATED_TEN
        defaultKhachSanShouldBeFound("ten.doesNotContain=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    void getAllKhachSansByDiaChiIsEqualToSomething() throws Exception {
        // Initialize the database
        khachSanRepository.saveAndFlush(khachSan);

        // Get all the khachSanList where diaChi equals to DEFAULT_DIA_CHI
        defaultKhachSanShouldBeFound("diaChi.equals=" + DEFAULT_DIA_CHI);

        // Get all the khachSanList where diaChi equals to UPDATED_DIA_CHI
        defaultKhachSanShouldNotBeFound("diaChi.equals=" + UPDATED_DIA_CHI);
    }

    @Test
    @Transactional
    void getAllKhachSansByDiaChiIsNotEqualToSomething() throws Exception {
        // Initialize the database
        khachSanRepository.saveAndFlush(khachSan);

        // Get all the khachSanList where diaChi not equals to DEFAULT_DIA_CHI
        defaultKhachSanShouldNotBeFound("diaChi.notEquals=" + DEFAULT_DIA_CHI);

        // Get all the khachSanList where diaChi not equals to UPDATED_DIA_CHI
        defaultKhachSanShouldBeFound("diaChi.notEquals=" + UPDATED_DIA_CHI);
    }

    @Test
    @Transactional
    void getAllKhachSansByDiaChiIsInShouldWork() throws Exception {
        // Initialize the database
        khachSanRepository.saveAndFlush(khachSan);

        // Get all the khachSanList where diaChi in DEFAULT_DIA_CHI or UPDATED_DIA_CHI
        defaultKhachSanShouldBeFound("diaChi.in=" + DEFAULT_DIA_CHI + "," + UPDATED_DIA_CHI);

        // Get all the khachSanList where diaChi equals to UPDATED_DIA_CHI
        defaultKhachSanShouldNotBeFound("diaChi.in=" + UPDATED_DIA_CHI);
    }

    @Test
    @Transactional
    void getAllKhachSansByDiaChiIsNullOrNotNull() throws Exception {
        // Initialize the database
        khachSanRepository.saveAndFlush(khachSan);

        // Get all the khachSanList where diaChi is not null
        defaultKhachSanShouldBeFound("diaChi.specified=true");

        // Get all the khachSanList where diaChi is null
        defaultKhachSanShouldNotBeFound("diaChi.specified=false");
    }

    @Test
    @Transactional
    void getAllKhachSansByDiaChiContainsSomething() throws Exception {
        // Initialize the database
        khachSanRepository.saveAndFlush(khachSan);

        // Get all the khachSanList where diaChi contains DEFAULT_DIA_CHI
        defaultKhachSanShouldBeFound("diaChi.contains=" + DEFAULT_DIA_CHI);

        // Get all the khachSanList where diaChi contains UPDATED_DIA_CHI
        defaultKhachSanShouldNotBeFound("diaChi.contains=" + UPDATED_DIA_CHI);
    }

    @Test
    @Transactional
    void getAllKhachSansByDiaChiNotContainsSomething() throws Exception {
        // Initialize the database
        khachSanRepository.saveAndFlush(khachSan);

        // Get all the khachSanList where diaChi does not contain DEFAULT_DIA_CHI
        defaultKhachSanShouldNotBeFound("diaChi.doesNotContain=" + DEFAULT_DIA_CHI);

        // Get all the khachSanList where diaChi does not contain UPDATED_DIA_CHI
        defaultKhachSanShouldBeFound("diaChi.doesNotContain=" + UPDATED_DIA_CHI);
    }

    @Test
    @Transactional
    void getAllKhachSansByEmailIsEqualToSomething() throws Exception {
        // Initialize the database
        khachSanRepository.saveAndFlush(khachSan);

        // Get all the khachSanList where email equals to DEFAULT_EMAIL
        defaultKhachSanShouldBeFound("email.equals=" + DEFAULT_EMAIL);

        // Get all the khachSanList where email equals to UPDATED_EMAIL
        defaultKhachSanShouldNotBeFound("email.equals=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    void getAllKhachSansByEmailIsNotEqualToSomething() throws Exception {
        // Initialize the database
        khachSanRepository.saveAndFlush(khachSan);

        // Get all the khachSanList where email not equals to DEFAULT_EMAIL
        defaultKhachSanShouldNotBeFound("email.notEquals=" + DEFAULT_EMAIL);

        // Get all the khachSanList where email not equals to UPDATED_EMAIL
        defaultKhachSanShouldBeFound("email.notEquals=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    void getAllKhachSansByEmailIsInShouldWork() throws Exception {
        // Initialize the database
        khachSanRepository.saveAndFlush(khachSan);

        // Get all the khachSanList where email in DEFAULT_EMAIL or UPDATED_EMAIL
        defaultKhachSanShouldBeFound("email.in=" + DEFAULT_EMAIL + "," + UPDATED_EMAIL);

        // Get all the khachSanList where email equals to UPDATED_EMAIL
        defaultKhachSanShouldNotBeFound("email.in=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    void getAllKhachSansByEmailIsNullOrNotNull() throws Exception {
        // Initialize the database
        khachSanRepository.saveAndFlush(khachSan);

        // Get all the khachSanList where email is not null
        defaultKhachSanShouldBeFound("email.specified=true");

        // Get all the khachSanList where email is null
        defaultKhachSanShouldNotBeFound("email.specified=false");
    }

    @Test
    @Transactional
    void getAllKhachSansByEmailContainsSomething() throws Exception {
        // Initialize the database
        khachSanRepository.saveAndFlush(khachSan);

        // Get all the khachSanList where email contains DEFAULT_EMAIL
        defaultKhachSanShouldBeFound("email.contains=" + DEFAULT_EMAIL);

        // Get all the khachSanList where email contains UPDATED_EMAIL
        defaultKhachSanShouldNotBeFound("email.contains=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    void getAllKhachSansByEmailNotContainsSomething() throws Exception {
        // Initialize the database
        khachSanRepository.saveAndFlush(khachSan);

        // Get all the khachSanList where email does not contain DEFAULT_EMAIL
        defaultKhachSanShouldNotBeFound("email.doesNotContain=" + DEFAULT_EMAIL);

        // Get all the khachSanList where email does not contain UPDATED_EMAIL
        defaultKhachSanShouldBeFound("email.doesNotContain=" + UPDATED_EMAIL);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultKhachSanShouldBeFound(String filter) throws Exception {
        restKhachSanMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(khachSan.getId().intValue())))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)))
            .andExpect(jsonPath("$.[*].diaChi").value(hasItem(DEFAULT_DIA_CHI)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)));

        // Check, that the count call also returns 1
        restKhachSanMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultKhachSanShouldNotBeFound(String filter) throws Exception {
        restKhachSanMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restKhachSanMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingKhachSan() throws Exception {
        // Get the khachSan
        restKhachSanMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewKhachSan() throws Exception {
        // Initialize the database
        khachSanRepository.saveAndFlush(khachSan);

        int databaseSizeBeforeUpdate = khachSanRepository.findAll().size();

        // Update the khachSan
        KhachSan updatedKhachSan = khachSanRepository.findById(khachSan.getId()).get();
        // Disconnect from session so that the updates on updatedKhachSan are not directly saved in db
        em.detach(updatedKhachSan);
        updatedKhachSan.ten(UPDATED_TEN).diaChi(UPDATED_DIA_CHI).email(UPDATED_EMAIL);
        KhachSanDTO khachSanDTO = khachSanMapper.toDto(updatedKhachSan);

        restKhachSanMockMvc
            .perform(
                put(ENTITY_API_URL_ID, khachSanDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(khachSanDTO))
            )
            .andExpect(status().isOk());

        // Validate the KhachSan in the database
        List<KhachSan> khachSanList = khachSanRepository.findAll();
        assertThat(khachSanList).hasSize(databaseSizeBeforeUpdate);
        KhachSan testKhachSan = khachSanList.get(khachSanList.size() - 1);
        assertThat(testKhachSan.getTen()).isEqualTo(UPDATED_TEN);
        assertThat(testKhachSan.getDiaChi()).isEqualTo(UPDATED_DIA_CHI);
        assertThat(testKhachSan.getEmail()).isEqualTo(UPDATED_EMAIL);
    }

    @Test
    @Transactional
    void putNonExistingKhachSan() throws Exception {
        int databaseSizeBeforeUpdate = khachSanRepository.findAll().size();
        khachSan.setId(count.incrementAndGet());

        // Create the KhachSan
        KhachSanDTO khachSanDTO = khachSanMapper.toDto(khachSan);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restKhachSanMockMvc
            .perform(
                put(ENTITY_API_URL_ID, khachSanDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(khachSanDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the KhachSan in the database
        List<KhachSan> khachSanList = khachSanRepository.findAll();
        assertThat(khachSanList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchKhachSan() throws Exception {
        int databaseSizeBeforeUpdate = khachSanRepository.findAll().size();
        khachSan.setId(count.incrementAndGet());

        // Create the KhachSan
        KhachSanDTO khachSanDTO = khachSanMapper.toDto(khachSan);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restKhachSanMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(khachSanDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the KhachSan in the database
        List<KhachSan> khachSanList = khachSanRepository.findAll();
        assertThat(khachSanList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamKhachSan() throws Exception {
        int databaseSizeBeforeUpdate = khachSanRepository.findAll().size();
        khachSan.setId(count.incrementAndGet());

        // Create the KhachSan
        KhachSanDTO khachSanDTO = khachSanMapper.toDto(khachSan);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restKhachSanMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(khachSanDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the KhachSan in the database
        List<KhachSan> khachSanList = khachSanRepository.findAll();
        assertThat(khachSanList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateKhachSanWithPatch() throws Exception {
        // Initialize the database
        khachSanRepository.saveAndFlush(khachSan);

        int databaseSizeBeforeUpdate = khachSanRepository.findAll().size();

        // Update the khachSan using partial update
        KhachSan partialUpdatedKhachSan = new KhachSan();
        partialUpdatedKhachSan.setId(khachSan.getId());

        partialUpdatedKhachSan.ten(UPDATED_TEN).email(UPDATED_EMAIL);

        restKhachSanMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedKhachSan.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedKhachSan))
            )
            .andExpect(status().isOk());

        // Validate the KhachSan in the database
        List<KhachSan> khachSanList = khachSanRepository.findAll();
        assertThat(khachSanList).hasSize(databaseSizeBeforeUpdate);
        KhachSan testKhachSan = khachSanList.get(khachSanList.size() - 1);
        assertThat(testKhachSan.getTen()).isEqualTo(UPDATED_TEN);
        assertThat(testKhachSan.getDiaChi()).isEqualTo(DEFAULT_DIA_CHI);
        assertThat(testKhachSan.getEmail()).isEqualTo(UPDATED_EMAIL);
    }

    @Test
    @Transactional
    void fullUpdateKhachSanWithPatch() throws Exception {
        // Initialize the database
        khachSanRepository.saveAndFlush(khachSan);

        int databaseSizeBeforeUpdate = khachSanRepository.findAll().size();

        // Update the khachSan using partial update
        KhachSan partialUpdatedKhachSan = new KhachSan();
        partialUpdatedKhachSan.setId(khachSan.getId());

        partialUpdatedKhachSan.ten(UPDATED_TEN).diaChi(UPDATED_DIA_CHI).email(UPDATED_EMAIL);

        restKhachSanMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedKhachSan.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedKhachSan))
            )
            .andExpect(status().isOk());

        // Validate the KhachSan in the database
        List<KhachSan> khachSanList = khachSanRepository.findAll();
        assertThat(khachSanList).hasSize(databaseSizeBeforeUpdate);
        KhachSan testKhachSan = khachSanList.get(khachSanList.size() - 1);
        assertThat(testKhachSan.getTen()).isEqualTo(UPDATED_TEN);
        assertThat(testKhachSan.getDiaChi()).isEqualTo(UPDATED_DIA_CHI);
        assertThat(testKhachSan.getEmail()).isEqualTo(UPDATED_EMAIL);
    }

    @Test
    @Transactional
    void patchNonExistingKhachSan() throws Exception {
        int databaseSizeBeforeUpdate = khachSanRepository.findAll().size();
        khachSan.setId(count.incrementAndGet());

        // Create the KhachSan
        KhachSanDTO khachSanDTO = khachSanMapper.toDto(khachSan);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restKhachSanMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, khachSanDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(khachSanDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the KhachSan in the database
        List<KhachSan> khachSanList = khachSanRepository.findAll();
        assertThat(khachSanList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchKhachSan() throws Exception {
        int databaseSizeBeforeUpdate = khachSanRepository.findAll().size();
        khachSan.setId(count.incrementAndGet());

        // Create the KhachSan
        KhachSanDTO khachSanDTO = khachSanMapper.toDto(khachSan);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restKhachSanMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(khachSanDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the KhachSan in the database
        List<KhachSan> khachSanList = khachSanRepository.findAll();
        assertThat(khachSanList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamKhachSan() throws Exception {
        int databaseSizeBeforeUpdate = khachSanRepository.findAll().size();
        khachSan.setId(count.incrementAndGet());

        // Create the KhachSan
        KhachSanDTO khachSanDTO = khachSanMapper.toDto(khachSan);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restKhachSanMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(khachSanDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the KhachSan in the database
        List<KhachSan> khachSanList = khachSanRepository.findAll();
        assertThat(khachSanList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteKhachSan() throws Exception {
        // Initialize the database
        khachSanRepository.saveAndFlush(khachSan);

        int databaseSizeBeforeDelete = khachSanRepository.findAll().size();

        // Delete the khachSan
        restKhachSanMockMvc
            .perform(delete(ENTITY_API_URL_ID, khachSan.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<KhachSan> khachSanList = khachSanRepository.findAll();
        assertThat(khachSanList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
