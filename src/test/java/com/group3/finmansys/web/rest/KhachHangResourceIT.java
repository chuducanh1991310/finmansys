package com.group3.finmansys.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.group3.finmansys.IntegrationTest;
import com.group3.finmansys.domain.KhachHang;
import com.group3.finmansys.repository.KhachHangRepository;
import com.group3.finmansys.service.criteria.KhachHangCriteria;
import com.group3.finmansys.service.dto.KhachHangDTO;
import com.group3.finmansys.service.mapper.KhachHangMapper;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link KhachHangResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class KhachHangResourceIT {

    private static final String DEFAULT_TEN = "AAAAAAAAAA";
    private static final String UPDATED_TEN = "BBBBBBBBBB";

    private static final String DEFAULT_DIA_CHI = "AAAAAAAAAA";
    private static final String UPDATED_DIA_CHI = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_SDT = "AAAAAAAAAA";
    private static final String UPDATED_SDT = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/khach-hangs";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private KhachHangRepository khachHangRepository;

    @Autowired
    private KhachHangMapper khachHangMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restKhachHangMockMvc;

    private KhachHang khachHang;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static KhachHang createEntity(EntityManager em) {
        KhachHang khachHang = new KhachHang().ten(DEFAULT_TEN).diaChi(DEFAULT_DIA_CHI).email(DEFAULT_EMAIL).sdt(DEFAULT_SDT);
        return khachHang;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static KhachHang createUpdatedEntity(EntityManager em) {
        KhachHang khachHang = new KhachHang().ten(UPDATED_TEN).diaChi(UPDATED_DIA_CHI).email(UPDATED_EMAIL).sdt(UPDATED_SDT);
        return khachHang;
    }

    @BeforeEach
    public void initTest() {
        khachHang = createEntity(em);
    }

    @Test
    @Transactional
    void createKhachHang() throws Exception {
        int databaseSizeBeforeCreate = khachHangRepository.findAll().size();
        // Create the KhachHang
        KhachHangDTO khachHangDTO = khachHangMapper.toDto(khachHang);
        restKhachHangMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(khachHangDTO)))
            .andExpect(status().isCreated());

        // Validate the KhachHang in the database
        List<KhachHang> khachHangList = khachHangRepository.findAll();
        assertThat(khachHangList).hasSize(databaseSizeBeforeCreate + 1);
        KhachHang testKhachHang = khachHangList.get(khachHangList.size() - 1);
        assertThat(testKhachHang.getTen()).isEqualTo(DEFAULT_TEN);
        assertThat(testKhachHang.getDiaChi()).isEqualTo(DEFAULT_DIA_CHI);
        assertThat(testKhachHang.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testKhachHang.getSdt()).isEqualTo(DEFAULT_SDT);
    }

    @Test
    @Transactional
    void createKhachHangWithExistingId() throws Exception {
        // Create the KhachHang with an existing ID
        khachHang.setId(1L);
        KhachHangDTO khachHangDTO = khachHangMapper.toDto(khachHang);

        int databaseSizeBeforeCreate = khachHangRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restKhachHangMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(khachHangDTO)))
            .andExpect(status().isBadRequest());

        // Validate the KhachHang in the database
        List<KhachHang> khachHangList = khachHangRepository.findAll();
        assertThat(khachHangList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkTenIsRequired() throws Exception {
        int databaseSizeBeforeTest = khachHangRepository.findAll().size();
        // set the field null
        khachHang.setTen(null);

        // Create the KhachHang, which fails.
        KhachHangDTO khachHangDTO = khachHangMapper.toDto(khachHang);

        restKhachHangMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(khachHangDTO)))
            .andExpect(status().isBadRequest());

        List<KhachHang> khachHangList = khachHangRepository.findAll();
        assertThat(khachHangList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllKhachHangs() throws Exception {
        // Initialize the database
        khachHangRepository.saveAndFlush(khachHang);

        // Get all the khachHangList
        restKhachHangMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(khachHang.getId().intValue())))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)))
            .andExpect(jsonPath("$.[*].diaChi").value(hasItem(DEFAULT_DIA_CHI)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].sdt").value(hasItem(DEFAULT_SDT)));
    }

    @Test
    @Transactional
    void getKhachHang() throws Exception {
        // Initialize the database
        khachHangRepository.saveAndFlush(khachHang);

        // Get the khachHang
        restKhachHangMockMvc
            .perform(get(ENTITY_API_URL_ID, khachHang.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(khachHang.getId().intValue()))
            .andExpect(jsonPath("$.ten").value(DEFAULT_TEN))
            .andExpect(jsonPath("$.diaChi").value(DEFAULT_DIA_CHI))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL))
            .andExpect(jsonPath("$.sdt").value(DEFAULT_SDT));
    }

    @Test
    @Transactional
    void getKhachHangsByIdFiltering() throws Exception {
        // Initialize the database
        khachHangRepository.saveAndFlush(khachHang);

        Long id = khachHang.getId();

        defaultKhachHangShouldBeFound("id.equals=" + id);
        defaultKhachHangShouldNotBeFound("id.notEquals=" + id);

        defaultKhachHangShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultKhachHangShouldNotBeFound("id.greaterThan=" + id);

        defaultKhachHangShouldBeFound("id.lessThanOrEqual=" + id);
        defaultKhachHangShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllKhachHangsByTenIsEqualToSomething() throws Exception {
        // Initialize the database
        khachHangRepository.saveAndFlush(khachHang);

        // Get all the khachHangList where ten equals to DEFAULT_TEN
        defaultKhachHangShouldBeFound("ten.equals=" + DEFAULT_TEN);

        // Get all the khachHangList where ten equals to UPDATED_TEN
        defaultKhachHangShouldNotBeFound("ten.equals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    void getAllKhachHangsByTenIsNotEqualToSomething() throws Exception {
        // Initialize the database
        khachHangRepository.saveAndFlush(khachHang);

        // Get all the khachHangList where ten not equals to DEFAULT_TEN
        defaultKhachHangShouldNotBeFound("ten.notEquals=" + DEFAULT_TEN);

        // Get all the khachHangList where ten not equals to UPDATED_TEN
        defaultKhachHangShouldBeFound("ten.notEquals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    void getAllKhachHangsByTenIsInShouldWork() throws Exception {
        // Initialize the database
        khachHangRepository.saveAndFlush(khachHang);

        // Get all the khachHangList where ten in DEFAULT_TEN or UPDATED_TEN
        defaultKhachHangShouldBeFound("ten.in=" + DEFAULT_TEN + "," + UPDATED_TEN);

        // Get all the khachHangList where ten equals to UPDATED_TEN
        defaultKhachHangShouldNotBeFound("ten.in=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    void getAllKhachHangsByTenIsNullOrNotNull() throws Exception {
        // Initialize the database
        khachHangRepository.saveAndFlush(khachHang);

        // Get all the khachHangList where ten is not null
        defaultKhachHangShouldBeFound("ten.specified=true");

        // Get all the khachHangList where ten is null
        defaultKhachHangShouldNotBeFound("ten.specified=false");
    }

    @Test
    @Transactional
    void getAllKhachHangsByTenContainsSomething() throws Exception {
        // Initialize the database
        khachHangRepository.saveAndFlush(khachHang);

        // Get all the khachHangList where ten contains DEFAULT_TEN
        defaultKhachHangShouldBeFound("ten.contains=" + DEFAULT_TEN);

        // Get all the khachHangList where ten contains UPDATED_TEN
        defaultKhachHangShouldNotBeFound("ten.contains=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    void getAllKhachHangsByTenNotContainsSomething() throws Exception {
        // Initialize the database
        khachHangRepository.saveAndFlush(khachHang);

        // Get all the khachHangList where ten does not contain DEFAULT_TEN
        defaultKhachHangShouldNotBeFound("ten.doesNotContain=" + DEFAULT_TEN);

        // Get all the khachHangList where ten does not contain UPDATED_TEN
        defaultKhachHangShouldBeFound("ten.doesNotContain=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    void getAllKhachHangsByDiaChiIsEqualToSomething() throws Exception {
        // Initialize the database
        khachHangRepository.saveAndFlush(khachHang);

        // Get all the khachHangList where diaChi equals to DEFAULT_DIA_CHI
        defaultKhachHangShouldBeFound("diaChi.equals=" + DEFAULT_DIA_CHI);

        // Get all the khachHangList where diaChi equals to UPDATED_DIA_CHI
        defaultKhachHangShouldNotBeFound("diaChi.equals=" + UPDATED_DIA_CHI);
    }

    @Test
    @Transactional
    void getAllKhachHangsByDiaChiIsNotEqualToSomething() throws Exception {
        // Initialize the database
        khachHangRepository.saveAndFlush(khachHang);

        // Get all the khachHangList where diaChi not equals to DEFAULT_DIA_CHI
        defaultKhachHangShouldNotBeFound("diaChi.notEquals=" + DEFAULT_DIA_CHI);

        // Get all the khachHangList where diaChi not equals to UPDATED_DIA_CHI
        defaultKhachHangShouldBeFound("diaChi.notEquals=" + UPDATED_DIA_CHI);
    }

    @Test
    @Transactional
    void getAllKhachHangsByDiaChiIsInShouldWork() throws Exception {
        // Initialize the database
        khachHangRepository.saveAndFlush(khachHang);

        // Get all the khachHangList where diaChi in DEFAULT_DIA_CHI or UPDATED_DIA_CHI
        defaultKhachHangShouldBeFound("diaChi.in=" + DEFAULT_DIA_CHI + "," + UPDATED_DIA_CHI);

        // Get all the khachHangList where diaChi equals to UPDATED_DIA_CHI
        defaultKhachHangShouldNotBeFound("diaChi.in=" + UPDATED_DIA_CHI);
    }

    @Test
    @Transactional
    void getAllKhachHangsByDiaChiIsNullOrNotNull() throws Exception {
        // Initialize the database
        khachHangRepository.saveAndFlush(khachHang);

        // Get all the khachHangList where diaChi is not null
        defaultKhachHangShouldBeFound("diaChi.specified=true");

        // Get all the khachHangList where diaChi is null
        defaultKhachHangShouldNotBeFound("diaChi.specified=false");
    }

    @Test
    @Transactional
    void getAllKhachHangsByDiaChiContainsSomething() throws Exception {
        // Initialize the database
        khachHangRepository.saveAndFlush(khachHang);

        // Get all the khachHangList where diaChi contains DEFAULT_DIA_CHI
        defaultKhachHangShouldBeFound("diaChi.contains=" + DEFAULT_DIA_CHI);

        // Get all the khachHangList where diaChi contains UPDATED_DIA_CHI
        defaultKhachHangShouldNotBeFound("diaChi.contains=" + UPDATED_DIA_CHI);
    }

    @Test
    @Transactional
    void getAllKhachHangsByDiaChiNotContainsSomething() throws Exception {
        // Initialize the database
        khachHangRepository.saveAndFlush(khachHang);

        // Get all the khachHangList where diaChi does not contain DEFAULT_DIA_CHI
        defaultKhachHangShouldNotBeFound("diaChi.doesNotContain=" + DEFAULT_DIA_CHI);

        // Get all the khachHangList where diaChi does not contain UPDATED_DIA_CHI
        defaultKhachHangShouldBeFound("diaChi.doesNotContain=" + UPDATED_DIA_CHI);
    }

    @Test
    @Transactional
    void getAllKhachHangsByEmailIsEqualToSomething() throws Exception {
        // Initialize the database
        khachHangRepository.saveAndFlush(khachHang);

        // Get all the khachHangList where email equals to DEFAULT_EMAIL
        defaultKhachHangShouldBeFound("email.equals=" + DEFAULT_EMAIL);

        // Get all the khachHangList where email equals to UPDATED_EMAIL
        defaultKhachHangShouldNotBeFound("email.equals=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    void getAllKhachHangsByEmailIsNotEqualToSomething() throws Exception {
        // Initialize the database
        khachHangRepository.saveAndFlush(khachHang);

        // Get all the khachHangList where email not equals to DEFAULT_EMAIL
        defaultKhachHangShouldNotBeFound("email.notEquals=" + DEFAULT_EMAIL);

        // Get all the khachHangList where email not equals to UPDATED_EMAIL
        defaultKhachHangShouldBeFound("email.notEquals=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    void getAllKhachHangsByEmailIsInShouldWork() throws Exception {
        // Initialize the database
        khachHangRepository.saveAndFlush(khachHang);

        // Get all the khachHangList where email in DEFAULT_EMAIL or UPDATED_EMAIL
        defaultKhachHangShouldBeFound("email.in=" + DEFAULT_EMAIL + "," + UPDATED_EMAIL);

        // Get all the khachHangList where email equals to UPDATED_EMAIL
        defaultKhachHangShouldNotBeFound("email.in=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    void getAllKhachHangsByEmailIsNullOrNotNull() throws Exception {
        // Initialize the database
        khachHangRepository.saveAndFlush(khachHang);

        // Get all the khachHangList where email is not null
        defaultKhachHangShouldBeFound("email.specified=true");

        // Get all the khachHangList where email is null
        defaultKhachHangShouldNotBeFound("email.specified=false");
    }

    @Test
    @Transactional
    void getAllKhachHangsByEmailContainsSomething() throws Exception {
        // Initialize the database
        khachHangRepository.saveAndFlush(khachHang);

        // Get all the khachHangList where email contains DEFAULT_EMAIL
        defaultKhachHangShouldBeFound("email.contains=" + DEFAULT_EMAIL);

        // Get all the khachHangList where email contains UPDATED_EMAIL
        defaultKhachHangShouldNotBeFound("email.contains=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    void getAllKhachHangsByEmailNotContainsSomething() throws Exception {
        // Initialize the database
        khachHangRepository.saveAndFlush(khachHang);

        // Get all the khachHangList where email does not contain DEFAULT_EMAIL
        defaultKhachHangShouldNotBeFound("email.doesNotContain=" + DEFAULT_EMAIL);

        // Get all the khachHangList where email does not contain UPDATED_EMAIL
        defaultKhachHangShouldBeFound("email.doesNotContain=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    void getAllKhachHangsBySdtIsEqualToSomething() throws Exception {
        // Initialize the database
        khachHangRepository.saveAndFlush(khachHang);

        // Get all the khachHangList where sdt equals to DEFAULT_SDT
        defaultKhachHangShouldBeFound("sdt.equals=" + DEFAULT_SDT);

        // Get all the khachHangList where sdt equals to UPDATED_SDT
        defaultKhachHangShouldNotBeFound("sdt.equals=" + UPDATED_SDT);
    }

    @Test
    @Transactional
    void getAllKhachHangsBySdtIsNotEqualToSomething() throws Exception {
        // Initialize the database
        khachHangRepository.saveAndFlush(khachHang);

        // Get all the khachHangList where sdt not equals to DEFAULT_SDT
        defaultKhachHangShouldNotBeFound("sdt.notEquals=" + DEFAULT_SDT);

        // Get all the khachHangList where sdt not equals to UPDATED_SDT
        defaultKhachHangShouldBeFound("sdt.notEquals=" + UPDATED_SDT);
    }

    @Test
    @Transactional
    void getAllKhachHangsBySdtIsInShouldWork() throws Exception {
        // Initialize the database
        khachHangRepository.saveAndFlush(khachHang);

        // Get all the khachHangList where sdt in DEFAULT_SDT or UPDATED_SDT
        defaultKhachHangShouldBeFound("sdt.in=" + DEFAULT_SDT + "," + UPDATED_SDT);

        // Get all the khachHangList where sdt equals to UPDATED_SDT
        defaultKhachHangShouldNotBeFound("sdt.in=" + UPDATED_SDT);
    }

    @Test
    @Transactional
    void getAllKhachHangsBySdtIsNullOrNotNull() throws Exception {
        // Initialize the database
        khachHangRepository.saveAndFlush(khachHang);

        // Get all the khachHangList where sdt is not null
        defaultKhachHangShouldBeFound("sdt.specified=true");

        // Get all the khachHangList where sdt is null
        defaultKhachHangShouldNotBeFound("sdt.specified=false");
    }

    @Test
    @Transactional
    void getAllKhachHangsBySdtContainsSomething() throws Exception {
        // Initialize the database
        khachHangRepository.saveAndFlush(khachHang);

        // Get all the khachHangList where sdt contains DEFAULT_SDT
        defaultKhachHangShouldBeFound("sdt.contains=" + DEFAULT_SDT);

        // Get all the khachHangList where sdt contains UPDATED_SDT
        defaultKhachHangShouldNotBeFound("sdt.contains=" + UPDATED_SDT);
    }

    @Test
    @Transactional
    void getAllKhachHangsBySdtNotContainsSomething() throws Exception {
        // Initialize the database
        khachHangRepository.saveAndFlush(khachHang);

        // Get all the khachHangList where sdt does not contain DEFAULT_SDT
        defaultKhachHangShouldNotBeFound("sdt.doesNotContain=" + DEFAULT_SDT);

        // Get all the khachHangList where sdt does not contain UPDATED_SDT
        defaultKhachHangShouldBeFound("sdt.doesNotContain=" + UPDATED_SDT);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultKhachHangShouldBeFound(String filter) throws Exception {
        restKhachHangMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(khachHang.getId().intValue())))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)))
            .andExpect(jsonPath("$.[*].diaChi").value(hasItem(DEFAULT_DIA_CHI)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].sdt").value(hasItem(DEFAULT_SDT)));

        // Check, that the count call also returns 1
        restKhachHangMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultKhachHangShouldNotBeFound(String filter) throws Exception {
        restKhachHangMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restKhachHangMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingKhachHang() throws Exception {
        // Get the khachHang
        restKhachHangMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewKhachHang() throws Exception {
        // Initialize the database
        khachHangRepository.saveAndFlush(khachHang);

        int databaseSizeBeforeUpdate = khachHangRepository.findAll().size();

        // Update the khachHang
        KhachHang updatedKhachHang = khachHangRepository.findById(khachHang.getId()).get();
        // Disconnect from session so that the updates on updatedKhachHang are not directly saved in db
        em.detach(updatedKhachHang);
        updatedKhachHang.ten(UPDATED_TEN).diaChi(UPDATED_DIA_CHI).email(UPDATED_EMAIL).sdt(UPDATED_SDT);
        KhachHangDTO khachHangDTO = khachHangMapper.toDto(updatedKhachHang);

        restKhachHangMockMvc
            .perform(
                put(ENTITY_API_URL_ID, khachHangDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(khachHangDTO))
            )
            .andExpect(status().isOk());

        // Validate the KhachHang in the database
        List<KhachHang> khachHangList = khachHangRepository.findAll();
        assertThat(khachHangList).hasSize(databaseSizeBeforeUpdate);
        KhachHang testKhachHang = khachHangList.get(khachHangList.size() - 1);
        assertThat(testKhachHang.getTen()).isEqualTo(UPDATED_TEN);
        assertThat(testKhachHang.getDiaChi()).isEqualTo(UPDATED_DIA_CHI);
        assertThat(testKhachHang.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testKhachHang.getSdt()).isEqualTo(UPDATED_SDT);
    }

    @Test
    @Transactional
    void putNonExistingKhachHang() throws Exception {
        int databaseSizeBeforeUpdate = khachHangRepository.findAll().size();
        khachHang.setId(count.incrementAndGet());

        // Create the KhachHang
        KhachHangDTO khachHangDTO = khachHangMapper.toDto(khachHang);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restKhachHangMockMvc
            .perform(
                put(ENTITY_API_URL_ID, khachHangDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(khachHangDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the KhachHang in the database
        List<KhachHang> khachHangList = khachHangRepository.findAll();
        assertThat(khachHangList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchKhachHang() throws Exception {
        int databaseSizeBeforeUpdate = khachHangRepository.findAll().size();
        khachHang.setId(count.incrementAndGet());

        // Create the KhachHang
        KhachHangDTO khachHangDTO = khachHangMapper.toDto(khachHang);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restKhachHangMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(khachHangDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the KhachHang in the database
        List<KhachHang> khachHangList = khachHangRepository.findAll();
        assertThat(khachHangList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamKhachHang() throws Exception {
        int databaseSizeBeforeUpdate = khachHangRepository.findAll().size();
        khachHang.setId(count.incrementAndGet());

        // Create the KhachHang
        KhachHangDTO khachHangDTO = khachHangMapper.toDto(khachHang);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restKhachHangMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(khachHangDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the KhachHang in the database
        List<KhachHang> khachHangList = khachHangRepository.findAll();
        assertThat(khachHangList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateKhachHangWithPatch() throws Exception {
        // Initialize the database
        khachHangRepository.saveAndFlush(khachHang);

        int databaseSizeBeforeUpdate = khachHangRepository.findAll().size();

        // Update the khachHang using partial update
        KhachHang partialUpdatedKhachHang = new KhachHang();
        partialUpdatedKhachHang.setId(khachHang.getId());

        partialUpdatedKhachHang.ten(UPDATED_TEN).diaChi(UPDATED_DIA_CHI);

        restKhachHangMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedKhachHang.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedKhachHang))
            )
            .andExpect(status().isOk());

        // Validate the KhachHang in the database
        List<KhachHang> khachHangList = khachHangRepository.findAll();
        assertThat(khachHangList).hasSize(databaseSizeBeforeUpdate);
        KhachHang testKhachHang = khachHangList.get(khachHangList.size() - 1);
        assertThat(testKhachHang.getTen()).isEqualTo(UPDATED_TEN);
        assertThat(testKhachHang.getDiaChi()).isEqualTo(UPDATED_DIA_CHI);
        assertThat(testKhachHang.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testKhachHang.getSdt()).isEqualTo(DEFAULT_SDT);
    }

    @Test
    @Transactional
    void fullUpdateKhachHangWithPatch() throws Exception {
        // Initialize the database
        khachHangRepository.saveAndFlush(khachHang);

        int databaseSizeBeforeUpdate = khachHangRepository.findAll().size();

        // Update the khachHang using partial update
        KhachHang partialUpdatedKhachHang = new KhachHang();
        partialUpdatedKhachHang.setId(khachHang.getId());

        partialUpdatedKhachHang.ten(UPDATED_TEN).diaChi(UPDATED_DIA_CHI).email(UPDATED_EMAIL).sdt(UPDATED_SDT);

        restKhachHangMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedKhachHang.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedKhachHang))
            )
            .andExpect(status().isOk());

        // Validate the KhachHang in the database
        List<KhachHang> khachHangList = khachHangRepository.findAll();
        assertThat(khachHangList).hasSize(databaseSizeBeforeUpdate);
        KhachHang testKhachHang = khachHangList.get(khachHangList.size() - 1);
        assertThat(testKhachHang.getTen()).isEqualTo(UPDATED_TEN);
        assertThat(testKhachHang.getDiaChi()).isEqualTo(UPDATED_DIA_CHI);
        assertThat(testKhachHang.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testKhachHang.getSdt()).isEqualTo(UPDATED_SDT);
    }

    @Test
    @Transactional
    void patchNonExistingKhachHang() throws Exception {
        int databaseSizeBeforeUpdate = khachHangRepository.findAll().size();
        khachHang.setId(count.incrementAndGet());

        // Create the KhachHang
        KhachHangDTO khachHangDTO = khachHangMapper.toDto(khachHang);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restKhachHangMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, khachHangDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(khachHangDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the KhachHang in the database
        List<KhachHang> khachHangList = khachHangRepository.findAll();
        assertThat(khachHangList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchKhachHang() throws Exception {
        int databaseSizeBeforeUpdate = khachHangRepository.findAll().size();
        khachHang.setId(count.incrementAndGet());

        // Create the KhachHang
        KhachHangDTO khachHangDTO = khachHangMapper.toDto(khachHang);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restKhachHangMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(khachHangDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the KhachHang in the database
        List<KhachHang> khachHangList = khachHangRepository.findAll();
        assertThat(khachHangList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamKhachHang() throws Exception {
        int databaseSizeBeforeUpdate = khachHangRepository.findAll().size();
        khachHang.setId(count.incrementAndGet());

        // Create the KhachHang
        KhachHangDTO khachHangDTO = khachHangMapper.toDto(khachHang);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restKhachHangMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(khachHangDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the KhachHang in the database
        List<KhachHang> khachHangList = khachHangRepository.findAll();
        assertThat(khachHangList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteKhachHang() throws Exception {
        // Initialize the database
        khachHangRepository.saveAndFlush(khachHang);

        int databaseSizeBeforeDelete = khachHangRepository.findAll().size();

        // Delete the khachHang
        restKhachHangMockMvc
            .perform(delete(ENTITY_API_URL_ID, khachHang.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<KhachHang> khachHangList = khachHangRepository.findAll();
        assertThat(khachHangList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
