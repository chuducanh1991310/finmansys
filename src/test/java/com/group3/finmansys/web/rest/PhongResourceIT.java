package com.group3.finmansys.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.group3.finmansys.IntegrationTest;
import com.group3.finmansys.domain.KhachSan;
import com.group3.finmansys.domain.Phong;
import com.group3.finmansys.repository.PhongRepository;
import com.group3.finmansys.service.criteria.PhongCriteria;
import com.group3.finmansys.service.dto.PhongDTO;
import com.group3.finmansys.service.mapper.PhongMapper;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link PhongResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class PhongResourceIT {

    private static final String DEFAULT_TEN = "AAAAAAAAAA";
    private static final String UPDATED_TEN = "BBBBBBBBBB";

    private static final String DEFAULT_LOAI = "AAAAAAAAAA";
    private static final String UPDATED_LOAI = "BBBBBBBBBB";

    private static final Double DEFAULT_GIA = 1D;
    private static final Double UPDATED_GIA = 2D;
    private static final Double SMALLER_GIA = 1D - 1D;

    private static final String ENTITY_API_URL = "/api/phongs";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private PhongRepository phongRepository;

    @Autowired
    private PhongMapper phongMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restPhongMockMvc;

    private Phong phong;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Phong createEntity(EntityManager em) {
        Phong phong = new Phong().ten(DEFAULT_TEN).loai(DEFAULT_LOAI).gia(DEFAULT_GIA);
        // Add required entity
        KhachSan khachSan;
        if (TestUtil.findAll(em, KhachSan.class).isEmpty()) {
            khachSan = KhachSanResourceIT.createEntity(em);
            em.persist(khachSan);
            em.flush();
        } else {
            khachSan = TestUtil.findAll(em, KhachSan.class).get(0);
        }
        phong.setKhachSan(khachSan);
        return phong;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Phong createUpdatedEntity(EntityManager em) {
        Phong phong = new Phong().ten(UPDATED_TEN).loai(UPDATED_LOAI).gia(UPDATED_GIA);
        // Add required entity
        KhachSan khachSan;
        if (TestUtil.findAll(em, KhachSan.class).isEmpty()) {
            khachSan = KhachSanResourceIT.createUpdatedEntity(em);
            em.persist(khachSan);
            em.flush();
        } else {
            khachSan = TestUtil.findAll(em, KhachSan.class).get(0);
        }
        phong.setKhachSan(khachSan);
        return phong;
    }

    @BeforeEach
    public void initTest() {
        phong = createEntity(em);
    }

    @Test
    @Transactional
    void createPhong() throws Exception {
        int databaseSizeBeforeCreate = phongRepository.findAll().size();
        // Create the Phong
        PhongDTO phongDTO = phongMapper.toDto(phong);
        restPhongMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(phongDTO)))
            .andExpect(status().isCreated());

        // Validate the Phong in the database
        List<Phong> phongList = phongRepository.findAll();
        assertThat(phongList).hasSize(databaseSizeBeforeCreate + 1);
        Phong testPhong = phongList.get(phongList.size() - 1);
        assertThat(testPhong.getTen()).isEqualTo(DEFAULT_TEN);
        assertThat(testPhong.getLoai()).isEqualTo(DEFAULT_LOAI);
        assertThat(testPhong.getGia()).isEqualTo(DEFAULT_GIA);
    }

    @Test
    @Transactional
    void createPhongWithExistingId() throws Exception {
        // Create the Phong with an existing ID
        phong.setId(1L);
        PhongDTO phongDTO = phongMapper.toDto(phong);

        int databaseSizeBeforeCreate = phongRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restPhongMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(phongDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Phong in the database
        List<Phong> phongList = phongRepository.findAll();
        assertThat(phongList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkTenIsRequired() throws Exception {
        int databaseSizeBeforeTest = phongRepository.findAll().size();
        // set the field null
        phong.setTen(null);

        // Create the Phong, which fails.
        PhongDTO phongDTO = phongMapper.toDto(phong);

        restPhongMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(phongDTO)))
            .andExpect(status().isBadRequest());

        List<Phong> phongList = phongRepository.findAll();
        assertThat(phongList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkLoaiIsRequired() throws Exception {
        int databaseSizeBeforeTest = phongRepository.findAll().size();
        // set the field null
        phong.setLoai(null);

        // Create the Phong, which fails.
        PhongDTO phongDTO = phongMapper.toDto(phong);

        restPhongMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(phongDTO)))
            .andExpect(status().isBadRequest());

        List<Phong> phongList = phongRepository.findAll();
        assertThat(phongList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkGiaIsRequired() throws Exception {
        int databaseSizeBeforeTest = phongRepository.findAll().size();
        // set the field null
        phong.setGia(null);

        // Create the Phong, which fails.
        PhongDTO phongDTO = phongMapper.toDto(phong);

        restPhongMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(phongDTO)))
            .andExpect(status().isBadRequest());

        List<Phong> phongList = phongRepository.findAll();
        assertThat(phongList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllPhongs() throws Exception {
        // Initialize the database
        phongRepository.saveAndFlush(phong);

        // Get all the phongList
        restPhongMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(phong.getId().intValue())))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)))
            .andExpect(jsonPath("$.[*].loai").value(hasItem(DEFAULT_LOAI)))
            .andExpect(jsonPath("$.[*].gia").value(hasItem(DEFAULT_GIA.doubleValue())));
    }

    @Test
    @Transactional
    void getPhong() throws Exception {
        // Initialize the database
        phongRepository.saveAndFlush(phong);

        // Get the phong
        restPhongMockMvc
            .perform(get(ENTITY_API_URL_ID, phong.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(phong.getId().intValue()))
            .andExpect(jsonPath("$.ten").value(DEFAULT_TEN))
            .andExpect(jsonPath("$.loai").value(DEFAULT_LOAI))
            .andExpect(jsonPath("$.gia").value(DEFAULT_GIA.doubleValue()));
    }

    @Test
    @Transactional
    void getPhongsByIdFiltering() throws Exception {
        // Initialize the database
        phongRepository.saveAndFlush(phong);

        Long id = phong.getId();

        defaultPhongShouldBeFound("id.equals=" + id);
        defaultPhongShouldNotBeFound("id.notEquals=" + id);

        defaultPhongShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultPhongShouldNotBeFound("id.greaterThan=" + id);

        defaultPhongShouldBeFound("id.lessThanOrEqual=" + id);
        defaultPhongShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllPhongsByTenIsEqualToSomething() throws Exception {
        // Initialize the database
        phongRepository.saveAndFlush(phong);

        // Get all the phongList where ten equals to DEFAULT_TEN
        defaultPhongShouldBeFound("ten.equals=" + DEFAULT_TEN);

        // Get all the phongList where ten equals to UPDATED_TEN
        defaultPhongShouldNotBeFound("ten.equals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    void getAllPhongsByTenIsNotEqualToSomething() throws Exception {
        // Initialize the database
        phongRepository.saveAndFlush(phong);

        // Get all the phongList where ten not equals to DEFAULT_TEN
        defaultPhongShouldNotBeFound("ten.notEquals=" + DEFAULT_TEN);

        // Get all the phongList where ten not equals to UPDATED_TEN
        defaultPhongShouldBeFound("ten.notEquals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    void getAllPhongsByTenIsInShouldWork() throws Exception {
        // Initialize the database
        phongRepository.saveAndFlush(phong);

        // Get all the phongList where ten in DEFAULT_TEN or UPDATED_TEN
        defaultPhongShouldBeFound("ten.in=" + DEFAULT_TEN + "," + UPDATED_TEN);

        // Get all the phongList where ten equals to UPDATED_TEN
        defaultPhongShouldNotBeFound("ten.in=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    void getAllPhongsByTenIsNullOrNotNull() throws Exception {
        // Initialize the database
        phongRepository.saveAndFlush(phong);

        // Get all the phongList where ten is not null
        defaultPhongShouldBeFound("ten.specified=true");

        // Get all the phongList where ten is null
        defaultPhongShouldNotBeFound("ten.specified=false");
    }

    @Test
    @Transactional
    void getAllPhongsByTenContainsSomething() throws Exception {
        // Initialize the database
        phongRepository.saveAndFlush(phong);

        // Get all the phongList where ten contains DEFAULT_TEN
        defaultPhongShouldBeFound("ten.contains=" + DEFAULT_TEN);

        // Get all the phongList where ten contains UPDATED_TEN
        defaultPhongShouldNotBeFound("ten.contains=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    void getAllPhongsByTenNotContainsSomething() throws Exception {
        // Initialize the database
        phongRepository.saveAndFlush(phong);

        // Get all the phongList where ten does not contain DEFAULT_TEN
        defaultPhongShouldNotBeFound("ten.doesNotContain=" + DEFAULT_TEN);

        // Get all the phongList where ten does not contain UPDATED_TEN
        defaultPhongShouldBeFound("ten.doesNotContain=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    void getAllPhongsByLoaiIsEqualToSomething() throws Exception {
        // Initialize the database
        phongRepository.saveAndFlush(phong);

        // Get all the phongList where loai equals to DEFAULT_LOAI
        defaultPhongShouldBeFound("loai.equals=" + DEFAULT_LOAI);

        // Get all the phongList where loai equals to UPDATED_LOAI
        defaultPhongShouldNotBeFound("loai.equals=" + UPDATED_LOAI);
    }

    @Test
    @Transactional
    void getAllPhongsByLoaiIsNotEqualToSomething() throws Exception {
        // Initialize the database
        phongRepository.saveAndFlush(phong);

        // Get all the phongList where loai not equals to DEFAULT_LOAI
        defaultPhongShouldNotBeFound("loai.notEquals=" + DEFAULT_LOAI);

        // Get all the phongList where loai not equals to UPDATED_LOAI
        defaultPhongShouldBeFound("loai.notEquals=" + UPDATED_LOAI);
    }

    @Test
    @Transactional
    void getAllPhongsByLoaiIsInShouldWork() throws Exception {
        // Initialize the database
        phongRepository.saveAndFlush(phong);

        // Get all the phongList where loai in DEFAULT_LOAI or UPDATED_LOAI
        defaultPhongShouldBeFound("loai.in=" + DEFAULT_LOAI + "," + UPDATED_LOAI);

        // Get all the phongList where loai equals to UPDATED_LOAI
        defaultPhongShouldNotBeFound("loai.in=" + UPDATED_LOAI);
    }

    @Test
    @Transactional
    void getAllPhongsByLoaiIsNullOrNotNull() throws Exception {
        // Initialize the database
        phongRepository.saveAndFlush(phong);

        // Get all the phongList where loai is not null
        defaultPhongShouldBeFound("loai.specified=true");

        // Get all the phongList where loai is null
        defaultPhongShouldNotBeFound("loai.specified=false");
    }

    @Test
    @Transactional
    void getAllPhongsByLoaiContainsSomething() throws Exception {
        // Initialize the database
        phongRepository.saveAndFlush(phong);

        // Get all the phongList where loai contains DEFAULT_LOAI
        defaultPhongShouldBeFound("loai.contains=" + DEFAULT_LOAI);

        // Get all the phongList where loai contains UPDATED_LOAI
        defaultPhongShouldNotBeFound("loai.contains=" + UPDATED_LOAI);
    }

    @Test
    @Transactional
    void getAllPhongsByLoaiNotContainsSomething() throws Exception {
        // Initialize the database
        phongRepository.saveAndFlush(phong);

        // Get all the phongList where loai does not contain DEFAULT_LOAI
        defaultPhongShouldNotBeFound("loai.doesNotContain=" + DEFAULT_LOAI);

        // Get all the phongList where loai does not contain UPDATED_LOAI
        defaultPhongShouldBeFound("loai.doesNotContain=" + UPDATED_LOAI);
    }

    @Test
    @Transactional
    void getAllPhongsByGiaIsEqualToSomething() throws Exception {
        // Initialize the database
        phongRepository.saveAndFlush(phong);

        // Get all the phongList where gia equals to DEFAULT_GIA
        defaultPhongShouldBeFound("gia.equals=" + DEFAULT_GIA);

        // Get all the phongList where gia equals to UPDATED_GIA
        defaultPhongShouldNotBeFound("gia.equals=" + UPDATED_GIA);
    }

    @Test
    @Transactional
    void getAllPhongsByGiaIsNotEqualToSomething() throws Exception {
        // Initialize the database
        phongRepository.saveAndFlush(phong);

        // Get all the phongList where gia not equals to DEFAULT_GIA
        defaultPhongShouldNotBeFound("gia.notEquals=" + DEFAULT_GIA);

        // Get all the phongList where gia not equals to UPDATED_GIA
        defaultPhongShouldBeFound("gia.notEquals=" + UPDATED_GIA);
    }

    @Test
    @Transactional
    void getAllPhongsByGiaIsInShouldWork() throws Exception {
        // Initialize the database
        phongRepository.saveAndFlush(phong);

        // Get all the phongList where gia in DEFAULT_GIA or UPDATED_GIA
        defaultPhongShouldBeFound("gia.in=" + DEFAULT_GIA + "," + UPDATED_GIA);

        // Get all the phongList where gia equals to UPDATED_GIA
        defaultPhongShouldNotBeFound("gia.in=" + UPDATED_GIA);
    }

    @Test
    @Transactional
    void getAllPhongsByGiaIsNullOrNotNull() throws Exception {
        // Initialize the database
        phongRepository.saveAndFlush(phong);

        // Get all the phongList where gia is not null
        defaultPhongShouldBeFound("gia.specified=true");

        // Get all the phongList where gia is null
        defaultPhongShouldNotBeFound("gia.specified=false");
    }

    @Test
    @Transactional
    void getAllPhongsByGiaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        phongRepository.saveAndFlush(phong);

        // Get all the phongList where gia is greater than or equal to DEFAULT_GIA
        defaultPhongShouldBeFound("gia.greaterThanOrEqual=" + DEFAULT_GIA);

        // Get all the phongList where gia is greater than or equal to UPDATED_GIA
        defaultPhongShouldNotBeFound("gia.greaterThanOrEqual=" + UPDATED_GIA);
    }

    @Test
    @Transactional
    void getAllPhongsByGiaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        phongRepository.saveAndFlush(phong);

        // Get all the phongList where gia is less than or equal to DEFAULT_GIA
        defaultPhongShouldBeFound("gia.lessThanOrEqual=" + DEFAULT_GIA);

        // Get all the phongList where gia is less than or equal to SMALLER_GIA
        defaultPhongShouldNotBeFound("gia.lessThanOrEqual=" + SMALLER_GIA);
    }

    @Test
    @Transactional
    void getAllPhongsByGiaIsLessThanSomething() throws Exception {
        // Initialize the database
        phongRepository.saveAndFlush(phong);

        // Get all the phongList where gia is less than DEFAULT_GIA
        defaultPhongShouldNotBeFound("gia.lessThan=" + DEFAULT_GIA);

        // Get all the phongList where gia is less than UPDATED_GIA
        defaultPhongShouldBeFound("gia.lessThan=" + UPDATED_GIA);
    }

    @Test
    @Transactional
    void getAllPhongsByGiaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        phongRepository.saveAndFlush(phong);

        // Get all the phongList where gia is greater than DEFAULT_GIA
        defaultPhongShouldNotBeFound("gia.greaterThan=" + DEFAULT_GIA);

        // Get all the phongList where gia is greater than SMALLER_GIA
        defaultPhongShouldBeFound("gia.greaterThan=" + SMALLER_GIA);
    }

    @Test
    @Transactional
    void getAllPhongsByKhachSanIsEqualToSomething() throws Exception {
        // Initialize the database
        phongRepository.saveAndFlush(phong);
        KhachSan khachSan = KhachSanResourceIT.createEntity(em);
        khachSan.setId(2L);
        khachSan.setTen("DDDDDDDDDDDDD");
        khachSan = em.merge(khachSan);
        em.flush();
        phong.setKhachSan(khachSan);
        phongRepository.saveAndFlush(phong);
        Long khachSanId = khachSan.getId();

        // Get all the phongList where khachSan equals to khachSanId
        defaultPhongShouldBeFound("khachSanId.equals=" + khachSanId);

        // Get all the phongList where khachSan equals to (khachSanId + 1)
        defaultPhongShouldNotBeFound("khachSanId.equals=" + (khachSanId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultPhongShouldBeFound(String filter) throws Exception {
        restPhongMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(phong.getId().intValue())))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)))
            .andExpect(jsonPath("$.[*].loai").value(hasItem(DEFAULT_LOAI)))
            .andExpect(jsonPath("$.[*].gia").value(hasItem(DEFAULT_GIA.doubleValue())));

        // Check, that the count call also returns 1
        restPhongMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultPhongShouldNotBeFound(String filter) throws Exception {
        restPhongMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restPhongMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingPhong() throws Exception {
        // Get the phong
        restPhongMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewPhong() throws Exception {
        // Initialize the database
        phongRepository.saveAndFlush(phong);

        int databaseSizeBeforeUpdate = phongRepository.findAll().size();

        // Update the phong
        Phong updatedPhong = phongRepository.findById(phong.getId()).get();
        // Disconnect from session so that the updates on updatedPhong are not directly saved in db
        em.detach(updatedPhong);
        updatedPhong.ten(UPDATED_TEN).loai(UPDATED_LOAI).gia(UPDATED_GIA);
        PhongDTO phongDTO = phongMapper.toDto(updatedPhong);

        restPhongMockMvc
            .perform(
                put(ENTITY_API_URL_ID, phongDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(phongDTO))
            )
            .andExpect(status().isOk());

        // Validate the Phong in the database
        List<Phong> phongList = phongRepository.findAll();
        assertThat(phongList).hasSize(databaseSizeBeforeUpdate);
        Phong testPhong = phongList.get(phongList.size() - 1);
        assertThat(testPhong.getTen()).isEqualTo(UPDATED_TEN);
        assertThat(testPhong.getLoai()).isEqualTo(UPDATED_LOAI);
        assertThat(testPhong.getGia()).isEqualTo(UPDATED_GIA);
    }

    @Test
    @Transactional
    void putNonExistingPhong() throws Exception {
        int databaseSizeBeforeUpdate = phongRepository.findAll().size();
        phong.setId(count.incrementAndGet());

        // Create the Phong
        PhongDTO phongDTO = phongMapper.toDto(phong);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPhongMockMvc
            .perform(
                put(ENTITY_API_URL_ID, phongDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(phongDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Phong in the database
        List<Phong> phongList = phongRepository.findAll();
        assertThat(phongList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchPhong() throws Exception {
        int databaseSizeBeforeUpdate = phongRepository.findAll().size();
        phong.setId(count.incrementAndGet());

        // Create the Phong
        PhongDTO phongDTO = phongMapper.toDto(phong);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPhongMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(phongDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Phong in the database
        List<Phong> phongList = phongRepository.findAll();
        assertThat(phongList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamPhong() throws Exception {
        int databaseSizeBeforeUpdate = phongRepository.findAll().size();
        phong.setId(count.incrementAndGet());

        // Create the Phong
        PhongDTO phongDTO = phongMapper.toDto(phong);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPhongMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(phongDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Phong in the database
        List<Phong> phongList = phongRepository.findAll();
        assertThat(phongList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdatePhongWithPatch() throws Exception {
        // Initialize the database
        phongRepository.saveAndFlush(phong);

        int databaseSizeBeforeUpdate = phongRepository.findAll().size();

        // Update the phong using partial update
        Phong partialUpdatedPhong = new Phong();
        partialUpdatedPhong.setId(phong.getId());

        partialUpdatedPhong.loai(UPDATED_LOAI);

        restPhongMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPhong.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPhong))
            )
            .andExpect(status().isOk());

        // Validate the Phong in the database
        List<Phong> phongList = phongRepository.findAll();
        assertThat(phongList).hasSize(databaseSizeBeforeUpdate);
        Phong testPhong = phongList.get(phongList.size() - 1);
        assertThat(testPhong.getTen()).isEqualTo(DEFAULT_TEN);
        assertThat(testPhong.getLoai()).isEqualTo(UPDATED_LOAI);
        assertThat(testPhong.getGia()).isEqualTo(DEFAULT_GIA);
    }

    @Test
    @Transactional
    void fullUpdatePhongWithPatch() throws Exception {
        // Initialize the database
        phongRepository.saveAndFlush(phong);

        int databaseSizeBeforeUpdate = phongRepository.findAll().size();

        // Update the phong using partial update
        Phong partialUpdatedPhong = new Phong();
        partialUpdatedPhong.setId(phong.getId());

        partialUpdatedPhong.ten(UPDATED_TEN).loai(UPDATED_LOAI).gia(UPDATED_GIA);

        restPhongMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPhong.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPhong))
            )
            .andExpect(status().isOk());

        // Validate the Phong in the database
        List<Phong> phongList = phongRepository.findAll();
        assertThat(phongList).hasSize(databaseSizeBeforeUpdate);
        Phong testPhong = phongList.get(phongList.size() - 1);
        assertThat(testPhong.getTen()).isEqualTo(UPDATED_TEN);
        assertThat(testPhong.getLoai()).isEqualTo(UPDATED_LOAI);
        assertThat(testPhong.getGia()).isEqualTo(UPDATED_GIA);
    }

    @Test
    @Transactional
    void patchNonExistingPhong() throws Exception {
        int databaseSizeBeforeUpdate = phongRepository.findAll().size();
        phong.setId(count.incrementAndGet());

        // Create the Phong
        PhongDTO phongDTO = phongMapper.toDto(phong);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPhongMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, phongDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(phongDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Phong in the database
        List<Phong> phongList = phongRepository.findAll();
        assertThat(phongList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchPhong() throws Exception {
        int databaseSizeBeforeUpdate = phongRepository.findAll().size();
        phong.setId(count.incrementAndGet());

        // Create the Phong
        PhongDTO phongDTO = phongMapper.toDto(phong);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPhongMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(phongDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Phong in the database
        List<Phong> phongList = phongRepository.findAll();
        assertThat(phongList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamPhong() throws Exception {
        int databaseSizeBeforeUpdate = phongRepository.findAll().size();
        phong.setId(count.incrementAndGet());

        // Create the Phong
        PhongDTO phongDTO = phongMapper.toDto(phong);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPhongMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(phongDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Phong in the database
        List<Phong> phongList = phongRepository.findAll();
        assertThat(phongList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deletePhong() throws Exception {
        // Initialize the database
        phongRepository.saveAndFlush(phong);

        int databaseSizeBeforeDelete = phongRepository.findAll().size();

        // Delete the phong
        restPhongMockMvc
            .perform(delete(ENTITY_API_URL_ID, phong.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Phong> phongList = phongRepository.findAll();
        assertThat(phongList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
