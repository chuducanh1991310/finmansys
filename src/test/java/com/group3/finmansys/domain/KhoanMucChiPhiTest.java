package com.group3.finmansys.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.group3.finmansys.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class KhoanMucChiPhiTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(KhoanMucChiPhi.class);
        KhoanMucChiPhi khoanMucChiPhi1 = new KhoanMucChiPhi();
        khoanMucChiPhi1.setId(1L);
        KhoanMucChiPhi khoanMucChiPhi2 = new KhoanMucChiPhi();
        khoanMucChiPhi2.setId(khoanMucChiPhi1.getId());
        assertThat(khoanMucChiPhi1).isEqualTo(khoanMucChiPhi2);
        khoanMucChiPhi2.setId(2L);
        assertThat(khoanMucChiPhi1).isNotEqualTo(khoanMucChiPhi2);
        khoanMucChiPhi1.setId(null);
        assertThat(khoanMucChiPhi1).isNotEqualTo(khoanMucChiPhi2);
    }
}
