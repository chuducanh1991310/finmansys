package com.group3.finmansys.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.group3.finmansys.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class DichVuTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DichVu.class);
        DichVu dichVu1 = new DichVu();
        dichVu1.setId(1L);
        DichVu dichVu2 = new DichVu();
        dichVu2.setId(dichVu1.getId());
        assertThat(dichVu1).isEqualTo(dichVu2);
        dichVu2.setId(2L);
        assertThat(dichVu1).isNotEqualTo(dichVu2);
        dichVu1.setId(null);
        assertThat(dichVu1).isNotEqualTo(dichVu2);
    }
}
