package com.group3.finmansys.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.group3.finmansys.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class MaKeToanTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(MaKeToan.class);
        MaKeToan maKeToan1 = new MaKeToan();
        maKeToan1.setId(1L);
        MaKeToan maKeToan2 = new MaKeToan();
        maKeToan2.setId(maKeToan1.getId());
        assertThat(maKeToan1).isEqualTo(maKeToan2);
        maKeToan2.setId(2L);
        assertThat(maKeToan1).isNotEqualTo(maKeToan2);
        maKeToan1.setId(null);
        assertThat(maKeToan1).isNotEqualTo(maKeToan2);
    }
}
