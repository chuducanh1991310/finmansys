package com.group3.finmansys.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.group3.finmansys.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class ChiTietTangTSCDDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ChiTietTangTSCDDTO.class);
        ChiTietTangTSCDDTO chiTietTangTSCDDTO1 = new ChiTietTangTSCDDTO();
        chiTietTangTSCDDTO1.setId(1L);
        ChiTietTangTSCDDTO chiTietTangTSCDDTO2 = new ChiTietTangTSCDDTO();
        assertThat(chiTietTangTSCDDTO1).isNotEqualTo(chiTietTangTSCDDTO2);
        chiTietTangTSCDDTO2.setId(chiTietTangTSCDDTO1.getId());
        assertThat(chiTietTangTSCDDTO1).isEqualTo(chiTietTangTSCDDTO2);
        chiTietTangTSCDDTO2.setId(2L);
        assertThat(chiTietTangTSCDDTO1).isNotEqualTo(chiTietTangTSCDDTO2);
        chiTietTangTSCDDTO1.setId(null);
        assertThat(chiTietTangTSCDDTO1).isNotEqualTo(chiTietTangTSCDDTO2);
    }
}
