package com.group3.finmansys.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.group3.finmansys.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class LoaiTaiSanDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(LoaiTaiSanDTO.class);
        LoaiTaiSanDTO loaiTaiSanDTO1 = new LoaiTaiSanDTO();
        loaiTaiSanDTO1.setId(1L);
        LoaiTaiSanDTO loaiTaiSanDTO2 = new LoaiTaiSanDTO();
        assertThat(loaiTaiSanDTO1).isNotEqualTo(loaiTaiSanDTO2);
        loaiTaiSanDTO2.setId(loaiTaiSanDTO1.getId());
        assertThat(loaiTaiSanDTO1).isEqualTo(loaiTaiSanDTO2);
        loaiTaiSanDTO2.setId(2L);
        assertThat(loaiTaiSanDTO1).isNotEqualTo(loaiTaiSanDTO2);
        loaiTaiSanDTO1.setId(null);
        assertThat(loaiTaiSanDTO1).isNotEqualTo(loaiTaiSanDTO2);
    }
}
