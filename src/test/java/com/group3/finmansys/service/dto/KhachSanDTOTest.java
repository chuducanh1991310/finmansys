package com.group3.finmansys.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.group3.finmansys.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class KhachSanDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(KhachSanDTO.class);
        KhachSanDTO khachSanDTO1 = new KhachSanDTO();
        khachSanDTO1.setId(1L);
        KhachSanDTO khachSanDTO2 = new KhachSanDTO();
        assertThat(khachSanDTO1).isNotEqualTo(khachSanDTO2);
        khachSanDTO2.setId(khachSanDTO1.getId());
        assertThat(khachSanDTO1).isEqualTo(khachSanDTO2);
        khachSanDTO2.setId(2L);
        assertThat(khachSanDTO1).isNotEqualTo(khachSanDTO2);
        khachSanDTO1.setId(null);
        assertThat(khachSanDTO1).isNotEqualTo(khachSanDTO2);
    }
}
