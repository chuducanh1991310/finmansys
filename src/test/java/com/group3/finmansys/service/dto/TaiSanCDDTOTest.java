package com.group3.finmansys.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.group3.finmansys.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class TaiSanCDDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(TaiSanCDDTO.class);
        TaiSanCDDTO taiSanCDDTO1 = new TaiSanCDDTO();
        taiSanCDDTO1.setId(1L);
        TaiSanCDDTO taiSanCDDTO2 = new TaiSanCDDTO();
        assertThat(taiSanCDDTO1).isNotEqualTo(taiSanCDDTO2);
        taiSanCDDTO2.setId(taiSanCDDTO1.getId());
        assertThat(taiSanCDDTO1).isEqualTo(taiSanCDDTO2);
        taiSanCDDTO2.setId(2L);
        assertThat(taiSanCDDTO1).isNotEqualTo(taiSanCDDTO2);
        taiSanCDDTO1.setId(null);
        assertThat(taiSanCDDTO1).isNotEqualTo(taiSanCDDTO2);
    }
}
