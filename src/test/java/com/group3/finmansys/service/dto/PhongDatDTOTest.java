package com.group3.finmansys.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.group3.finmansys.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class PhongDatDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PhongDatDTO.class);
        PhongDatDTO phongDatDTO1 = new PhongDatDTO();
        phongDatDTO1.setId(1L);
        PhongDatDTO phongDatDTO2 = new PhongDatDTO();
        assertThat(phongDatDTO1).isNotEqualTo(phongDatDTO2);
        phongDatDTO2.setId(phongDatDTO1.getId());
        assertThat(phongDatDTO1).isEqualTo(phongDatDTO2);
        phongDatDTO2.setId(2L);
        assertThat(phongDatDTO1).isNotEqualTo(phongDatDTO2);
        phongDatDTO1.setId(null);
        assertThat(phongDatDTO1).isNotEqualTo(phongDatDTO2);
    }
}
