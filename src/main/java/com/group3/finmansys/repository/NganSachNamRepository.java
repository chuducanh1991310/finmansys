package com.group3.finmansys.repository;

import com.group3.finmansys.domain.NganSachNam;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the NganSachNam entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NganSachNamRepository extends JpaRepository<NganSachNam, Long>, JpaSpecificationExecutor<NganSachNam> {}
