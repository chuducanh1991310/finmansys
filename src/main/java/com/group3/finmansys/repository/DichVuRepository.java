package com.group3.finmansys.repository;

import com.group3.finmansys.domain.DichVu;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the DichVu entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DichVuRepository extends JpaRepository<DichVu, Long>, JpaSpecificationExecutor<DichVu> {}
