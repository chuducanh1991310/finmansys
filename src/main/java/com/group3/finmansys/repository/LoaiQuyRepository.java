package com.group3.finmansys.repository;

import com.group3.finmansys.domain.LoaiQuy;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the LoaiQuy entity.
 */
@SuppressWarnings("unused")
@Repository
public interface LoaiQuyRepository extends JpaRepository<LoaiQuy, Long>, JpaSpecificationExecutor<LoaiQuy> {}
