package com.group3.finmansys.repository;

import com.group3.finmansys.domain.Phong;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Phong entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PhongRepository extends JpaRepository<Phong, Long>, JpaSpecificationExecutor<Phong> {}
