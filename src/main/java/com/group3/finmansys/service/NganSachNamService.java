package com.group3.finmansys.service;

import com.group3.finmansys.service.dto.NganSachNamDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.group3.finmansys.domain.NganSachNam}.
 */
public interface NganSachNamService {
    /**
     * Save a nganSachNam.
     *
     * @param nganSachNamDTO the entity to save.
     * @return the persisted entity.
     */
    NganSachNamDTO save(NganSachNamDTO nganSachNamDTO);

    /**
     * Partially updates a nganSachNam.
     *
     * @param nganSachNamDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<NganSachNamDTO> partialUpdate(NganSachNamDTO nganSachNamDTO);

    /**
     * Get all the nganSachNams.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<NganSachNamDTO> findAll(Pageable pageable);

    /**
     * Get the "id" nganSachNam.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<NganSachNamDTO> findOne(Long id);

    /**
     * Delete the "id" nganSachNam.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    Optional<NganSachNamDTO> getChiTieu(Long id);
}
