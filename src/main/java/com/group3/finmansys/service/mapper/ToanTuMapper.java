package com.group3.finmansys.service.mapper;

import com.group3.finmansys.domain.*;
import com.group3.finmansys.service.dto.ToanTuDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link ToanTu} and its DTO {@link ToanTuDTO}.
 */
@Mapper(componentModel = "spring", uses = { MaKeToanMapper.class, CongThucMapper.class })
public interface ToanTuMapper extends EntityMapper<ToanTuDTO, ToanTu> {
    @Mapping(target = "maKeToan", source = "maKeToan", qualifiedByName = "soHieu")
    @Mapping(target = "congThuc", source = "congThuc", qualifiedByName = "maSo")
    ToanTuDTO toDto(ToanTu s);
}
