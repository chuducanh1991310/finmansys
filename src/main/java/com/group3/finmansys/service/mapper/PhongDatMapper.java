package com.group3.finmansys.service.mapper;

import com.group3.finmansys.domain.*;
import com.group3.finmansys.service.dto.PhongDatDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link PhongDat} and its DTO {@link PhongDatDTO}.
 */
@Mapper(componentModel = "spring", uses = { PhongMapper.class, PhongDuocDatMapper.class })
public interface PhongDatMapper extends EntityMapper<PhongDatDTO, PhongDat> {
    @Mapping(target = "phong", source = "phong", qualifiedByName = "ten")
    @Mapping(target = "phongDuocDat", source = "phongDuocDat", qualifiedByName = "id")
    PhongDatDTO toDto(PhongDat s);
}
