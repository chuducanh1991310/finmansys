package com.group3.finmansys.service.mapper;

import com.group3.finmansys.domain.*;
import com.group3.finmansys.service.dto.DichVuDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link DichVu} and its DTO {@link DichVuDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface DichVuMapper extends EntityMapper<DichVuDTO, DichVu> {
    @Named("ten")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "ten", source = "ten")
    DichVuDTO toDtoTen(DichVu dichVu);
}
