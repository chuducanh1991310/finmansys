package com.group3.finmansys.service.mapper;

import com.group3.finmansys.domain.*;
import com.group3.finmansys.service.dto.LoaiTaiSanDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link LoaiTaiSan} and its DTO {@link LoaiTaiSanDTO}.
 */
@Mapper(componentModel = "spring", uses = { MaKeToanMapper.class })
public interface LoaiTaiSanMapper extends EntityMapper<LoaiTaiSanDTO, LoaiTaiSan> {
    @Mapping(target = "maKeToan", source = "maKeToan")
    LoaiTaiSanDTO toDto(LoaiTaiSan s);

    @Named("ten")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "ten", source = "ten")
    LoaiTaiSanDTO toDtoTen(LoaiTaiSan loaiTaiSan);
}
