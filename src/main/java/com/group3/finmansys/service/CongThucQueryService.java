package com.group3.finmansys.service;

import com.group3.finmansys.domain.*; // for static metamodels
import com.group3.finmansys.domain.CongThuc;
import com.group3.finmansys.repository.CongThucRepository;
import com.group3.finmansys.repository.KhoanPhaiThuRepository;
import com.group3.finmansys.repository.KhoanPhaiTraRepository;
import com.group3.finmansys.service.criteria.CongThucCriteria;
import com.group3.finmansys.service.dto.CongThucDTO;
import com.group3.finmansys.service.mapper.CongThucMapper;
import java.time.Instant;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link CongThuc} entities in the database.
 * The main input is a {@link CongThucCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link CongThucDTO} or a {@link Page} of {@link CongThucDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class CongThucQueryService extends QueryService<CongThuc> {

    private final Logger log = LoggerFactory.getLogger(CongThucQueryService.class);

    private final CongThucRepository congThucRepository;

    private final CongThucMapper congThucMapper;

    private final KhoanPhaiThuRepository khoanPhaiThuRepository;

    private final KhoanPhaiTraRepository khoanPhaiTraRepository;

    public CongThucQueryService(
        CongThucRepository congThucRepository,
        CongThucMapper congThucMapper,
        KhoanPhaiThuRepository khoanPhaiThuRepository,
        KhoanPhaiTraRepository khoanPhaiTraRepository
    ) {
        this.congThucRepository = congThucRepository;
        this.congThucMapper = congThucMapper;
        this.khoanPhaiThuRepository = khoanPhaiThuRepository;
        this.khoanPhaiTraRepository = khoanPhaiTraRepository;
    }

    /**
     * Return a {@link List} of {@link CongThucDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<CongThucDTO> findByCriteria(CongThucCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<CongThuc> specification = createSpecification(criteria);
        return congThucMapper.toDto(congThucRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link CongThucDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<CongThucDTO> findByCriteria(CongThucCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<CongThuc> specification = createSpecification(criteria);
        return congThucRepository.findAll(specification, page).map(congThucMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(CongThucCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<CongThuc> specification = createSpecification(criteria);
        return congThucRepository.count(specification);
    }

    @Transactional(readOnly = true)
    public List<CongThucDTO> getBangCanDoiKeToan(String startDate, String endDate) {
        Instant start = Instant.parse(startDate);
        Instant end = Instant.parse(endDate);

        List<CongThuc> congThucList = this.congThucRepository.findAll();
        for (int cap = 4; cap >= 1; cap--) {
            congThucList = calc(start, end, congThucList, cap);
        }

        return congThucMapper.toDto(congThucList);
    }

    private List<CongThuc> calc(Instant startDate, Instant endDate, List<CongThuc> congThucList, int cap) {
        for (CongThuc congThuc : congThucList) {
            if (congThuc.getCap() == cap) {
                if (congThuc.getCapCons().size() == 0) {
                    double sumDauKy = 0;
                    double sumCuoiKy = 0;
                    for (ToanTu toanTu : congThuc.getToanTus()) {
                        List<KhoanPhaiThu> khoanPhaiThuList =
                            this.khoanPhaiThuRepository.getBangCanDoiKeToan(startDate, toanTu.getMaKeToan().getSoHieu());
                        List<KhoanPhaiTra> khoanPhaiTraList =
                            this.khoanPhaiTraRepository.getBangCanDoiKeToan(startDate, toanTu.getMaKeToan().getSoHieu());
                        for (KhoanPhaiThu khoanPhaiThu : khoanPhaiThuList) {
                            if (toanTu.getDau().equals("+")) {
                                sumDauKy += khoanPhaiThu.getSoTien();
                            } else {
                                sumDauKy -= khoanPhaiThu.getSoTien();
                            }
                        }
                        for (KhoanPhaiTra khoanPhaiTra : khoanPhaiTraList) {
                            if (toanTu.getDau().equals("+")) {
                                sumDauKy -= khoanPhaiTra.getSoTien();
                            } else {
                                sumDauKy += khoanPhaiTra.getSoTien();
                            }
                        }

                        khoanPhaiThuList = this.khoanPhaiThuRepository.getBangCanDoiKeToan(endDate, toanTu.getMaKeToan().getSoHieu());
                        khoanPhaiTraList = this.khoanPhaiTraRepository.getBangCanDoiKeToan(endDate, toanTu.getMaKeToan().getSoHieu());

                        for (KhoanPhaiThu khoanPhaiThu : khoanPhaiThuList) {
                            if (toanTu.getDau().equals("+")) {
                                sumCuoiKy += khoanPhaiThu.getSoTien();
                            } else {
                                sumCuoiKy -= khoanPhaiThu.getSoTien();
                            }
                        }
                        for (KhoanPhaiTra khoanPhaiTra : khoanPhaiTraList) {
                            if (toanTu.getDau().equals("+")) {
                                sumCuoiKy -= khoanPhaiTra.getSoTien();
                            } else {
                                sumCuoiKy += khoanPhaiTra.getSoTien();
                            }
                        }
                    }
                    congThuc.setSoDauKy(sumDauKy);
                    congThuc.setSoCuoiKy(sumCuoiKy);
                } else {
                    if (congThuc.getCapCha() != null) {
                        double sumDauKy = 0;
                        double sumCuoiKy = 0;
                        for (CongThuc ctCon : congThuc.getCapCons()) {
                            sumDauKy += ctCon.getSoDauKy();
                            sumCuoiKy += ctCon.getSoCuoiKy();
                        }
                        congThuc.setSoDauKy(sumDauKy);
                        congThuc.setSoCuoiKy(sumCuoiKy);
                    }
                }
            }
        }
        for (CongThuc congThuc : congThucList) {
            if (congThuc.getCap() == cap && congThuc.getCapCons().size() > 0 && congThuc.getCapCha() == null) {
                double sumDauKy = 0;
                double sumCuoiKy = 0;
                for (CongThuc ctCon : congThuc.getCapCons()) {
                    sumDauKy += ctCon.getSoDauKy();
                    sumCuoiKy += ctCon.getSoCuoiKy();
                }
                congThuc.setSoDauKy(sumDauKy);
                congThuc.setSoCuoiKy(sumCuoiKy);
            }
        }
        return congThucList;
    }

    /**
     * Function to convert {@link CongThucCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<CongThuc> createSpecification(CongThucCriteria criteria) {
        Specification<CongThuc> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), CongThuc_.id));
            }
            if (criteria.getMaSo() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getMaSo(), CongThuc_.maSo));
            }
            if (criteria.getTenChiTieu() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTenChiTieu(), CongThuc_.tenChiTieu));
            }
            if (criteria.getCap() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCap(), CongThuc_.cap));
            }
            if (criteria.getSoDauKy() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSoDauKy(), CongThuc_.soDauKy));
            }
            if (criteria.getSoCuoiKy() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSoCuoiKy(), CongThuc_.soCuoiKy));
            }
            if (criteria.getToanTuId() != null) {
                specification =
                    specification.and(
                        buildSpecification(criteria.getToanTuId(), root -> root.join(CongThuc_.toanTus, JoinType.LEFT).get(ToanTu_.id))
                    );
            }
            if (criteria.getCapChaId() != null) {
                specification =
                    specification.and(
                        buildSpecification(criteria.getCapChaId(), root -> root.join(CongThuc_.capCha, JoinType.LEFT).get(CongThuc_.id))
                    );
            }
            if (criteria.getCapConId() != null) {
                specification =
                    specification.and(
                        buildSpecification(criteria.getCapConId(), root -> root.join(CongThuc_.capCons, JoinType.LEFT).get(CongThuc_.id))
                    );
            }
        }
        return specification;
    }
}
