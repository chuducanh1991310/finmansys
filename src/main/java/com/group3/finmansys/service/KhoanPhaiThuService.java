package com.group3.finmansys.service;

import com.group3.finmansys.service.dto.KhoanPhaiThuDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.group3.finmansys.domain.KhoanPhaiThu}.
 */
public interface KhoanPhaiThuService {
    /**
     * Save a khoanPhaiThu.
     *
     * @param khoanPhaiThuDTO the entity to save.
     * @return the persisted entity.
     */
    KhoanPhaiThuDTO save(KhoanPhaiThuDTO khoanPhaiThuDTO);

    /**
     * Partially updates a khoanPhaiThu.
     *
     * @param khoanPhaiThuDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<KhoanPhaiThuDTO> partialUpdate(KhoanPhaiThuDTO khoanPhaiThuDTO);

    /**
     * Get all the khoanPhaiThus.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<KhoanPhaiThuDTO> findAll(Pageable pageable);

    /**
     * Get the "id" khoanPhaiThu.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<KhoanPhaiThuDTO> findOne(Long id);

    /**
     * Delete the "id" khoanPhaiThu.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
