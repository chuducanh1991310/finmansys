package com.group3.finmansys.service.impl;

import com.group3.finmansys.domain.KhoanMucChiPhi;
import com.group3.finmansys.repository.KhoanMucChiPhiRepository;
import com.group3.finmansys.service.KhoanMucChiPhiService;
import com.group3.finmansys.service.dto.KhoanMucChiPhiDTO;
import com.group3.finmansys.service.mapper.KhoanMucChiPhiMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link KhoanMucChiPhi}.
 */
@Service
@Transactional
public class KhoanMucChiPhiServiceImpl implements KhoanMucChiPhiService {

    private final Logger log = LoggerFactory.getLogger(KhoanMucChiPhiServiceImpl.class);

    private final KhoanMucChiPhiRepository khoanMucChiPhiRepository;

    private final KhoanMucChiPhiMapper khoanMucChiPhiMapper;

    public KhoanMucChiPhiServiceImpl(KhoanMucChiPhiRepository khoanMucChiPhiRepository, KhoanMucChiPhiMapper khoanMucChiPhiMapper) {
        this.khoanMucChiPhiRepository = khoanMucChiPhiRepository;
        this.khoanMucChiPhiMapper = khoanMucChiPhiMapper;
    }

    @Override
    public KhoanMucChiPhiDTO save(KhoanMucChiPhiDTO khoanMucChiPhiDTO) {
        log.debug("Request to save KhoanMucChiPhi : {}", khoanMucChiPhiDTO);
        KhoanMucChiPhi khoanMucChiPhi = khoanMucChiPhiMapper.toEntity(khoanMucChiPhiDTO);
        khoanMucChiPhi = khoanMucChiPhiRepository.save(khoanMucChiPhi);
        return khoanMucChiPhiMapper.toDto(khoanMucChiPhi);
    }

    @Override
    public Optional<KhoanMucChiPhiDTO> partialUpdate(KhoanMucChiPhiDTO khoanMucChiPhiDTO) {
        log.debug("Request to partially update KhoanMucChiPhi : {}", khoanMucChiPhiDTO);

        return khoanMucChiPhiRepository
            .findById(khoanMucChiPhiDTO.getId())
            .map(
                existingKhoanMucChiPhi -> {
                    khoanMucChiPhiMapper.partialUpdate(existingKhoanMucChiPhi, khoanMucChiPhiDTO);
                    return existingKhoanMucChiPhi;
                }
            )
            .map(khoanMucChiPhiRepository::save)
            .map(khoanMucChiPhiMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<KhoanMucChiPhiDTO> findAll(Pageable pageable) {
        log.debug("Request to get all KhoanMucChiPhis");
        return khoanMucChiPhiRepository.findAll(pageable).map(khoanMucChiPhiMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<KhoanMucChiPhiDTO> findOne(Long id) {
        log.debug("Request to get KhoanMucChiPhi : {}", id);
        return khoanMucChiPhiRepository.findById(id).map(khoanMucChiPhiMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete KhoanMucChiPhi : {}", id);
        khoanMucChiPhiRepository.deleteById(id);
    }
}
