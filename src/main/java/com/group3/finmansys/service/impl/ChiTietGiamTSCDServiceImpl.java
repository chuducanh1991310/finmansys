package com.group3.finmansys.service.impl;

import com.group3.finmansys.domain.ChiTietGiamTSCD;
import com.group3.finmansys.repository.ChiTietGiamTSCDRepository;
import com.group3.finmansys.service.ChiTietGiamTSCDService;
import com.group3.finmansys.service.dto.ChiTietGiamTSCDDTO;
import com.group3.finmansys.service.mapper.ChiTietGiamTSCDMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link ChiTietGiamTSCD}.
 */
@Service
@Transactional
public class ChiTietGiamTSCDServiceImpl implements ChiTietGiamTSCDService {

    private final Logger log = LoggerFactory.getLogger(ChiTietGiamTSCDServiceImpl.class);

    private final ChiTietGiamTSCDRepository chiTietGiamTSCDRepository;

    private final ChiTietGiamTSCDMapper chiTietGiamTSCDMapper;

    public ChiTietGiamTSCDServiceImpl(ChiTietGiamTSCDRepository chiTietGiamTSCDRepository, ChiTietGiamTSCDMapper chiTietGiamTSCDMapper) {
        this.chiTietGiamTSCDRepository = chiTietGiamTSCDRepository;
        this.chiTietGiamTSCDMapper = chiTietGiamTSCDMapper;
    }

    @Override
    public ChiTietGiamTSCDDTO save(ChiTietGiamTSCDDTO chiTietGiamTSCDDTO) {
        log.debug("Request to save ChiTietGiamTSCD : {}", chiTietGiamTSCDDTO);
        ChiTietGiamTSCD chiTietGiamTSCD = chiTietGiamTSCDMapper.toEntity(chiTietGiamTSCDDTO);
        chiTietGiamTSCD = chiTietGiamTSCDRepository.save(chiTietGiamTSCD);
        return chiTietGiamTSCDMapper.toDto(chiTietGiamTSCD);
    }

    @Override
    public Optional<ChiTietGiamTSCDDTO> partialUpdate(ChiTietGiamTSCDDTO chiTietGiamTSCDDTO) {
        log.debug("Request to partially update ChiTietGiamTSCD : {}", chiTietGiamTSCDDTO);

        return chiTietGiamTSCDRepository
            .findById(chiTietGiamTSCDDTO.getId())
            .map(
                existingChiTietGiamTSCD -> {
                    chiTietGiamTSCDMapper.partialUpdate(existingChiTietGiamTSCD, chiTietGiamTSCDDTO);
                    return existingChiTietGiamTSCD;
                }
            )
            .map(chiTietGiamTSCDRepository::save)
            .map(chiTietGiamTSCDMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<ChiTietGiamTSCDDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ChiTietGiamTSCDS");
        return chiTietGiamTSCDRepository.findAll(pageable).map(chiTietGiamTSCDMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<ChiTietGiamTSCDDTO> findOne(Long id) {
        log.debug("Request to get ChiTietGiamTSCD : {}", id);
        return chiTietGiamTSCDRepository.findById(id).map(chiTietGiamTSCDMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete ChiTietGiamTSCD : {}", id);
        chiTietGiamTSCDRepository.deleteById(id);
    }
}
