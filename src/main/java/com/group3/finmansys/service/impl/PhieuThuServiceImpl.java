package com.group3.finmansys.service.impl;

import com.group3.finmansys.domain.PhieuThu;
import com.group3.finmansys.repository.PhieuThuRepository;
import com.group3.finmansys.service.PhieuThuService;
import com.group3.finmansys.service.dto.PhieuThuDTO;
import com.group3.finmansys.service.mapper.PhieuThuMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link PhieuThu}.
 */
@Service
@Transactional
public class PhieuThuServiceImpl implements PhieuThuService {

    private final Logger log = LoggerFactory.getLogger(PhieuThuServiceImpl.class);

    private final PhieuThuRepository phieuThuRepository;

    private final PhieuThuMapper phieuThuMapper;

    public PhieuThuServiceImpl(PhieuThuRepository phieuThuRepository, PhieuThuMapper phieuThuMapper) {
        this.phieuThuRepository = phieuThuRepository;
        this.phieuThuMapper = phieuThuMapper;
    }

    @Override
    public PhieuThuDTO save(PhieuThuDTO phieuThuDTO) {
        log.debug("Request to save PhieuThu : {}", phieuThuDTO);
        PhieuThu phieuThu = phieuThuMapper.toEntity(phieuThuDTO);
        phieuThu = phieuThuRepository.save(phieuThu);
        return phieuThuMapper.toDto(phieuThu);
    }

    @Override
    public Optional<PhieuThuDTO> partialUpdate(PhieuThuDTO phieuThuDTO) {
        log.debug("Request to partially update PhieuThu : {}", phieuThuDTO);

        return phieuThuRepository
            .findById(phieuThuDTO.getId())
            .map(
                existingPhieuThu -> {
                    phieuThuMapper.partialUpdate(existingPhieuThu, phieuThuDTO);
                    return existingPhieuThu;
                }
            )
            .map(phieuThuRepository::save)
            .map(phieuThuMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<PhieuThuDTO> findAll(Pageable pageable) {
        log.debug("Request to get all PhieuThus");
        return phieuThuRepository.findAll(pageable).map(phieuThuMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<PhieuThuDTO> findOne(Long id) {
        log.debug("Request to get PhieuThu : {}", id);
        return phieuThuRepository.findById(id).map(phieuThuMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete PhieuThu : {}", id);
        phieuThuRepository.deleteById(id);
    }
}
