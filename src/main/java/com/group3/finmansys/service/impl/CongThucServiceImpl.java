package com.group3.finmansys.service.impl;

import com.group3.finmansys.domain.CongThuc;
import com.group3.finmansys.repository.CongThucRepository;
import com.group3.finmansys.service.CongThucService;
import com.group3.finmansys.service.dto.CongThucDTO;
import com.group3.finmansys.service.mapper.CongThucMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link CongThuc}.
 */
@Service
@Transactional
public class CongThucServiceImpl implements CongThucService {

    private final Logger log = LoggerFactory.getLogger(CongThucServiceImpl.class);

    private final CongThucRepository congThucRepository;

    private final CongThucMapper congThucMapper;

    public CongThucServiceImpl(CongThucRepository congThucRepository, CongThucMapper congThucMapper) {
        this.congThucRepository = congThucRepository;
        this.congThucMapper = congThucMapper;
    }

    @Override
    public CongThucDTO save(CongThucDTO congThucDTO) {
        log.debug("Request to save CongThuc : {}", congThucDTO);
        CongThuc congThuc = congThucMapper.toEntity(congThucDTO);
        congThuc = congThucRepository.save(congThuc);
        return congThucMapper.toDto(congThuc);
    }

    @Override
    public Optional<CongThucDTO> partialUpdate(CongThucDTO congThucDTO) {
        log.debug("Request to partially update CongThuc : {}", congThucDTO);

        return congThucRepository
            .findById(congThucDTO.getId())
            .map(
                existingCongThuc -> {
                    congThucMapper.partialUpdate(existingCongThuc, congThucDTO);
                    return existingCongThuc;
                }
            )
            .map(congThucRepository::save)
            .map(congThucMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<CongThucDTO> findAll(Pageable pageable) {
        log.debug("Request to get all CongThucs");
        return congThucRepository.findAll(pageable).map(congThucMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<CongThucDTO> findOne(Long id) {
        log.debug("Request to get CongThuc : {}", id);
        return congThucRepository.findById(id).map(congThucMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete CongThuc : {}", id);
        congThucRepository.deleteById(id);
    }
}
