package com.group3.finmansys.service.impl;

import com.group3.finmansys.domain.NganSachThang;
import com.group3.finmansys.repository.NganSachThangRepository;
import com.group3.finmansys.service.NganSachThangService;
import com.group3.finmansys.service.dto.NganSachThangDTO;
import com.group3.finmansys.service.mapper.NganSachThangMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link NganSachThang}.
 */
@Service
@Transactional
public class NganSachThangServiceImpl implements NganSachThangService {

    private final Logger log = LoggerFactory.getLogger(NganSachThangServiceImpl.class);

    private final NganSachThangRepository nganSachThangRepository;

    private final NganSachThangMapper nganSachThangMapper;

    public NganSachThangServiceImpl(NganSachThangRepository nganSachThangRepository, NganSachThangMapper nganSachThangMapper) {
        this.nganSachThangRepository = nganSachThangRepository;
        this.nganSachThangMapper = nganSachThangMapper;
    }

    @Override
    public NganSachThangDTO save(NganSachThangDTO nganSachThangDTO) {
        log.debug("Request to save NganSachThang : {}", nganSachThangDTO);
        NganSachThang nganSachThang = nganSachThangMapper.toEntity(nganSachThangDTO);
        nganSachThang = nganSachThangRepository.save(nganSachThang);
        return nganSachThangMapper.toDto(nganSachThang);
    }

    @Override
    public Optional<NganSachThangDTO> partialUpdate(NganSachThangDTO nganSachThangDTO) {
        log.debug("Request to partially update NganSachThang : {}", nganSachThangDTO);

        return nganSachThangRepository
            .findById(nganSachThangDTO.getId())
            .map(
                existingNganSachThang -> {
                    nganSachThangMapper.partialUpdate(existingNganSachThang, nganSachThangDTO);
                    return existingNganSachThang;
                }
            )
            .map(nganSachThangRepository::save)
            .map(nganSachThangMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<NganSachThangDTO> findAll(Pageable pageable) {
        log.debug("Request to get all NganSachThangs");
        return nganSachThangRepository.findAll(pageable).map(nganSachThangMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<NganSachThangDTO> findOne(Long id) {
        log.debug("Request to get NganSachThang : {}", id);
        return nganSachThangRepository.findById(id).map(nganSachThangMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete NganSachThang : {}", id);
        nganSachThangRepository.deleteById(id);
    }
}
