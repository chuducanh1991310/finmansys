package com.group3.finmansys.service.impl;

import com.group3.finmansys.domain.KhoanPhaiThu;
import com.group3.finmansys.repository.KhoanPhaiThuRepository;
import com.group3.finmansys.service.KhoanPhaiThuService;
import com.group3.finmansys.service.dto.KhoanPhaiThuDTO;
import com.group3.finmansys.service.mapper.KhoanPhaiThuMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link KhoanPhaiThu}.
 */
@Service
@Transactional
public class KhoanPhaiThuServiceImpl implements KhoanPhaiThuService {

    private final Logger log = LoggerFactory.getLogger(KhoanPhaiThuServiceImpl.class);

    private final KhoanPhaiThuRepository khoanPhaiThuRepository;

    private final KhoanPhaiThuMapper khoanPhaiThuMapper;

    public KhoanPhaiThuServiceImpl(KhoanPhaiThuRepository khoanPhaiThuRepository, KhoanPhaiThuMapper khoanPhaiThuMapper) {
        this.khoanPhaiThuRepository = khoanPhaiThuRepository;
        this.khoanPhaiThuMapper = khoanPhaiThuMapper;
    }

    @Override
    public KhoanPhaiThuDTO save(KhoanPhaiThuDTO khoanPhaiThuDTO) {
        log.debug("Request to save KhoanPhaiThu : {}", khoanPhaiThuDTO);
        KhoanPhaiThu khoanPhaiThu = khoanPhaiThuMapper.toEntity(khoanPhaiThuDTO);
        khoanPhaiThu = khoanPhaiThuRepository.save(khoanPhaiThu);
        return khoanPhaiThuMapper.toDto(khoanPhaiThu);
    }

    @Override
    public Optional<KhoanPhaiThuDTO> partialUpdate(KhoanPhaiThuDTO khoanPhaiThuDTO) {
        log.debug("Request to partially update KhoanPhaiThu : {}", khoanPhaiThuDTO);

        return khoanPhaiThuRepository
            .findById(khoanPhaiThuDTO.getId())
            .map(
                existingKhoanPhaiThu -> {
                    khoanPhaiThuMapper.partialUpdate(existingKhoanPhaiThu, khoanPhaiThuDTO);
                    return existingKhoanPhaiThu;
                }
            )
            .map(khoanPhaiThuRepository::save)
            .map(khoanPhaiThuMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<KhoanPhaiThuDTO> findAll(Pageable pageable) {
        log.debug("Request to get all KhoanPhaiThus");
        return khoanPhaiThuRepository.findAll(pageable).map(khoanPhaiThuMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<KhoanPhaiThuDTO> findOne(Long id) {
        log.debug("Request to get KhoanPhaiThu : {}", id);
        return khoanPhaiThuRepository.findById(id).map(khoanPhaiThuMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete KhoanPhaiThu : {}", id);
        khoanPhaiThuRepository.deleteById(id);
    }
}
