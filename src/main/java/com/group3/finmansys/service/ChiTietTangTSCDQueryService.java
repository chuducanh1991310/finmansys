package com.group3.finmansys.service;

import com.group3.finmansys.domain.*; // for static metamodels
import com.group3.finmansys.domain.ChiTietTangTSCD;
import com.group3.finmansys.repository.ChiTietTangTSCDRepository;
import com.group3.finmansys.service.criteria.ChiTietTangTSCDCriteria;
import com.group3.finmansys.service.dto.ChiTietTangTSCDDTO;
import com.group3.finmansys.service.mapper.ChiTietTangTSCDMapper;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link ChiTietTangTSCD} entities in the database.
 * The main input is a {@link ChiTietTangTSCDCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ChiTietTangTSCDDTO} or a {@link Page} of {@link ChiTietTangTSCDDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ChiTietTangTSCDQueryService extends QueryService<ChiTietTangTSCD> {

    private final Logger log = LoggerFactory.getLogger(ChiTietTangTSCDQueryService.class);

    private final ChiTietTangTSCDRepository chiTietTangTSCDRepository;

    private final ChiTietTangTSCDMapper chiTietTangTSCDMapper;

    public ChiTietTangTSCDQueryService(ChiTietTangTSCDRepository chiTietTangTSCDRepository, ChiTietTangTSCDMapper chiTietTangTSCDMapper) {
        this.chiTietTangTSCDRepository = chiTietTangTSCDRepository;
        this.chiTietTangTSCDMapper = chiTietTangTSCDMapper;
    }

    /**
     * Return a {@link List} of {@link ChiTietTangTSCDDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ChiTietTangTSCDDTO> findByCriteria(ChiTietTangTSCDCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ChiTietTangTSCD> specification = createSpecification(criteria);
        return chiTietTangTSCDMapper.toDto(chiTietTangTSCDRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ChiTietTangTSCDDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ChiTietTangTSCDDTO> findByCriteria(ChiTietTangTSCDCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ChiTietTangTSCD> specification = createSpecification(criteria);
        return chiTietTangTSCDRepository.findAll(specification, page).map(chiTietTangTSCDMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ChiTietTangTSCDCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ChiTietTangTSCD> specification = createSpecification(criteria);
        return chiTietTangTSCDRepository.count(specification);
    }

    /**
     * Function to convert {@link ChiTietTangTSCDCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<ChiTietTangTSCD> createSpecification(ChiTietTangTSCDCriteria criteria) {
        Specification<ChiTietTangTSCD> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), ChiTietTangTSCD_.id));
            }
            if (criteria.getNgayThuMua() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNgayThuMua(), ChiTietTangTSCD_.ngayThuMua));
            }
            if (criteria.getSoLuong() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSoLuong(), ChiTietTangTSCD_.soLuong));
            }
            if (criteria.getChiPhiMua() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getChiPhiMua(), ChiTietTangTSCD_.chiPhiMua));
            }
            if (criteria.getTaiSanCDId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getTaiSanCDId(),
                            root -> root.join(ChiTietTangTSCD_.taiSanCD, JoinType.LEFT).get(TaiSanCD_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
