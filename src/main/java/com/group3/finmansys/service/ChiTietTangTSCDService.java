package com.group3.finmansys.service;

import com.group3.finmansys.service.dto.ChiTietTangTSCDDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.group3.finmansys.domain.ChiTietTangTSCD}.
 */
public interface ChiTietTangTSCDService {
    /**
     * Save a chiTietTangTSCD.
     *
     * @param chiTietTangTSCDDTO the entity to save.
     * @return the persisted entity.
     */
    ChiTietTangTSCDDTO save(ChiTietTangTSCDDTO chiTietTangTSCDDTO);

    /**
     * Partially updates a chiTietTangTSCD.
     *
     * @param chiTietTangTSCDDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<ChiTietTangTSCDDTO> partialUpdate(ChiTietTangTSCDDTO chiTietTangTSCDDTO);

    /**
     * Get all the chiTietTangTSCDS.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ChiTietTangTSCDDTO> findAll(Pageable pageable);

    /**
     * Get the "id" chiTietTangTSCD.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ChiTietTangTSCDDTO> findOne(Long id);

    /**
     * Delete the "id" chiTietTangTSCD.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
