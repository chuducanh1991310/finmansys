package com.group3.finmansys.service;

import com.group3.finmansys.domain.*; // for static metamodels
import com.group3.finmansys.domain.ToanTu;
import com.group3.finmansys.repository.ToanTuRepository;
import com.group3.finmansys.service.criteria.ToanTuCriteria;
import com.group3.finmansys.service.dto.ToanTuDTO;
import com.group3.finmansys.service.mapper.ToanTuMapper;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link ToanTu} entities in the database.
 * The main input is a {@link ToanTuCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ToanTuDTO} or a {@link Page} of {@link ToanTuDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ToanTuQueryService extends QueryService<ToanTu> {

    private final Logger log = LoggerFactory.getLogger(ToanTuQueryService.class);

    private final ToanTuRepository toanTuRepository;

    private final ToanTuMapper toanTuMapper;

    public ToanTuQueryService(ToanTuRepository toanTuRepository, ToanTuMapper toanTuMapper) {
        this.toanTuRepository = toanTuRepository;
        this.toanTuMapper = toanTuMapper;
    }

    /**
     * Return a {@link List} of {@link ToanTuDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ToanTuDTO> findByCriteria(ToanTuCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ToanTu> specification = createSpecification(criteria);
        return toanTuMapper.toDto(toanTuRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ToanTuDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ToanTuDTO> findByCriteria(ToanTuCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ToanTu> specification = createSpecification(criteria);
        return toanTuRepository.findAll(specification, page).map(toanTuMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ToanTuCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ToanTu> specification = createSpecification(criteria);
        return toanTuRepository.count(specification);
    }

    /**
     * Function to convert {@link ToanTuCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<ToanTu> createSpecification(ToanTuCriteria criteria) {
        Specification<ToanTu> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), ToanTu_.id));
            }
            if (criteria.getDau() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDau(), ToanTu_.dau));
            }
            if (criteria.getMaKeToanId() != null) {
                specification =
                    specification.and(
                        buildSpecification(criteria.getMaKeToanId(), root -> root.join(ToanTu_.maKeToan, JoinType.LEFT).get(MaKeToan_.id))
                    );
            }
            if (criteria.getCongThucId() != null) {
                specification =
                    specification.and(
                        buildSpecification(criteria.getCongThucId(), root -> root.join(ToanTu_.congThuc, JoinType.LEFT).get(CongThuc_.id))
                    );
            }
        }
        return specification;
    }
}
