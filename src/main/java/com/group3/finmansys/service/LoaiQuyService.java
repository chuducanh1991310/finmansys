package com.group3.finmansys.service;

import com.group3.finmansys.service.dto.LoaiQuyDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.group3.finmansys.domain.LoaiQuy}.
 */
public interface LoaiQuyService {
    /**
     * Save a loaiQuy.
     *
     * @param loaiQuyDTO the entity to save.
     * @return the persisted entity.
     */
    LoaiQuyDTO save(LoaiQuyDTO loaiQuyDTO);

    /**
     * Partially updates a loaiQuy.
     *
     * @param loaiQuyDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<LoaiQuyDTO> partialUpdate(LoaiQuyDTO loaiQuyDTO);

    /**
     * Get all the loaiQuies.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<LoaiQuyDTO> findAll(Pageable pageable);

    /**
     * Get the "id" loaiQuy.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<LoaiQuyDTO> findOne(Long id);

    /**
     * Delete the "id" loaiQuy.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
