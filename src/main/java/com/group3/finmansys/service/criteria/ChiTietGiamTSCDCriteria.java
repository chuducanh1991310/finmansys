package com.group3.finmansys.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.BooleanFilter;
import tech.jhipster.service.filter.DoubleFilter;
import tech.jhipster.service.filter.Filter;
import tech.jhipster.service.filter.FloatFilter;
import tech.jhipster.service.filter.InstantFilter;
import tech.jhipster.service.filter.IntegerFilter;
import tech.jhipster.service.filter.LongFilter;
import tech.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.group3.finmansys.domain.ChiTietGiamTSCD} entity. This class is used
 * in {@link com.group3.finmansys.web.rest.ChiTietGiamTSCDResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /chi-tiet-giam-tscds?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ChiTietGiamTSCDCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private InstantFilter ngayMua;

    private IntegerFilter soLuong;

    private DoubleFilter khauHao;

    private DoubleFilter soTienThanhLy;

    private DoubleFilter chiPhiMua;

    private LongFilter taiSanCDId;

    public ChiTietGiamTSCDCriteria() {}

    public ChiTietGiamTSCDCriteria(ChiTietGiamTSCDCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.ngayMua = other.ngayMua == null ? null : other.ngayMua.copy();
        this.soLuong = other.soLuong == null ? null : other.soLuong.copy();
        this.khauHao = other.khauHao == null ? null : other.khauHao.copy();
        this.soTienThanhLy = other.soTienThanhLy == null ? null : other.soTienThanhLy.copy();
        this.chiPhiMua = other.chiPhiMua == null ? null : other.chiPhiMua.copy();
        this.taiSanCDId = other.taiSanCDId == null ? null : other.taiSanCDId.copy();
    }

    @Override
    public ChiTietGiamTSCDCriteria copy() {
        return new ChiTietGiamTSCDCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public InstantFilter getNgayMua() {
        return ngayMua;
    }

    public InstantFilter ngayMua() {
        if (ngayMua == null) {
            ngayMua = new InstantFilter();
        }
        return ngayMua;
    }

    public void setNgayMua(InstantFilter ngayMua) {
        this.ngayMua = ngayMua;
    }

    public IntegerFilter getSoLuong() {
        return soLuong;
    }

    public IntegerFilter soLuong() {
        if (soLuong == null) {
            soLuong = new IntegerFilter();
        }
        return soLuong;
    }

    public void setSoLuong(IntegerFilter soLuong) {
        this.soLuong = soLuong;
    }

    public DoubleFilter getKhauHao() {
        return khauHao;
    }

    public DoubleFilter khauHao() {
        if (khauHao == null) {
            khauHao = new DoubleFilter();
        }
        return khauHao;
    }

    public void setKhauHao(DoubleFilter khauHao) {
        this.khauHao = khauHao;
    }

    public DoubleFilter getSoTienThanhLy() {
        return soTienThanhLy;
    }

    public DoubleFilter soTienThanhLy() {
        if (soTienThanhLy == null) {
            soTienThanhLy = new DoubleFilter();
        }
        return soTienThanhLy;
    }

    public void setSoTienThanhLy(DoubleFilter soTienThanhLy) {
        this.soTienThanhLy = soTienThanhLy;
    }

    public DoubleFilter getChiPhiMua() {
        return chiPhiMua;
    }

    public DoubleFilter chiPhiMua() {
        if (chiPhiMua == null) {
            chiPhiMua = new DoubleFilter();
        }
        return chiPhiMua;
    }

    public void setChiPhiMua(DoubleFilter chiPhiMua) {
        this.chiPhiMua = chiPhiMua;
    }

    public LongFilter getTaiSanCDId() {
        return taiSanCDId;
    }

    public LongFilter taiSanCDId() {
        if (taiSanCDId == null) {
            taiSanCDId = new LongFilter();
        }
        return taiSanCDId;
    }

    public void setTaiSanCDId(LongFilter taiSanCDId) {
        this.taiSanCDId = taiSanCDId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ChiTietGiamTSCDCriteria that = (ChiTietGiamTSCDCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(ngayMua, that.ngayMua) &&
            Objects.equals(soLuong, that.soLuong) &&
            Objects.equals(khauHao, that.khauHao) &&
            Objects.equals(soTienThanhLy, that.soTienThanhLy) &&
            Objects.equals(chiPhiMua, that.chiPhiMua) &&
            Objects.equals(taiSanCDId, that.taiSanCDId)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, ngayMua, soLuong, khauHao, soTienThanhLy, chiPhiMua, taiSanCDId);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ChiTietGiamTSCDCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (ngayMua != null ? "ngayMua=" + ngayMua + ", " : "") +
            (soLuong != null ? "soLuong=" + soLuong + ", " : "") +
            (khauHao != null ? "khauHao=" + khauHao + ", " : "") +
            (soTienThanhLy != null ? "soTienThanhLy=" + soTienThanhLy + ", " : "") +
            (chiPhiMua != null ? "chiPhiMua=" + chiPhiMua + ", " : "") +
            (taiSanCDId != null ? "taiSanCDId=" + taiSanCDId + ", " : "") +
            "}";
    }
}
