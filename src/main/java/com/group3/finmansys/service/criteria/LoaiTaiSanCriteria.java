package com.group3.finmansys.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.BooleanFilter;
import tech.jhipster.service.filter.DoubleFilter;
import tech.jhipster.service.filter.Filter;
import tech.jhipster.service.filter.FloatFilter;
import tech.jhipster.service.filter.IntegerFilter;
import tech.jhipster.service.filter.LongFilter;
import tech.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.group3.finmansys.domain.LoaiTaiSan} entity. This class is used
 * in {@link com.group3.finmansys.web.rest.LoaiTaiSanResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /loai-tai-sans?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class LoaiTaiSanCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter ten;

    private StringFilter moTa;

    private LongFilter maKeToanId;

    public LoaiTaiSanCriteria() {}

    public LoaiTaiSanCriteria(LoaiTaiSanCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.ten = other.ten == null ? null : other.ten.copy();
        this.moTa = other.moTa == null ? null : other.moTa.copy();
        this.maKeToanId = other.maKeToanId == null ? null : other.maKeToanId.copy();
    }

    @Override
    public LoaiTaiSanCriteria copy() {
        return new LoaiTaiSanCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getTen() {
        return ten;
    }

    public StringFilter ten() {
        if (ten == null) {
            ten = new StringFilter();
        }
        return ten;
    }

    public void setTen(StringFilter ten) {
        this.ten = ten;
    }

    public StringFilter getMoTa() {
        return moTa;
    }

    public StringFilter moTa() {
        if (moTa == null) {
            moTa = new StringFilter();
        }
        return moTa;
    }

    public void setMoTa(StringFilter moTa) {
        this.moTa = moTa;
    }

    public LongFilter getMaKeToanId() {
        return maKeToanId;
    }

    public LongFilter maKeToanId() {
        if (maKeToanId == null) {
            maKeToanId = new LongFilter();
        }
        return maKeToanId;
    }

    public void setMaKeToanId(LongFilter maKeToanId) {
        this.maKeToanId = maKeToanId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final LoaiTaiSanCriteria that = (LoaiTaiSanCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(ten, that.ten) &&
            Objects.equals(moTa, that.moTa) &&
            Objects.equals(maKeToanId, that.maKeToanId)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, ten, moTa, maKeToanId);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "LoaiTaiSanCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (ten != null ? "ten=" + ten + ", " : "") +
            (moTa != null ? "moTa=" + moTa + ", " : "") +
            (maKeToanId != null ? "maKeToanId=" + maKeToanId + ", " : "") +
            "}";
    }
}
