package com.group3.finmansys.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.BooleanFilter;
import tech.jhipster.service.filter.DoubleFilter;
import tech.jhipster.service.filter.Filter;
import tech.jhipster.service.filter.FloatFilter;
import tech.jhipster.service.filter.IntegerFilter;
import tech.jhipster.service.filter.LongFilter;
import tech.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.group3.finmansys.domain.NganSachNam} entity. This class is used
 * in {@link com.group3.finmansys.web.rest.NganSachNamResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /ngan-sach-nams?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class NganSachNamCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter ten;

    private IntegerFilter nam;

    private DoubleFilter soTien;

    private LongFilter khachSanId;

    private LongFilter nganSachThangId;

    public NganSachNamCriteria() {}

    public NganSachNamCriteria(NganSachNamCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.ten = other.ten == null ? null : other.ten.copy();
        this.nam = other.nam == null ? null : other.nam.copy();
        this.soTien = other.soTien == null ? null : other.soTien.copy();
        this.khachSanId = other.khachSanId == null ? null : other.khachSanId.copy();
        this.nganSachThangId = other.nganSachThangId == null ? null : other.nganSachThangId.copy();
    }

    @Override
    public NganSachNamCriteria copy() {
        return new NganSachNamCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getTen() {
        return ten;
    }

    public StringFilter ten() {
        if (ten == null) {
            ten = new StringFilter();
        }
        return ten;
    }

    public void setTen(StringFilter ten) {
        this.ten = ten;
    }

    public IntegerFilter getNam() {
        return nam;
    }

    public IntegerFilter nam() {
        if (nam == null) {
            nam = new IntegerFilter();
        }
        return nam;
    }

    public void setNam(IntegerFilter nam) {
        this.nam = nam;
    }

    public DoubleFilter getSoTien() {
        return soTien;
    }

    public DoubleFilter soTien() {
        if (soTien == null) {
            soTien = new DoubleFilter();
        }
        return soTien;
    }

    public void setSoTien(DoubleFilter soTien) {
        this.soTien = soTien;
    }

    public LongFilter getKhachSanId() {
        return khachSanId;
    }

    public LongFilter khachSanId() {
        if (khachSanId == null) {
            khachSanId = new LongFilter();
        }
        return khachSanId;
    }

    public void setKhachSanId(LongFilter khachSanId) {
        this.khachSanId = khachSanId;
    }

    public LongFilter getNganSachThangId() {
        return nganSachThangId;
    }

    public LongFilter nganSachThangId() {
        if (nganSachThangId == null) {
            nganSachThangId = new LongFilter();
        }
        return nganSachThangId;
    }

    public void setNganSachThangId(LongFilter nganSachThangId) {
        this.nganSachThangId = nganSachThangId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final NganSachNamCriteria that = (NganSachNamCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(ten, that.ten) &&
            Objects.equals(nam, that.nam) &&
            Objects.equals(soTien, that.soTien) &&
            Objects.equals(khachSanId, that.khachSanId) &&
            Objects.equals(nganSachThangId, that.nganSachThangId)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, ten, nam, soTien, khachSanId, nganSachThangId);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "NganSachNamCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (ten != null ? "ten=" + ten + ", " : "") +
            (nam != null ? "nam=" + nam + ", " : "") +
            (soTien != null ? "soTien=" + soTien + ", " : "") +
            (khachSanId != null ? "khachSanId=" + khachSanId + ", " : "") +
            (nganSachThangId != null ? "nganSachThangId=" + nganSachThangId + ", " : "") +
            "}";
    }
}
