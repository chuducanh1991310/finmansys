package com.group3.finmansys.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.BooleanFilter;
import tech.jhipster.service.filter.DoubleFilter;
import tech.jhipster.service.filter.Filter;
import tech.jhipster.service.filter.FloatFilter;
import tech.jhipster.service.filter.IntegerFilter;
import tech.jhipster.service.filter.LongFilter;
import tech.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.group3.finmansys.domain.QuyTien} entity. This class is used
 * in {@link com.group3.finmansys.web.rest.QuyTienResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /quy-tiens?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class QuyTienCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private DoubleFilter soTien;

    private StringFilter ghiChu;

    private StringFilter ten;

    private LongFilter loaiQuyId;

    private LongFilter maKeToanId;

    private LongFilter khachSanId;

    private LongFilter phieuThuId;

    private LongFilter phieuChiId;

    public QuyTienCriteria() {}

    public QuyTienCriteria(QuyTienCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.soTien = other.soTien == null ? null : other.soTien.copy();
        this.ghiChu = other.ghiChu == null ? null : other.ghiChu.copy();
        this.ten = other.ten == null ? null : other.ten.copy();
        this.loaiQuyId = other.loaiQuyId == null ? null : other.loaiQuyId.copy();
        this.maKeToanId = other.maKeToanId == null ? null : other.maKeToanId.copy();
        this.khachSanId = other.khachSanId == null ? null : other.khachSanId.copy();
        this.phieuThuId = other.phieuThuId == null ? null : other.phieuThuId.copy();
        this.phieuChiId = other.phieuChiId == null ? null : other.phieuChiId.copy();
    }

    @Override
    public QuyTienCriteria copy() {
        return new QuyTienCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public DoubleFilter getSoTien() {
        return soTien;
    }

    public DoubleFilter soTien() {
        if (soTien == null) {
            soTien = new DoubleFilter();
        }
        return soTien;
    }

    public void setSoTien(DoubleFilter soTien) {
        this.soTien = soTien;
    }

    public StringFilter getGhiChu() {
        return ghiChu;
    }

    public StringFilter ghiChu() {
        if (ghiChu == null) {
            ghiChu = new StringFilter();
        }
        return ghiChu;
    }

    public void setGhiChu(StringFilter ghiChu) {
        this.ghiChu = ghiChu;
    }

    public StringFilter getTen() {
        return ten;
    }

    public StringFilter ten() {
        if (ten == null) {
            ten = new StringFilter();
        }
        return ten;
    }

    public void setTen(StringFilter ten) {
        this.ten = ten;
    }

    public LongFilter getLoaiQuyId() {
        return loaiQuyId;
    }

    public LongFilter loaiQuyId() {
        if (loaiQuyId == null) {
            loaiQuyId = new LongFilter();
        }
        return loaiQuyId;
    }

    public void setLoaiQuyId(LongFilter loaiQuyId) {
        this.loaiQuyId = loaiQuyId;
    }

    public LongFilter getMaKeToanId() {
        return maKeToanId;
    }

    public LongFilter maKeToanId() {
        if (maKeToanId == null) {
            maKeToanId = new LongFilter();
        }
        return maKeToanId;
    }

    public void setMaKeToanId(LongFilter maKeToanId) {
        this.maKeToanId = maKeToanId;
    }

    public LongFilter getKhachSanId() {
        return khachSanId;
    }

    public LongFilter khachSanId() {
        if (khachSanId == null) {
            khachSanId = new LongFilter();
        }
        return khachSanId;
    }

    public void setKhachSanId(LongFilter khachSanId) {
        this.khachSanId = khachSanId;
    }

    public LongFilter getPhieuThuId() {
        return phieuThuId;
    }

    public LongFilter phieuThuId() {
        if (phieuThuId == null) {
            phieuThuId = new LongFilter();
        }
        return phieuThuId;
    }

    public void setPhieuThuId(LongFilter phieuThuId) {
        this.phieuThuId = phieuThuId;
    }

    public LongFilter getPhieuChiId() {
        return phieuChiId;
    }

    public LongFilter phieuChiId() {
        if (phieuChiId == null) {
            phieuChiId = new LongFilter();
        }
        return phieuChiId;
    }

    public void setPhieuChiId(LongFilter phieuChiId) {
        this.phieuChiId = phieuChiId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final QuyTienCriteria that = (QuyTienCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(soTien, that.soTien) &&
            Objects.equals(ghiChu, that.ghiChu) &&
            Objects.equals(ten, that.ten) &&
            Objects.equals(loaiQuyId, that.loaiQuyId) &&
            Objects.equals(maKeToanId, that.maKeToanId) &&
            Objects.equals(khachSanId, that.khachSanId) &&
            Objects.equals(phieuThuId, that.phieuThuId) &&
            Objects.equals(phieuChiId, that.phieuChiId)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, soTien, ghiChu, ten, loaiQuyId, maKeToanId, khachSanId, phieuThuId, phieuChiId);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "QuyTienCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (soTien != null ? "soTien=" + soTien + ", " : "") +
            (ghiChu != null ? "ghiChu=" + ghiChu + ", " : "") +
            (ten != null ? "ten=" + ten + ", " : "") +
            (loaiQuyId != null ? "loaiQuyId=" + loaiQuyId + ", " : "") +
            (maKeToanId != null ? "maKeToanId=" + maKeToanId + ", " : "") +
            (khachSanId != null ? "khachSanId=" + khachSanId + ", " : "") +
            (phieuThuId != null ? "phieuThuId=" + phieuThuId + ", " : "") +
            (phieuChiId != null ? "phieuChiId=" + phieuChiId + ", " : "") +
            "}";
    }
}
