package com.group3.finmansys.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.BooleanFilter;
import tech.jhipster.service.filter.DoubleFilter;
import tech.jhipster.service.filter.Filter;
import tech.jhipster.service.filter.FloatFilter;
import tech.jhipster.service.filter.InstantFilter;
import tech.jhipster.service.filter.IntegerFilter;
import tech.jhipster.service.filter.LongFilter;
import tech.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.group3.finmansys.domain.PhieuChi} entity. This class is used
 * in {@link com.group3.finmansys.web.rest.PhieuChiResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /phieu-chis?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class PhieuChiCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private InstantFilter ngayChungTu;

    private DoubleFilter soTien;

    private LongFilter khachHangId;

    private LongFilter khoanMucChiPhiId;

    private LongFilter khoanPhaiTraId;

    private LongFilter quyTienId;

    public PhieuChiCriteria() {}

    public PhieuChiCriteria(PhieuChiCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.ngayChungTu = other.ngayChungTu == null ? null : other.ngayChungTu.copy();
        this.soTien = other.soTien == null ? null : other.soTien.copy();
        this.khachHangId = other.khachHangId == null ? null : other.khachHangId.copy();
        this.khoanMucChiPhiId = other.khoanMucChiPhiId == null ? null : other.khoanMucChiPhiId.copy();
        this.khoanPhaiTraId = other.khoanPhaiTraId == null ? null : other.khoanPhaiTraId.copy();
        this.quyTienId = other.quyTienId == null ? null : other.quyTienId.copy();
    }

    @Override
    public PhieuChiCriteria copy() {
        return new PhieuChiCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public InstantFilter getNgayChungTu() {
        return ngayChungTu;
    }

    public InstantFilter ngayChungTu() {
        if (ngayChungTu == null) {
            ngayChungTu = new InstantFilter();
        }
        return ngayChungTu;
    }

    public void setNgayChungTu(InstantFilter ngayChungTu) {
        this.ngayChungTu = ngayChungTu;
    }

    public DoubleFilter getSoTien() {
        return soTien;
    }

    public DoubleFilter soTien() {
        if (soTien == null) {
            soTien = new DoubleFilter();
        }
        return soTien;
    }

    public void setSoTien(DoubleFilter soTien) {
        this.soTien = soTien;
    }

    public LongFilter getKhachHangId() {
        return khachHangId;
    }

    public LongFilter khachHangId() {
        if (khachHangId == null) {
            khachHangId = new LongFilter();
        }
        return khachHangId;
    }

    public void setKhachHangId(LongFilter khachHangId) {
        this.khachHangId = khachHangId;
    }

    public LongFilter getKhoanMucChiPhiId() {
        return khoanMucChiPhiId;
    }

    public LongFilter khoanMucChiPhiId() {
        if (khoanMucChiPhiId == null) {
            khoanMucChiPhiId = new LongFilter();
        }
        return khoanMucChiPhiId;
    }

    public void setKhoanMucChiPhiId(LongFilter khoanMucChiPhiId) {
        this.khoanMucChiPhiId = khoanMucChiPhiId;
    }

    public LongFilter getKhoanPhaiTraId() {
        return khoanPhaiTraId;
    }

    public LongFilter khoanPhaiTraId() {
        if (khoanPhaiTraId == null) {
            khoanPhaiTraId = new LongFilter();
        }
        return khoanPhaiTraId;
    }

    public void setKhoanPhaiTraId(LongFilter khoanPhaiTraId) {
        this.khoanPhaiTraId = khoanPhaiTraId;
    }

    public LongFilter getQuyTienId() {
        return quyTienId;
    }

    public LongFilter quyTienId() {
        if (quyTienId == null) {
            quyTienId = new LongFilter();
        }
        return quyTienId;
    }

    public void setQuyTienId(LongFilter quyTienId) {
        this.quyTienId = quyTienId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final PhieuChiCriteria that = (PhieuChiCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(ngayChungTu, that.ngayChungTu) &&
            Objects.equals(soTien, that.soTien) &&
            Objects.equals(khachHangId, that.khachHangId) &&
            Objects.equals(khoanMucChiPhiId, that.khoanMucChiPhiId) &&
            Objects.equals(khoanPhaiTraId, that.khoanPhaiTraId) &&
            Objects.equals(quyTienId, that.quyTienId)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, ngayChungTu, soTien, khachHangId, khoanMucChiPhiId, khoanPhaiTraId, quyTienId);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PhieuChiCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (ngayChungTu != null ? "ngayChungTu=" + ngayChungTu + ", " : "") +
            (soTien != null ? "soTien=" + soTien + ", " : "") +
            (khachHangId != null ? "khachHangId=" + khachHangId + ", " : "") +
            (khoanMucChiPhiId != null ? "khoanMucChiPhiId=" + khoanMucChiPhiId + ", " : "") +
            (khoanPhaiTraId != null ? "khoanPhaiTraId=" + khoanPhaiTraId + ", " : "") +
            (quyTienId != null ? "quyTienId=" + quyTienId + ", " : "") +
            "}";
    }
}
