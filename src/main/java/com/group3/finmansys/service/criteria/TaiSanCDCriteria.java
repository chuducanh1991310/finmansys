package com.group3.finmansys.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.BooleanFilter;
import tech.jhipster.service.filter.DoubleFilter;
import tech.jhipster.service.filter.Filter;
import tech.jhipster.service.filter.FloatFilter;
import tech.jhipster.service.filter.IntegerFilter;
import tech.jhipster.service.filter.LongFilter;
import tech.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.group3.finmansys.domain.TaiSanCD} entity. This class is used
 * in {@link com.group3.finmansys.web.rest.TaiSanCDResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /tai-san-cds?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class TaiSanCDCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter ten;

    private StringFilter donViTuoiTho;

    private DoubleFilter thoiGianSuDung;

    private LongFilter khachSanId;

    private LongFilter loaiTaiSanId;

    public TaiSanCDCriteria() {}

    public TaiSanCDCriteria(TaiSanCDCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.ten = other.ten == null ? null : other.ten.copy();
        this.donViTuoiTho = other.donViTuoiTho == null ? null : other.donViTuoiTho.copy();
        this.thoiGianSuDung = other.thoiGianSuDung == null ? null : other.thoiGianSuDung.copy();
        this.khachSanId = other.khachSanId == null ? null : other.khachSanId.copy();
        this.loaiTaiSanId = other.loaiTaiSanId == null ? null : other.loaiTaiSanId.copy();
    }

    @Override
    public TaiSanCDCriteria copy() {
        return new TaiSanCDCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getTen() {
        return ten;
    }

    public StringFilter ten() {
        if (ten == null) {
            ten = new StringFilter();
        }
        return ten;
    }

    public void setTen(StringFilter ten) {
        this.ten = ten;
    }

    public StringFilter getDonViTuoiTho() {
        return donViTuoiTho;
    }

    public StringFilter donViTuoiTho() {
        if (donViTuoiTho == null) {
            donViTuoiTho = new StringFilter();
        }
        return donViTuoiTho;
    }

    public void setDonViTuoiTho(StringFilter donViTuoiTho) {
        this.donViTuoiTho = donViTuoiTho;
    }

    public DoubleFilter getThoiGianSuDung() {
        return thoiGianSuDung;
    }

    public DoubleFilter thoiGianSuDung() {
        if (thoiGianSuDung == null) {
            thoiGianSuDung = new DoubleFilter();
        }
        return thoiGianSuDung;
    }

    public void setThoiGianSuDung(DoubleFilter thoiGianSuDung) {
        this.thoiGianSuDung = thoiGianSuDung;
    }

    public LongFilter getKhachSanId() {
        return khachSanId;
    }

    public LongFilter khachSanId() {
        if (khachSanId == null) {
            khachSanId = new LongFilter();
        }
        return khachSanId;
    }

    public void setKhachSanId(LongFilter khachSanId) {
        this.khachSanId = khachSanId;
    }

    public LongFilter getLoaiTaiSanId() {
        return loaiTaiSanId;
    }

    public LongFilter loaiTaiSanId() {
        if (loaiTaiSanId == null) {
            loaiTaiSanId = new LongFilter();
        }
        return loaiTaiSanId;
    }

    public void setLoaiTaiSanId(LongFilter loaiTaiSanId) {
        this.loaiTaiSanId = loaiTaiSanId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final TaiSanCDCriteria that = (TaiSanCDCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(ten, that.ten) &&
            Objects.equals(donViTuoiTho, that.donViTuoiTho) &&
            Objects.equals(thoiGianSuDung, that.thoiGianSuDung) &&
            Objects.equals(khachSanId, that.khachSanId) &&
            Objects.equals(loaiTaiSanId, that.loaiTaiSanId)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, ten, donViTuoiTho, thoiGianSuDung, khachSanId, loaiTaiSanId);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "TaiSanCDCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (ten != null ? "ten=" + ten + ", " : "") +
            (donViTuoiTho != null ? "donViTuoiTho=" + donViTuoiTho + ", " : "") +
            (thoiGianSuDung != null ? "thoiGianSuDung=" + thoiGianSuDung + ", " : "") +
            (khachSanId != null ? "khachSanId=" + khachSanId + ", " : "") +
            (loaiTaiSanId != null ? "loaiTaiSanId=" + loaiTaiSanId + ", " : "") +
            "}";
    }
}
