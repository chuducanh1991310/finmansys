package com.group3.finmansys.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.BooleanFilter;
import tech.jhipster.service.filter.DoubleFilter;
import tech.jhipster.service.filter.Filter;
import tech.jhipster.service.filter.FloatFilter;
import tech.jhipster.service.filter.IntegerFilter;
import tech.jhipster.service.filter.LongFilter;
import tech.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.group3.finmansys.domain.MaKeToan} entity. This class is used
 * in {@link com.group3.finmansys.web.rest.MaKeToanResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /ma-ke-toans?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class MaKeToanCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private DoubleFilter soHieu;

    private StringFilter ten;

    public MaKeToanCriteria() {}

    public MaKeToanCriteria(MaKeToanCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.soHieu = other.soHieu == null ? null : other.soHieu.copy();
        this.ten = other.ten == null ? null : other.ten.copy();
    }

    @Override
    public MaKeToanCriteria copy() {
        return new MaKeToanCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public DoubleFilter getSoHieu() {
        return soHieu;
    }

    public DoubleFilter soHieu() {
        if (soHieu == null) {
            soHieu = new DoubleFilter();
        }
        return soHieu;
    }

    public void setSoHieu(DoubleFilter soHieu) {
        this.soHieu = soHieu;
    }

    public StringFilter getTen() {
        return ten;
    }

    public StringFilter ten() {
        if (ten == null) {
            ten = new StringFilter();
        }
        return ten;
    }

    public void setTen(StringFilter ten) {
        this.ten = ten;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final MaKeToanCriteria that = (MaKeToanCriteria) o;
        return Objects.equals(id, that.id) && Objects.equals(soHieu, that.soHieu) && Objects.equals(ten, that.ten);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, soHieu, ten);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "MaKeToanCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (soHieu != null ? "soHieu=" + soHieu + ", " : "") +
            (ten != null ? "ten=" + ten + ", " : "") +
            "}";
    }
}
