package com.group3.finmansys.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.BooleanFilter;
import tech.jhipster.service.filter.DoubleFilter;
import tech.jhipster.service.filter.Filter;
import tech.jhipster.service.filter.FloatFilter;
import tech.jhipster.service.filter.IntegerFilter;
import tech.jhipster.service.filter.LongFilter;
import tech.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.group3.finmansys.domain.ToanTu} entity. This class is used
 * in {@link com.group3.finmansys.web.rest.ToanTuResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /toan-tus?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ToanTuCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter dau;

    private LongFilter maKeToanId;

    private LongFilter congThucId;

    public ToanTuCriteria() {}

    public ToanTuCriteria(ToanTuCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.dau = other.dau == null ? null : other.dau.copy();
        this.maKeToanId = other.maKeToanId == null ? null : other.maKeToanId.copy();
        this.congThucId = other.congThucId == null ? null : other.congThucId.copy();
    }

    @Override
    public ToanTuCriteria copy() {
        return new ToanTuCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getDau() {
        return dau;
    }

    public StringFilter dau() {
        if (dau == null) {
            dau = new StringFilter();
        }
        return dau;
    }

    public void setDau(StringFilter dau) {
        this.dau = dau;
    }

    public LongFilter getMaKeToanId() {
        return maKeToanId;
    }

    public LongFilter maKeToanId() {
        if (maKeToanId == null) {
            maKeToanId = new LongFilter();
        }
        return maKeToanId;
    }

    public void setMaKeToanId(LongFilter maKeToanId) {
        this.maKeToanId = maKeToanId;
    }

    public LongFilter getCongThucId() {
        return congThucId;
    }

    public LongFilter congThucId() {
        if (congThucId == null) {
            congThucId = new LongFilter();
        }
        return congThucId;
    }

    public void setCongThucId(LongFilter congThucId) {
        this.congThucId = congThucId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ToanTuCriteria that = (ToanTuCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(dau, that.dau) &&
            Objects.equals(maKeToanId, that.maKeToanId) &&
            Objects.equals(congThucId, that.congThucId)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, dau, maKeToanId, congThucId);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ToanTuCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (dau != null ? "dau=" + dau + ", " : "") +
            (maKeToanId != null ? "maKeToanId=" + maKeToanId + ", " : "") +
            (congThucId != null ? "congThucId=" + congThucId + ", " : "") +
            "}";
    }
}
