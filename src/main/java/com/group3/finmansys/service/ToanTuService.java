package com.group3.finmansys.service;

import com.group3.finmansys.service.dto.ToanTuDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.group3.finmansys.domain.ToanTu}.
 */
public interface ToanTuService {
    /**
     * Save a toanTu.
     *
     * @param toanTuDTO the entity to save.
     * @return the persisted entity.
     */
    ToanTuDTO save(ToanTuDTO toanTuDTO);

    /**
     * Partially updates a toanTu.
     *
     * @param toanTuDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<ToanTuDTO> partialUpdate(ToanTuDTO toanTuDTO);

    /**
     * Get all the toanTus.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ToanTuDTO> findAll(Pageable pageable);

    /**
     * Get the "id" toanTu.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ToanTuDTO> findOne(Long id);

    /**
     * Delete the "id" toanTu.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
