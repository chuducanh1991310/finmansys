package com.group3.finmansys.service;

import com.group3.finmansys.service.dto.KhachSanDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.group3.finmansys.domain.KhachSan}.
 */
public interface KhachSanService {
    /**
     * Save a khachSan.
     *
     * @param khachSanDTO the entity to save.
     * @return the persisted entity.
     */
    KhachSanDTO save(KhachSanDTO khachSanDTO);

    /**
     * Partially updates a khachSan.
     *
     * @param khachSanDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<KhachSanDTO> partialUpdate(KhachSanDTO khachSanDTO);

    /**
     * Get all the khachSans.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<KhachSanDTO> findAll(Pageable pageable);

    /**
     * Get the "id" khachSan.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<KhachSanDTO> findOne(Long id);

    /**
     * Delete the "id" khachSan.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
