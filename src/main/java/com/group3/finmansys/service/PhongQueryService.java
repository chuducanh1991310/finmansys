package com.group3.finmansys.service;

import com.group3.finmansys.domain.*; // for static metamodels
import com.group3.finmansys.domain.Phong;
import com.group3.finmansys.repository.PhongRepository;
import com.group3.finmansys.service.criteria.PhongCriteria;
import com.group3.finmansys.service.dto.PhongDTO;
import com.group3.finmansys.service.mapper.PhongMapper;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link Phong} entities in the database.
 * The main input is a {@link PhongCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link PhongDTO} or a {@link Page} of {@link PhongDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class PhongQueryService extends QueryService<Phong> {

    private final Logger log = LoggerFactory.getLogger(PhongQueryService.class);

    private final PhongRepository phongRepository;

    private final PhongMapper phongMapper;

    public PhongQueryService(PhongRepository phongRepository, PhongMapper phongMapper) {
        this.phongRepository = phongRepository;
        this.phongMapper = phongMapper;
    }

    /**
     * Return a {@link List} of {@link PhongDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<PhongDTO> findByCriteria(PhongCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Phong> specification = createSpecification(criteria);
        return phongMapper.toDto(phongRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link PhongDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<PhongDTO> findByCriteria(PhongCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Phong> specification = createSpecification(criteria);
        return phongRepository.findAll(specification, page).map(phongMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(PhongCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Phong> specification = createSpecification(criteria);
        return phongRepository.count(specification);
    }

    /**
     * Function to convert {@link PhongCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Phong> createSpecification(PhongCriteria criteria) {
        Specification<Phong> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Phong_.id));
            }
            if (criteria.getTen() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTen(), Phong_.ten));
            }
            if (criteria.getLoai() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLoai(), Phong_.loai));
            }
            if (criteria.getGia() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getGia(), Phong_.gia));
            }
            if (criteria.getKhachSanId() != null) {
                specification =
                    specification.and(
                        buildSpecification(criteria.getKhachSanId(), root -> root.join(Phong_.khachSan, JoinType.LEFT).get(KhachSan_.id))
                    );
            }
        }
        return specification;
    }
}
