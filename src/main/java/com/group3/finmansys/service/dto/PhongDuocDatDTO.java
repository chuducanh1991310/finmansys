package com.group3.finmansys.service.dto;

import com.group3.finmansys.domain.DichVuSuDung;
import com.group3.finmansys.domain.PhongDat;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.group3.finmansys.domain.PhongDuocDat} entity.
 */
public class PhongDuocDatDTO implements Serializable {

    private Long id;

    @NotNull
    private Instant ngayDat;

    @DecimalMin(value = "0")
    @DecimalMax(value = "100")
    private Double khuyenMai;

    @NotNull
    private Instant checkIn;

    @NotNull
    private Instant checkOut;

    @NotNull
    @DecimalMin(value = "1")
    private Double gia;

    private KhachHangDTO khachHang;

    private Set<PhongDat> phongDats = new HashSet<>();

    private Set<DichVuSuDung> dichVuSuDungs = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getNgayDat() {
        return ngayDat;
    }

    public void setNgayDat(Instant ngayDat) {
        this.ngayDat = ngayDat;
    }

    public Double getKhuyenMai() {
        return khuyenMai;
    }

    public void setKhuyenMai(Double khuyenMai) {
        this.khuyenMai = khuyenMai;
    }

    public Instant getCheckIn() {
        return checkIn;
    }

    public void setCheckIn(Instant checkIn) {
        this.checkIn = checkIn;
    }

    public Instant getCheckOut() {
        return checkOut;
    }

    public void setCheckOut(Instant checkOut) {
        this.checkOut = checkOut;
    }

    public Double getGia() {
        return gia;
    }

    public void setGia(Double gia) {
        this.gia = gia;
    }

    public KhachHangDTO getKhachHang() {
        return khachHang;
    }

    public void setKhachHang(KhachHangDTO khachHang) {
        this.khachHang = khachHang;
    }

    public Set<PhongDat> getPhongDats() {
        return phongDats;
    }

    public void setPhongDats(Set<PhongDat> phongDats) {
        this.phongDats = phongDats;
    }

    public Set<DichVuSuDung> getDichVuSuDungs() {
        return dichVuSuDungs;
    }

    public void setDichVuSuDungs(Set<DichVuSuDung> dichVuSuDungs) {
        this.dichVuSuDungs = dichVuSuDungs;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PhongDuocDatDTO)) {
            return false;
        }

        PhongDuocDatDTO phongDuocDatDTO = (PhongDuocDatDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, phongDuocDatDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PhongDuocDatDTO{" +
            "id=" + getId() +
            ", ngayDat='" + getNgayDat() + "'" +
            ", khuyenMai=" + getKhuyenMai() +
            ", checkIn='" + getCheckIn() + "'" +
            ", checkOut='" + getCheckOut() + "'" +
            ", gia=" + getGia() +
            ", khachHang=" + getKhachHang() +
            "}";
    }
}
