package com.group3.finmansys.service.dto;

import java.io.Serializable;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.group3.finmansys.domain.DichVu} entity.
 */
public class DichVuDTO implements Serializable {

    private Long id;

    @NotNull
    private String ten;

    @NotNull
    @DecimalMin(value = "1")
    private Double gia;

    private String moTa;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public Double getGia() {
        return gia;
    }

    public void setGia(Double gia) {
        this.gia = gia;
    }

    public String getMoTa() {
        return moTa;
    }

    public void setMoTa(String moTa) {
        this.moTa = moTa;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DichVuDTO)) {
            return false;
        }

        DichVuDTO dichVuDTO = (DichVuDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, dichVuDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "DichVuDTO{" +
            "id=" + getId() +
            ", ten='" + getTen() + "'" +
            ", gia=" + getGia() +
            ", moTa='" + getMoTa() + "'" +
            "}";
    }
}
