package com.group3.finmansys.service.dto;

import java.io.Serializable;
import java.util.Objects;
import java.util.Set;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.group3.finmansys.domain.NganSachNam} entity.
 */
public class NganSachNamDTO implements Serializable {

    private Long id;

    @NotNull
    private String ten;

    @NotNull
    @Min(value = 1)
    private Integer nam;

    @NotNull
    @DecimalMin(value = "1")
    private Double soTien;

    private Set<NganSachThangDTO> nganSachThangs;

    private KhachSanDTO khachSan;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public Integer getNam() {
        return nam;
    }

    public void setNam(Integer nam) {
        this.nam = nam;
    }

    public Double getSoTien() {
        return soTien;
    }

    public void setSoTien(Double soTien) {
        this.soTien = soTien;
    }

    public KhachSanDTO getKhachSan() {
        return khachSan;
    }

    public void setKhachSan(KhachSanDTO khachSan) {
        this.khachSan = khachSan;
    }

    public Set<NganSachThangDTO> getNganSachThangs() {
        return nganSachThangs;
    }

    public void setNganSachThangs(Set<NganSachThangDTO> nganSachThangs) {
        this.nganSachThangs = nganSachThangs;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof NganSachNamDTO)) {
            return false;
        }

        NganSachNamDTO nganSachNamDTO = (NganSachNamDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, nganSachNamDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "NganSachNamDTO{" +
            "id=" + getId() +
            ", ten='" + getTen() + "'" +
            ", nam=" + getNam() +
            ", soTien=" + getSoTien() +
            ", khachSan=" + getKhachSan() +
            "}";
    }
}
