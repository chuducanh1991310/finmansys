package com.group3.finmansys.service.dto;

import com.group3.finmansys.domain.CongThuc;
import com.group3.finmansys.domain.ToanTu;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.group3.finmansys.domain.CongThuc} entity.
 */
public class CongThucDTO implements Serializable {

    private Long id;

    @NotNull
    private Integer maSo;

    @NotNull
    private String tenChiTieu;

    @NotNull
    private Integer cap;

    private Double soDauKy;

    private Double soCuoiKy;

    private CongThucDTO capCha;

    private Set<ToanTu> toanTus = new HashSet<>();

    private Set<CongThuc> capCons = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getMaSo() {
        return maSo;
    }

    public void setMaSo(Integer maSo) {
        this.maSo = maSo;
    }

    public String getTenChiTieu() {
        return tenChiTieu;
    }

    public void setTenChiTieu(String tenChiTieu) {
        this.tenChiTieu = tenChiTieu;
    }

    public Integer getCap() {
        return cap;
    }

    public void setCap(Integer cap) {
        this.cap = cap;
    }

    public Double getSoDauKy() {
        return soDauKy;
    }

    public void setSoDauKy(Double soDauKy) {
        this.soDauKy = soDauKy;
    }

    public Double getSoCuoiKy() {
        return soCuoiKy;
    }

    public void setSoCuoiKy(Double soCuoiKy) {
        this.soCuoiKy = soCuoiKy;
    }

    public CongThucDTO getCapCha() {
        return capCha;
    }

    public void setCapCha(CongThucDTO capCha) {
        this.capCha = capCha;
    }

    public Set<ToanTu> getToanTus() {
        return toanTus;
    }

    public void setToanTus(Set<ToanTu> toanTus) {
        this.toanTus = toanTus;
    }

    public Set<CongThuc> getCapCons() {
        return capCons;
    }

    public void setCapCons(Set<CongThuc> capCons) {
        this.capCons = capCons;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CongThucDTO)) {
            return false;
        }

        CongThucDTO congThucDTO = (CongThucDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, congThucDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CongThucDTO{" +
            "id=" + getId() +
            ", maSo=" + getMaSo() +
            ", tenChiTieu='" + getTenChiTieu() + "'" +
            ", cap=" + getCap() +
            ", soDauKy=" + getSoDauKy() +
            ", soCuoiKy=" + getSoCuoiKy() +
            ", capCha=" + getCapCha() +
            "}";
    }
}
