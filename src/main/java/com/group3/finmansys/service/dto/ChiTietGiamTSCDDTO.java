package com.group3.finmansys.service.dto;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.group3.finmansys.domain.ChiTietGiamTSCD} entity.
 */
public class ChiTietGiamTSCDDTO implements Serializable {

    private Long id;

    @NotNull
    private Instant ngayMua;

    @NotNull
    @Min(value = 1)
    private Integer soLuong;

    @DecimalMin(value = "0")
    private Double khauHao;

    @NotNull
    @DecimalMin(value = "1")
    private Double soTienThanhLy;

    @NotNull
    @DecimalMin(value = "1")
    private Double chiPhiMua;

    private TaiSanCDDTO taiSanCD;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getNgayMua() {
        return ngayMua;
    }

    public void setNgayMua(Instant ngayMua) {
        this.ngayMua = ngayMua;
    }

    public Integer getSoLuong() {
        return soLuong;
    }

    public void setSoLuong(Integer soLuong) {
        this.soLuong = soLuong;
    }

    public Double getKhauHao() {
        return khauHao;
    }

    public void setKhauHao(Double khauHao) {
        this.khauHao = khauHao;
    }

    public Double getSoTienThanhLy() {
        return soTienThanhLy;
    }

    public void setSoTienThanhLy(Double soTienThanhLy) {
        this.soTienThanhLy = soTienThanhLy;
    }

    public Double getChiPhiMua() {
        return chiPhiMua;
    }

    public void setChiPhiMua(Double chiPhiMua) {
        this.chiPhiMua = chiPhiMua;
    }

    public TaiSanCDDTO getTaiSanCD() {
        return taiSanCD;
    }

    public void setTaiSanCD(TaiSanCDDTO taiSanCD) {
        this.taiSanCD = taiSanCD;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ChiTietGiamTSCDDTO)) {
            return false;
        }

        ChiTietGiamTSCDDTO chiTietGiamTSCDDTO = (ChiTietGiamTSCDDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, chiTietGiamTSCDDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ChiTietGiamTSCDDTO{" +
            "id=" + getId() +
            ", ngayMua='" + getNgayMua() + "'" +
            ", soLuong=" + getSoLuong() +
            ", khauHao=" + getKhauHao() +
            ", soTienThanhLy=" + getSoTienThanhLy() +
            ", chiPhiMua=" + getChiPhiMua() +
            ", taiSanCD=" + getTaiSanCD() +
            "}";
    }
}
