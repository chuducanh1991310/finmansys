package com.group3.finmansys.service.dto;

import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.group3.finmansys.domain.KhoanPhaiTra} entity.
 */
public class KhoanPhaiTraDTO implements Serializable {

    private Long id;

    @NotNull
    private Instant ngayTao;

    @NotNull
    private Instant ngayPhaiTra;

    @NotNull
    @DecimalMin(value = "1")
    private Double soTien;

    private String ghiChu;

    private KhachHangDTO khachHang;

    private MaKeToanDTO maKeToan;

    private Set<PhieuChiDTO> dsPhieuChis = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getNgayTao() {
        return ngayTao;
    }

    public void setNgayTao(Instant ngayTao) {
        this.ngayTao = ngayTao;
    }

    public Instant getNgayPhaiTra() {
        return ngayPhaiTra;
    }

    public void setNgayPhaiTra(Instant ngayPhaiTra) {
        this.ngayPhaiTra = ngayPhaiTra;
    }

    public Double getSoTien() {
        return soTien;
    }

    public void setSoTien(Double soTien) {
        this.soTien = soTien;
    }

    public String getGhiChu() {
        return ghiChu;
    }

    public void setGhiChu(String ghiChu) {
        this.ghiChu = ghiChu;
    }

    public KhachHangDTO getKhachHang() {
        return khachHang;
    }

    public void setKhachHang(KhachHangDTO khachHang) {
        this.khachHang = khachHang;
    }

    public MaKeToanDTO getMaKeToan() {
        return maKeToan;
    }

    public void setMaKeToan(MaKeToanDTO maKeToan) {
        this.maKeToan = maKeToan;
    }

    public Set<PhieuChiDTO> getDsPhieuChis() {
        return dsPhieuChis;
    }

    public void setDsPhieuChis(Set<PhieuChiDTO> dsPhieuChis) {
        this.dsPhieuChis = dsPhieuChis;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof KhoanPhaiTraDTO)) {
            return false;
        }

        KhoanPhaiTraDTO khoanPhaiTraDTO = (KhoanPhaiTraDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, khoanPhaiTraDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "KhoanPhaiTraDTO{" +
            "id=" + getId() +
            ", ngayTao='" + getNgayTao() + "'" +
            ", ngayPhaiTra='" + getNgayPhaiTra() + "'" +
            ", soTien=" + getSoTien() +
            ", ghiChu='" + getGhiChu() + "'" +
            ", khachHang=" + getKhachHang() +
            ", maKeToan=" + getMaKeToan() +
            "}";
    }
}
