package com.group3.finmansys.service;

import com.group3.finmansys.domain.*; // for static metamodels
import com.group3.finmansys.domain.KhachSan;
import com.group3.finmansys.repository.KhachSanRepository;
import com.group3.finmansys.service.criteria.KhachSanCriteria;
import com.group3.finmansys.service.dto.KhachSanDTO;
import com.group3.finmansys.service.mapper.KhachSanMapper;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link KhachSan} entities in the database.
 * The main input is a {@link KhachSanCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link KhachSanDTO} or a {@link Page} of {@link KhachSanDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class KhachSanQueryService extends QueryService<KhachSan> {

    private final Logger log = LoggerFactory.getLogger(KhachSanQueryService.class);

    private final KhachSanRepository khachSanRepository;

    private final KhachSanMapper khachSanMapper;

    public KhachSanQueryService(KhachSanRepository khachSanRepository, KhachSanMapper khachSanMapper) {
        this.khachSanRepository = khachSanRepository;
        this.khachSanMapper = khachSanMapper;
    }

    /**
     * Return a {@link List} of {@link KhachSanDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<KhachSanDTO> findByCriteria(KhachSanCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<KhachSan> specification = createSpecification(criteria);
        return khachSanMapper.toDto(khachSanRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link KhachSanDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<KhachSanDTO> findByCriteria(KhachSanCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<KhachSan> specification = createSpecification(criteria);
        return khachSanRepository.findAll(specification, page).map(khachSanMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(KhachSanCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<KhachSan> specification = createSpecification(criteria);
        return khachSanRepository.count(specification);
    }

    /**
     * Function to convert {@link KhachSanCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<KhachSan> createSpecification(KhachSanCriteria criteria) {
        Specification<KhachSan> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), KhachSan_.id));
            }
            if (criteria.getTen() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTen(), KhachSan_.ten));
            }
            if (criteria.getDiaChi() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDiaChi(), KhachSan_.diaChi));
            }
            if (criteria.getEmail() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEmail(), KhachSan_.email));
            }
        }
        return specification;
    }
}
