package com.group3.finmansys.service;

import com.group3.finmansys.service.dto.DichVuDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.group3.finmansys.domain.DichVu}.
 */
public interface DichVuService {
    /**
     * Save a dichVu.
     *
     * @param dichVuDTO the entity to save.
     * @return the persisted entity.
     */
    DichVuDTO save(DichVuDTO dichVuDTO);

    /**
     * Partially updates a dichVu.
     *
     * @param dichVuDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<DichVuDTO> partialUpdate(DichVuDTO dichVuDTO);

    /**
     * Get all the dichVus.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<DichVuDTO> findAll(Pageable pageable);

    /**
     * Get the "id" dichVu.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<DichVuDTO> findOne(Long id);

    /**
     * Delete the "id" dichVu.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
