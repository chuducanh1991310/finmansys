package com.group3.finmansys.service;

import com.group3.finmansys.service.dto.PhongDatDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.group3.finmansys.domain.PhongDat}.
 */
public interface PhongDatService {
    /**
     * Save a phongDat.
     *
     * @param phongDatDTO the entity to save.
     * @return the persisted entity.
     */
    PhongDatDTO save(PhongDatDTO phongDatDTO);

    /**
     * Partially updates a phongDat.
     *
     * @param phongDatDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<PhongDatDTO> partialUpdate(PhongDatDTO phongDatDTO);

    /**
     * Get all the phongDats.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<PhongDatDTO> findAll(Pageable pageable);

    /**
     * Get the "id" phongDat.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<PhongDatDTO> findOne(Long id);

    /**
     * Delete the "id" phongDat.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
