package com.group3.finmansys.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.Instant;
import javax.persistence.*;
import javax.validation.constraints.*;

/**
 * A PhongDat.
 */
@Entity
@Table(name = "phong_dat")
public class PhongDat implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "ngay_dat", nullable = false)
    private Instant ngayDat;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = { "khachSan" }, allowSetters = true)
    private Phong phong;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = { "khachHang", "phongDats", "dichVuSuDungs" }, allowSetters = true)
    private PhongDuocDat phongDuocDat;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PhongDat id(Long id) {
        this.id = id;
        return this;
    }

    public Instant getNgayDat() {
        return this.ngayDat;
    }

    public PhongDat ngayDat(Instant ngayDat) {
        this.ngayDat = ngayDat;
        return this;
    }

    public void setNgayDat(Instant ngayDat) {
        this.ngayDat = ngayDat;
    }

    public Phong getPhong() {
        return this.phong;
    }

    public PhongDat phong(Phong phong) {
        this.setPhong(phong);
        return this;
    }

    public void setPhong(Phong phong) {
        this.phong = phong;
    }

    public PhongDuocDat getPhongDuocDat() {
        return this.phongDuocDat;
    }

    public PhongDat phongDuocDat(PhongDuocDat phongDuocDat) {
        this.setPhongDuocDat(phongDuocDat);
        return this;
    }

    public void setPhongDuocDat(PhongDuocDat phongDuocDat) {
        this.phongDuocDat = phongDuocDat;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PhongDat)) {
            return false;
        }
        return id != null && id.equals(((PhongDat) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PhongDat{" +
            "id=" + getId() +
            ", ngayDat='" + getNgayDat() + "'" +
            "}";
    }
}
