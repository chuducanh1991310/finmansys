package com.group3.finmansys.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;

/**
 * A NganSachNam.
 */
@Entity
@Table(name = "ngan_sach_nam")
public class NganSachNam implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "ten", nullable = false)
    private String ten;

    @NotNull
    @Min(value = 1)
    @Column(name = "nam", nullable = false)
    private Integer nam;

    @NotNull
    @DecimalMin(value = "1")
    @Column(name = "so_tien", nullable = false)
    private Double soTien;

    @ManyToOne(optional = false)
    @NotNull
    private KhachSan khachSan;

    @OneToMany(mappedBy = "nganSachNam", fetch = FetchType.EAGER)
    //    @JsonIgnoreProperties(value = { "nganSachNam" }, allowSetters = true)
    private Set<NganSachThang> nganSachThangs = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public NganSachNam id(Long id) {
        this.id = id;
        return this;
    }

    public String getTen() {
        return this.ten;
    }

    public NganSachNam ten(String ten) {
        this.ten = ten;
        return this;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public Integer getNam() {
        return this.nam;
    }

    public NganSachNam nam(Integer nam) {
        this.nam = nam;
        return this;
    }

    public void setNam(Integer nam) {
        this.nam = nam;
    }

    public Double getSoTien() {
        return this.soTien;
    }

    public NganSachNam soTien(Double soTien) {
        this.soTien = soTien;
        return this;
    }

    public void setSoTien(Double soTien) {
        this.soTien = soTien;
    }

    public KhachSan getKhachSan() {
        return this.khachSan;
    }

    public NganSachNam khachSan(KhachSan khachSan) {
        this.setKhachSan(khachSan);
        return this;
    }

    public void setKhachSan(KhachSan khachSan) {
        this.khachSan = khachSan;
    }

    public Set<NganSachThang> getNganSachThangs() {
        return this.nganSachThangs;
    }

    public NganSachNam nganSachThangs(Set<NganSachThang> nganSachThangs) {
        this.setNganSachThangs(nganSachThangs);
        return this;
    }

    public NganSachNam addNganSachThang(NganSachThang nganSachThang) {
        this.nganSachThangs.add(nganSachThang);
        nganSachThang.setNganSachNam(this);
        return this;
    }

    public NganSachNam removeNganSachThang(NganSachThang nganSachThang) {
        this.nganSachThangs.remove(nganSachThang);
        nganSachThang.setNganSachNam(null);
        return this;
    }

    public void setNganSachThangs(Set<NganSachThang> nganSachThangs) {
        if (this.nganSachThangs != null) {
            this.nganSachThangs.forEach(i -> i.setNganSachNam(null));
        }
        if (nganSachThangs != null) {
            nganSachThangs.forEach(i -> i.setNganSachNam(this));
        }
        this.nganSachThangs = nganSachThangs;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof NganSachNam)) {
            return false;
        }
        return id != null && id.equals(((NganSachNam) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "NganSachNam{" +
            "id=" + getId() +
            ", ten='" + getTen() + "'" +
            ", nam=" + getNam() +
            ", soTien=" + getSoTien() +
            "}";
    }
}
