package com.group3.finmansys.web.rest;

import com.group3.finmansys.repository.LoaiQuyRepository;
import com.group3.finmansys.service.LoaiQuyQueryService;
import com.group3.finmansys.service.LoaiQuyService;
import com.group3.finmansys.service.criteria.LoaiQuyCriteria;
import com.group3.finmansys.service.dto.LoaiQuyDTO;
import com.group3.finmansys.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.group3.finmansys.domain.LoaiQuy}.
 */
@RestController
@RequestMapping("/api")
public class LoaiQuyResource {

    private final Logger log = LoggerFactory.getLogger(LoaiQuyResource.class);

    private static final String ENTITY_NAME = "loaiQuy";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final LoaiQuyService loaiQuyService;

    private final LoaiQuyRepository loaiQuyRepository;

    private final LoaiQuyQueryService loaiQuyQueryService;

    public LoaiQuyResource(LoaiQuyService loaiQuyService, LoaiQuyRepository loaiQuyRepository, LoaiQuyQueryService loaiQuyQueryService) {
        this.loaiQuyService = loaiQuyService;
        this.loaiQuyRepository = loaiQuyRepository;
        this.loaiQuyQueryService = loaiQuyQueryService;
    }

    /**
     * {@code POST  /loai-quies} : Create a new loaiQuy.
     *
     * @param loaiQuyDTO the loaiQuyDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new loaiQuyDTO, or with status {@code 400 (Bad Request)} if the loaiQuy has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/loai-quies")
    public ResponseEntity<LoaiQuyDTO> createLoaiQuy(@Valid @RequestBody LoaiQuyDTO loaiQuyDTO) throws URISyntaxException {
        log.debug("REST request to save LoaiQuy : {}", loaiQuyDTO);
        if (loaiQuyDTO.getId() != null) {
            throw new BadRequestAlertException("A new loaiQuy cannot already have an ID", ENTITY_NAME, "idexists");
        }
        LoaiQuyDTO result = loaiQuyService.save(loaiQuyDTO);
        return ResponseEntity
            .created(new URI("/api/loai-quies/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /loai-quies/:id} : Updates an existing loaiQuy.
     *
     * @param id the id of the loaiQuyDTO to save.
     * @param loaiQuyDTO the loaiQuyDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated loaiQuyDTO,
     * or with status {@code 400 (Bad Request)} if the loaiQuyDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the loaiQuyDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/loai-quies/{id}")
    public ResponseEntity<LoaiQuyDTO> updateLoaiQuy(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody LoaiQuyDTO loaiQuyDTO
    ) throws URISyntaxException {
        log.debug("REST request to update LoaiQuy : {}, {}", id, loaiQuyDTO);
        if (loaiQuyDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, loaiQuyDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!loaiQuyRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        LoaiQuyDTO result = loaiQuyService.save(loaiQuyDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, loaiQuyDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /loai-quies/:id} : Partial updates given fields of an existing loaiQuy, field will ignore if it is null
     *
     * @param id the id of the loaiQuyDTO to save.
     * @param loaiQuyDTO the loaiQuyDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated loaiQuyDTO,
     * or with status {@code 400 (Bad Request)} if the loaiQuyDTO is not valid,
     * or with status {@code 404 (Not Found)} if the loaiQuyDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the loaiQuyDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/loai-quies/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<LoaiQuyDTO> partialUpdateLoaiQuy(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody LoaiQuyDTO loaiQuyDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update LoaiQuy partially : {}, {}", id, loaiQuyDTO);
        if (loaiQuyDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, loaiQuyDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!loaiQuyRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<LoaiQuyDTO> result = loaiQuyService.partialUpdate(loaiQuyDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, loaiQuyDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /loai-quies} : get all the loaiQuies.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of loaiQuies in body.
     */
    @GetMapping("/loai-quies")
    public ResponseEntity<List<LoaiQuyDTO>> getAllLoaiQuies(LoaiQuyCriteria criteria, Pageable pageable) {
        log.debug("REST request to get LoaiQuies by criteria: {}", criteria);
        Page<LoaiQuyDTO> page = loaiQuyQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /loai-quies/count} : count all the loaiQuies.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/loai-quies/count")
    public ResponseEntity<Long> countLoaiQuies(LoaiQuyCriteria criteria) {
        log.debug("REST request to count LoaiQuies by criteria: {}", criteria);
        return ResponseEntity.ok().body(loaiQuyQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /loai-quies/:id} : get the "id" loaiQuy.
     *
     * @param id the id of the loaiQuyDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the loaiQuyDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/loai-quies/{id}")
    public ResponseEntity<LoaiQuyDTO> getLoaiQuy(@PathVariable Long id) {
        log.debug("REST request to get LoaiQuy : {}", id);
        Optional<LoaiQuyDTO> loaiQuyDTO = loaiQuyService.findOne(id);
        return ResponseUtil.wrapOrNotFound(loaiQuyDTO);
    }

    /**
     * {@code DELETE  /loai-quies/:id} : delete the "id" loaiQuy.
     *
     * @param id the id of the loaiQuyDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/loai-quies/{id}")
    public ResponseEntity<Void> deleteLoaiQuy(@PathVariable Long id) {
        log.debug("REST request to delete LoaiQuy : {}", id);
        loaiQuyService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
