package com.group3.finmansys.web.rest;

import com.group3.finmansys.repository.NganSachThangRepository;
import com.group3.finmansys.service.NganSachThangQueryService;
import com.group3.finmansys.service.NganSachThangService;
import com.group3.finmansys.service.criteria.NganSachThangCriteria;
import com.group3.finmansys.service.dto.NganSachThangDTO;
import com.group3.finmansys.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.group3.finmansys.domain.NganSachThang}.
 */
@RestController
@RequestMapping("/api")
public class NganSachThangResource {

    private final Logger log = LoggerFactory.getLogger(NganSachThangResource.class);

    private static final String ENTITY_NAME = "nganSachThang";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final NganSachThangService nganSachThangService;

    private final NganSachThangRepository nganSachThangRepository;

    private final NganSachThangQueryService nganSachThangQueryService;

    public NganSachThangResource(
        NganSachThangService nganSachThangService,
        NganSachThangRepository nganSachThangRepository,
        NganSachThangQueryService nganSachThangQueryService
    ) {
        this.nganSachThangService = nganSachThangService;
        this.nganSachThangRepository = nganSachThangRepository;
        this.nganSachThangQueryService = nganSachThangQueryService;
    }

    /**
     * {@code POST  /ngan-sach-thangs} : Create a new nganSachThang.
     *
     * @param nganSachThangDTO the nganSachThangDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new nganSachThangDTO, or with status {@code 400 (Bad Request)} if the nganSachThang has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/ngan-sach-thangs")
    public ResponseEntity<NganSachThangDTO> createNganSachThang(@Valid @RequestBody NganSachThangDTO nganSachThangDTO)
        throws URISyntaxException {
        log.debug("REST request to save NganSachThang : {}", nganSachThangDTO);
        if (nganSachThangDTO.getId() != null) {
            throw new BadRequestAlertException("A new nganSachThang cannot already have an ID", ENTITY_NAME, "idexists");
        }
        NganSachThangDTO result = nganSachThangService.save(nganSachThangDTO);
        return ResponseEntity
            .created(new URI("/api/ngan-sach-thangs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /ngan-sach-thangs/:id} : Updates an existing nganSachThang.
     *
     * @param id the id of the nganSachThangDTO to save.
     * @param nganSachThangDTO the nganSachThangDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated nganSachThangDTO,
     * or with status {@code 400 (Bad Request)} if the nganSachThangDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the nganSachThangDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/ngan-sach-thangs/{id}")
    public ResponseEntity<NganSachThangDTO> updateNganSachThang(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody NganSachThangDTO nganSachThangDTO
    ) throws URISyntaxException {
        log.debug("REST request to update NganSachThang : {}, {}", id, nganSachThangDTO);
        if (nganSachThangDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, nganSachThangDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!nganSachThangRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        NganSachThangDTO result = nganSachThangService.save(nganSachThangDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, nganSachThangDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /ngan-sach-thangs/:id} : Partial updates given fields of an existing nganSachThang, field will ignore if it is null
     *
     * @param id the id of the nganSachThangDTO to save.
     * @param nganSachThangDTO the nganSachThangDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated nganSachThangDTO,
     * or with status {@code 400 (Bad Request)} if the nganSachThangDTO is not valid,
     * or with status {@code 404 (Not Found)} if the nganSachThangDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the nganSachThangDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/ngan-sach-thangs/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<NganSachThangDTO> partialUpdateNganSachThang(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody NganSachThangDTO nganSachThangDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update NganSachThang partially : {}, {}", id, nganSachThangDTO);
        if (nganSachThangDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, nganSachThangDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!nganSachThangRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<NganSachThangDTO> result = nganSachThangService.partialUpdate(nganSachThangDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, nganSachThangDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /ngan-sach-thangs} : get all the nganSachThangs.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of nganSachThangs in body.
     */
    @GetMapping("/ngan-sach-thangs")
    public ResponseEntity<List<NganSachThangDTO>> getAllNganSachThangs(NganSachThangCriteria criteria, Pageable pageable) {
        log.debug("REST request to get NganSachThangs by criteria: {}", criteria);
        Page<NganSachThangDTO> page = nganSachThangQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /ngan-sach-thangs/count} : count all the nganSachThangs.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/ngan-sach-thangs/count")
    public ResponseEntity<Long> countNganSachThangs(NganSachThangCriteria criteria) {
        log.debug("REST request to count NganSachThangs by criteria: {}", criteria);
        return ResponseEntity.ok().body(nganSachThangQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /ngan-sach-thangs/:id} : get the "id" nganSachThang.
     *
     * @param id the id of the nganSachThangDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the nganSachThangDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/ngan-sach-thangs/{id}")
    public ResponseEntity<NganSachThangDTO> getNganSachThang(@PathVariable Long id) {
        log.debug("REST request to get NganSachThang : {}", id);
        Optional<NganSachThangDTO> nganSachThangDTO = nganSachThangService.findOne(id);
        return ResponseUtil.wrapOrNotFound(nganSachThangDTO);
    }

    /**
     * {@code DELETE  /ngan-sach-thangs/:id} : delete the "id" nganSachThang.
     *
     * @param id the id of the nganSachThangDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/ngan-sach-thangs/{id}")
    public ResponseEntity<Void> deleteNganSachThang(@PathVariable Long id) {
        log.debug("REST request to delete NganSachThang : {}", id);
        nganSachThangService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
