package com.group3.finmansys.web.rest;

import com.group3.finmansys.repository.ChiTietGiamTSCDRepository;
import com.group3.finmansys.service.ChiTietGiamTSCDQueryService;
import com.group3.finmansys.service.ChiTietGiamTSCDService;
import com.group3.finmansys.service.criteria.ChiTietGiamTSCDCriteria;
import com.group3.finmansys.service.dto.ChiTietGiamTSCDDTO;
import com.group3.finmansys.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.group3.finmansys.domain.ChiTietGiamTSCD}.
 */
@RestController
@RequestMapping("/api")
public class ChiTietGiamTSCDResource {

    private final Logger log = LoggerFactory.getLogger(ChiTietGiamTSCDResource.class);

    private static final String ENTITY_NAME = "chiTietGiamTSCD";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ChiTietGiamTSCDService chiTietGiamTSCDService;

    private final ChiTietGiamTSCDRepository chiTietGiamTSCDRepository;

    private final ChiTietGiamTSCDQueryService chiTietGiamTSCDQueryService;

    public ChiTietGiamTSCDResource(
        ChiTietGiamTSCDService chiTietGiamTSCDService,
        ChiTietGiamTSCDRepository chiTietGiamTSCDRepository,
        ChiTietGiamTSCDQueryService chiTietGiamTSCDQueryService
    ) {
        this.chiTietGiamTSCDService = chiTietGiamTSCDService;
        this.chiTietGiamTSCDRepository = chiTietGiamTSCDRepository;
        this.chiTietGiamTSCDQueryService = chiTietGiamTSCDQueryService;
    }

    /**
     * {@code POST  /chi-tiet-giam-tscds} : Create a new chiTietGiamTSCD.
     *
     * @param chiTietGiamTSCDDTO the chiTietGiamTSCDDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new chiTietGiamTSCDDTO, or with status {@code 400 (Bad Request)} if the chiTietGiamTSCD has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/chi-tiet-giam-tscds")
    public ResponseEntity<ChiTietGiamTSCDDTO> createChiTietGiamTSCD(@Valid @RequestBody ChiTietGiamTSCDDTO chiTietGiamTSCDDTO)
        throws URISyntaxException {
        log.debug("REST request to save ChiTietGiamTSCD : {}", chiTietGiamTSCDDTO);
        if (chiTietGiamTSCDDTO.getId() != null) {
            throw new BadRequestAlertException("A new chiTietGiamTSCD cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ChiTietGiamTSCDDTO result = chiTietGiamTSCDService.save(chiTietGiamTSCDDTO);
        return ResponseEntity
            .created(new URI("/api/chi-tiet-giam-tscds/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /chi-tiet-giam-tscds/:id} : Updates an existing chiTietGiamTSCD.
     *
     * @param id the id of the chiTietGiamTSCDDTO to save.
     * @param chiTietGiamTSCDDTO the chiTietGiamTSCDDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated chiTietGiamTSCDDTO,
     * or with status {@code 400 (Bad Request)} if the chiTietGiamTSCDDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the chiTietGiamTSCDDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/chi-tiet-giam-tscds/{id}")
    public ResponseEntity<ChiTietGiamTSCDDTO> updateChiTietGiamTSCD(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody ChiTietGiamTSCDDTO chiTietGiamTSCDDTO
    ) throws URISyntaxException {
        log.debug("REST request to update ChiTietGiamTSCD : {}, {}", id, chiTietGiamTSCDDTO);
        if (chiTietGiamTSCDDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, chiTietGiamTSCDDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!chiTietGiamTSCDRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        ChiTietGiamTSCDDTO result = chiTietGiamTSCDService.save(chiTietGiamTSCDDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, chiTietGiamTSCDDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /chi-tiet-giam-tscds/:id} : Partial updates given fields of an existing chiTietGiamTSCD, field will ignore if it is null
     *
     * @param id the id of the chiTietGiamTSCDDTO to save.
     * @param chiTietGiamTSCDDTO the chiTietGiamTSCDDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated chiTietGiamTSCDDTO,
     * or with status {@code 400 (Bad Request)} if the chiTietGiamTSCDDTO is not valid,
     * or with status {@code 404 (Not Found)} if the chiTietGiamTSCDDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the chiTietGiamTSCDDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/chi-tiet-giam-tscds/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<ChiTietGiamTSCDDTO> partialUpdateChiTietGiamTSCD(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody ChiTietGiamTSCDDTO chiTietGiamTSCDDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update ChiTietGiamTSCD partially : {}, {}", id, chiTietGiamTSCDDTO);
        if (chiTietGiamTSCDDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, chiTietGiamTSCDDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!chiTietGiamTSCDRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<ChiTietGiamTSCDDTO> result = chiTietGiamTSCDService.partialUpdate(chiTietGiamTSCDDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, chiTietGiamTSCDDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /chi-tiet-giam-tscds} : get all the chiTietGiamTSCDS.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of chiTietGiamTSCDS in body.
     */
    @GetMapping("/chi-tiet-giam-tscds")
    public ResponseEntity<List<ChiTietGiamTSCDDTO>> getAllChiTietGiamTSCDS(ChiTietGiamTSCDCriteria criteria, Pageable pageable) {
        log.debug("REST request to get ChiTietGiamTSCDS by criteria: {}", criteria);
        Page<ChiTietGiamTSCDDTO> page = chiTietGiamTSCDQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /chi-tiet-giam-tscds/count} : count all the chiTietGiamTSCDS.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/chi-tiet-giam-tscds/count")
    public ResponseEntity<Long> countChiTietGiamTSCDS(ChiTietGiamTSCDCriteria criteria) {
        log.debug("REST request to count ChiTietGiamTSCDS by criteria: {}", criteria);
        return ResponseEntity.ok().body(chiTietGiamTSCDQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /chi-tiet-giam-tscds/:id} : get the "id" chiTietGiamTSCD.
     *
     * @param id the id of the chiTietGiamTSCDDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the chiTietGiamTSCDDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/chi-tiet-giam-tscds/{id}")
    public ResponseEntity<ChiTietGiamTSCDDTO> getChiTietGiamTSCD(@PathVariable Long id) {
        log.debug("REST request to get ChiTietGiamTSCD : {}", id);
        Optional<ChiTietGiamTSCDDTO> chiTietGiamTSCDDTO = chiTietGiamTSCDService.findOne(id);
        return ResponseUtil.wrapOrNotFound(chiTietGiamTSCDDTO);
    }

    /**
     * {@code DELETE  /chi-tiet-giam-tscds/:id} : delete the "id" chiTietGiamTSCD.
     *
     * @param id the id of the chiTietGiamTSCDDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/chi-tiet-giam-tscds/{id}")
    public ResponseEntity<Void> deleteChiTietGiamTSCD(@PathVariable Long id) {
        log.debug("REST request to delete ChiTietGiamTSCD : {}", id);
        chiTietGiamTSCDService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
