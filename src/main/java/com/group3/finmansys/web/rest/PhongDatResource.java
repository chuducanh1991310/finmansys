package com.group3.finmansys.web.rest;

import com.group3.finmansys.repository.PhongDatRepository;
import com.group3.finmansys.service.PhongDatQueryService;
import com.group3.finmansys.service.PhongDatService;
import com.group3.finmansys.service.criteria.PhongDatCriteria;
import com.group3.finmansys.service.dto.PhongDatDTO;
import com.group3.finmansys.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.group3.finmansys.domain.PhongDat}.
 */
@RestController
@RequestMapping("/api")
public class PhongDatResource {

    private final Logger log = LoggerFactory.getLogger(PhongDatResource.class);

    private static final String ENTITY_NAME = "phongDat";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PhongDatService phongDatService;

    private final PhongDatRepository phongDatRepository;

    private final PhongDatQueryService phongDatQueryService;

    public PhongDatResource(
        PhongDatService phongDatService,
        PhongDatRepository phongDatRepository,
        PhongDatQueryService phongDatQueryService
    ) {
        this.phongDatService = phongDatService;
        this.phongDatRepository = phongDatRepository;
        this.phongDatQueryService = phongDatQueryService;
    }

    /**
     * {@code POST  /phong-dats} : Create a new phongDat.
     *
     * @param phongDatDTO the phongDatDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new phongDatDTO, or with status {@code 400 (Bad Request)} if the phongDat has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/phong-dats")
    public ResponseEntity<PhongDatDTO> createPhongDat(@Valid @RequestBody PhongDatDTO phongDatDTO) throws URISyntaxException {
        log.debug("REST request to save PhongDat : {}", phongDatDTO);
        if (phongDatDTO.getId() != null) {
            throw new BadRequestAlertException("A new phongDat cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PhongDatDTO result = phongDatService.save(phongDatDTO);
        return ResponseEntity
            .created(new URI("/api/phong-dats/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /phong-dats/:id} : Updates an existing phongDat.
     *
     * @param id the id of the phongDatDTO to save.
     * @param phongDatDTO the phongDatDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated phongDatDTO,
     * or with status {@code 400 (Bad Request)} if the phongDatDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the phongDatDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/phong-dats/{id}")
    public ResponseEntity<PhongDatDTO> updatePhongDat(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody PhongDatDTO phongDatDTO
    ) throws URISyntaxException {
        log.debug("REST request to update PhongDat : {}, {}", id, phongDatDTO);
        if (phongDatDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, phongDatDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!phongDatRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        PhongDatDTO result = phongDatService.save(phongDatDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, phongDatDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /phong-dats/:id} : Partial updates given fields of an existing phongDat, field will ignore if it is null
     *
     * @param id the id of the phongDatDTO to save.
     * @param phongDatDTO the phongDatDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated phongDatDTO,
     * or with status {@code 400 (Bad Request)} if the phongDatDTO is not valid,
     * or with status {@code 404 (Not Found)} if the phongDatDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the phongDatDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/phong-dats/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<PhongDatDTO> partialUpdatePhongDat(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody PhongDatDTO phongDatDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update PhongDat partially : {}, {}", id, phongDatDTO);
        if (phongDatDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, phongDatDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!phongDatRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<PhongDatDTO> result = phongDatService.partialUpdate(phongDatDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, phongDatDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /phong-dats} : get all the phongDats.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of phongDats in body.
     */
    @GetMapping("/phong-dats")
    public ResponseEntity<List<PhongDatDTO>> getAllPhongDats(PhongDatCriteria criteria, Pageable pageable) {
        log.debug("REST request to get PhongDats by criteria: {}", criteria);
        Page<PhongDatDTO> page = phongDatQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /phong-dats/count} : count all the phongDats.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/phong-dats/count")
    public ResponseEntity<Long> countPhongDats(PhongDatCriteria criteria) {
        log.debug("REST request to count PhongDats by criteria: {}", criteria);
        return ResponseEntity.ok().body(phongDatQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /phong-dats/:id} : get the "id" phongDat.
     *
     * @param id the id of the phongDatDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the phongDatDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/phong-dats/{id}")
    public ResponseEntity<PhongDatDTO> getPhongDat(@PathVariable Long id) {
        log.debug("REST request to get PhongDat : {}", id);
        Optional<PhongDatDTO> phongDatDTO = phongDatService.findOne(id);
        return ResponseUtil.wrapOrNotFound(phongDatDTO);
    }

    /**
     * {@code DELETE  /phong-dats/:id} : delete the "id" phongDat.
     *
     * @param id the id of the phongDatDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/phong-dats/{id}")
    public ResponseEntity<Void> deletePhongDat(@PathVariable Long id) {
        log.debug("REST request to delete PhongDat : {}", id);
        phongDatService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
