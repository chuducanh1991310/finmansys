import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IPhieuThu, PhieuThu } from '../phieu-thu.model';
import { PhieuThuService } from '../service/phieu-thu.service';

@Injectable({ providedIn: 'root' })
export class PhieuThuRoutingResolveService implements Resolve<IPhieuThu> {
  constructor(protected service: PhieuThuService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IPhieuThu> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((phieuThu: HttpResponse<PhieuThu>) => {
          if (phieuThu.body) {
            return of(phieuThu.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new PhieuThu());
  }
}
