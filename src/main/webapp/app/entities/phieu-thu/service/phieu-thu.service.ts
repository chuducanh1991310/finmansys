import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as dayjs from 'dayjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IPhieuThu, getPhieuThuIdentifier } from '../phieu-thu.model';

export type EntityResponseType = HttpResponse<IPhieuThu>;
export type EntityArrayResponseType = HttpResponse<IPhieuThu[]>;

@Injectable({ providedIn: 'root' })
export class PhieuThuService {
  public resourceUrl = this.applicationConfigService.getEndpointFor('api/phieu-thus');

  constructor(protected http: HttpClient, private applicationConfigService: ApplicationConfigService) {}

  create(phieuThu: IPhieuThu): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(phieuThu);
    return this.http
      .post<IPhieuThu>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(phieuThu: IPhieuThu): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(phieuThu);
    return this.http
      .put<IPhieuThu>(`${this.resourceUrl}/${getPhieuThuIdentifier(phieuThu) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(phieuThu: IPhieuThu): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(phieuThu);
    return this.http
      .patch<IPhieuThu>(`${this.resourceUrl}/${getPhieuThuIdentifier(phieuThu) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IPhieuThu>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  getPredictions(year: number, month: number, khachSanId: number): Observable<HttpResponse<any>> {
    return this.http.get<any>(`http://34.126.155.87:5000/api/predict/${year}/${month}/${khachSanId}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IPhieuThu[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addPhieuThuToCollectionIfMissing(phieuThuCollection: IPhieuThu[], ...phieuThusToCheck: (IPhieuThu | null | undefined)[]): IPhieuThu[] {
    const phieuThus: IPhieuThu[] = phieuThusToCheck.filter(isPresent);
    if (phieuThus.length > 0) {
      const phieuThuCollectionIdentifiers = phieuThuCollection.map(phieuThuItem => getPhieuThuIdentifier(phieuThuItem)!);
      const phieuThusToAdd = phieuThus.filter(phieuThuItem => {
        const phieuThuIdentifier = getPhieuThuIdentifier(phieuThuItem);
        if (phieuThuIdentifier == null || phieuThuCollectionIdentifiers.includes(phieuThuIdentifier)) {
          return false;
        }
        phieuThuCollectionIdentifiers.push(phieuThuIdentifier);
        return true;
      });
      return [...phieuThusToAdd, ...phieuThuCollection];
    }
    return phieuThuCollection;
  }

  protected convertDateFromClient(phieuThu: IPhieuThu): IPhieuThu {
    return Object.assign({}, phieuThu, {
      ngayChungTu: phieuThu.ngayChungTu?.isValid() ? phieuThu.ngayChungTu.toJSON() : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.ngayChungTu = res.body.ngayChungTu ? dayjs(res.body.ngayChungTu) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((phieuThu: IPhieuThu) => {
        phieuThu.ngayChungTu = phieuThu.ngayChungTu ? dayjs(phieuThu.ngayChungTu) : undefined;
      });
    }
    return res;
  }
}
