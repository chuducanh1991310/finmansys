import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IPhieuThu } from '../phieu-thu.model';
import { PhieuThuService } from '../service/phieu-thu.service';

@Component({
  templateUrl: './phieu-thu-delete-dialog.component.html',
})
export class PhieuThuDeleteDialogComponent {
  phieuThu?: IPhieuThu;

  constructor(protected phieuThuService: PhieuThuService, public activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.phieuThuService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
