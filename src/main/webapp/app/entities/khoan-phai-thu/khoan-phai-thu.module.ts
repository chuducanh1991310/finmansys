import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';
import { KhoanPhaiThuComponent } from './list/khoan-phai-thu.component';
import { KhoanPhaiThuDetailComponent } from './detail/khoan-phai-thu-detail.component';
import { KhoanPhaiThuUpdateComponent } from './update/khoan-phai-thu-update.component';
import { KhoanPhaiThuDeleteDialogComponent } from './delete/khoan-phai-thu-delete-dialog.component';
import { KhoanPhaiThuRoutingModule } from './route/khoan-phai-thu-routing.module';

@NgModule({
  imports: [SharedModule, KhoanPhaiThuRoutingModule],
  declarations: [KhoanPhaiThuComponent, KhoanPhaiThuDetailComponent, KhoanPhaiThuUpdateComponent, KhoanPhaiThuDeleteDialogComponent],
  entryComponents: [KhoanPhaiThuDeleteDialogComponent],
})
export class KhoanPhaiThuModule {}
