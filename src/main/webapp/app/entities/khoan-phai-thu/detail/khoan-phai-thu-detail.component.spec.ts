import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { KhoanPhaiThuDetailComponent } from './khoan-phai-thu-detail.component';

describe('Component Tests', () => {
  describe('KhoanPhaiThu Management Detail Component', () => {
    let comp: KhoanPhaiThuDetailComponent;
    let fixture: ComponentFixture<KhoanPhaiThuDetailComponent>;

    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [KhoanPhaiThuDetailComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: { data: of({ khoanPhaiThu: { id: 123 } }) },
          },
        ],
      })
        .overrideTemplate(KhoanPhaiThuDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(KhoanPhaiThuDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load khoanPhaiThu on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.khoanPhaiThu).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
