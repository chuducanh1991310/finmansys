import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IKhoanPhaiThu } from '../khoan-phai-thu.model';

@Component({
  selector: 'jhi-khoan-phai-thu-detail',
  templateUrl: './khoan-phai-thu-detail.component.html',
})
export class KhoanPhaiThuDetailComponent implements OnInit {
  khoanPhaiThu: IKhoanPhaiThu | null = null;

  soTienDaNhan = 0;

  soTienConThieu = 0;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ khoanPhaiThu }) => {
      this.khoanPhaiThu = khoanPhaiThu;
      for (const phieuThu of khoanPhaiThu.phieuThus) {
        this.soTienDaNhan = this.soTienDaNhan + Number(phieuThu.soTien);
      }
      this.soTienConThieu = khoanPhaiThu.soTien - this.soTienDaNhan;
      if (this.soTienConThieu < 0) {
        this.soTienConThieu = 0;
      }
    });
  }

  previousState(): void {
    window.history.back();
  }
}
