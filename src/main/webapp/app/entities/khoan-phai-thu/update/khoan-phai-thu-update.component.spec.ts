jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { KhoanPhaiThuService } from '../service/khoan-phai-thu.service';
import { IKhoanPhaiThu, KhoanPhaiThu } from '../khoan-phai-thu.model';
import { IKhachHang } from 'app/entities/khach-hang/khach-hang.model';
import { KhachHangService } from 'app/entities/khach-hang/service/khach-hang.service';
import { IMaKeToan } from 'app/entities/ma-ke-toan/ma-ke-toan.model';
import { MaKeToanService } from 'app/entities/ma-ke-toan/service/ma-ke-toan.service';
import { IPhongDuocDat } from 'app/entities/phong-duoc-dat/phong-duoc-dat.model';
import { PhongDuocDatService } from 'app/entities/phong-duoc-dat/service/phong-duoc-dat.service';

import { KhoanPhaiThuUpdateComponent } from './khoan-phai-thu-update.component';

describe('Component Tests', () => {
  describe('KhoanPhaiThu Management Update Component', () => {
    let comp: KhoanPhaiThuUpdateComponent;
    let fixture: ComponentFixture<KhoanPhaiThuUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let khoanPhaiThuService: KhoanPhaiThuService;
    let khachHangService: KhachHangService;
    let maKeToanService: MaKeToanService;
    let phongDuocDatService: PhongDuocDatService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [KhoanPhaiThuUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(KhoanPhaiThuUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(KhoanPhaiThuUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      khoanPhaiThuService = TestBed.inject(KhoanPhaiThuService);
      khachHangService = TestBed.inject(KhachHangService);
      maKeToanService = TestBed.inject(MaKeToanService);
      phongDuocDatService = TestBed.inject(PhongDuocDatService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should call KhachHang query and add missing value', () => {
        const khoanPhaiThu: IKhoanPhaiThu = { id: 456 };
        const khachHang: IKhachHang = { id: 71646 };
        khoanPhaiThu.khachHang = khachHang;

        const khachHangCollection: IKhachHang[] = [{ id: 29324 }];
        spyOn(khachHangService, 'query').and.returnValue(of(new HttpResponse({ body: khachHangCollection })));
        const additionalKhachHangs = [khachHang];
        const expectedCollection: IKhachHang[] = [...additionalKhachHangs, ...khachHangCollection];
        spyOn(khachHangService, 'addKhachHangToCollectionIfMissing').and.returnValue(expectedCollection);

        activatedRoute.data = of({ khoanPhaiThu });
        comp.ngOnInit();

        expect(khachHangService.query).toHaveBeenCalled();
        expect(khachHangService.addKhachHangToCollectionIfMissing).toHaveBeenCalledWith(khachHangCollection, ...additionalKhachHangs);
        expect(comp.khachHangsSharedCollection).toEqual(expectedCollection);
      });

      it('Should call MaKeToan query and add missing value', () => {
        const khoanPhaiThu: IKhoanPhaiThu = { id: 456 };
        const maKeToan: IMaKeToan = { id: 53263 };
        khoanPhaiThu.maKeToan = maKeToan;

        const maKeToanCollection: IMaKeToan[] = [{ id: 10718 }];
        spyOn(maKeToanService, 'query').and.returnValue(of(new HttpResponse({ body: maKeToanCollection })));
        const additionalMaKeToans = [maKeToan];
        const expectedCollection: IMaKeToan[] = [...additionalMaKeToans, ...maKeToanCollection];
        spyOn(maKeToanService, 'addMaKeToanToCollectionIfMissing').and.returnValue(expectedCollection);

        activatedRoute.data = of({ khoanPhaiThu });
        comp.ngOnInit();

        expect(maKeToanService.query).toHaveBeenCalled();
        expect(maKeToanService.addMaKeToanToCollectionIfMissing).toHaveBeenCalledWith(maKeToanCollection, ...additionalMaKeToans);
        expect(comp.maKeToansSharedCollection).toEqual(expectedCollection);
      });

      it('Should call PhongDuocDat query and add missing value', () => {
        const khoanPhaiThu: IKhoanPhaiThu = { id: 456 };
        const phongDuocDat: IPhongDuocDat = { id: 65351 };
        khoanPhaiThu.phongDuocDat = phongDuocDat;

        const phongDuocDatCollection: IPhongDuocDat[] = [{ id: 89075 }];
        spyOn(phongDuocDatService, 'query').and.returnValue(of(new HttpResponse({ body: phongDuocDatCollection })));
        const additionalPhongDuocDats = [phongDuocDat];
        const expectedCollection: IPhongDuocDat[] = [...additionalPhongDuocDats, ...phongDuocDatCollection];
        spyOn(phongDuocDatService, 'addPhongDuocDatToCollectionIfMissing').and.returnValue(expectedCollection);

        activatedRoute.data = of({ khoanPhaiThu });
        comp.ngOnInit();

        expect(phongDuocDatService.query).toHaveBeenCalled();
        expect(phongDuocDatService.addPhongDuocDatToCollectionIfMissing).toHaveBeenCalledWith(
          phongDuocDatCollection,
          ...additionalPhongDuocDats
        );
        expect(comp.phongDuocDatsSharedCollection).toEqual(expectedCollection);
      });

      it('Should update editForm', () => {
        const khoanPhaiThu: IKhoanPhaiThu = { id: 456 };
        const khachHang: IKhachHang = { id: 89279 };
        khoanPhaiThu.khachHang = khachHang;
        const maKeToan: IMaKeToan = { id: 25204 };
        khoanPhaiThu.maKeToan = maKeToan;
        const phongDuocDat: IPhongDuocDat = { id: 67195 };
        khoanPhaiThu.phongDuocDat = phongDuocDat;

        activatedRoute.data = of({ khoanPhaiThu });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(khoanPhaiThu));
        expect(comp.khachHangsSharedCollection).toContain(khachHang);
        expect(comp.maKeToansSharedCollection).toContain(maKeToan);
        expect(comp.phongDuocDatsSharedCollection).toContain(phongDuocDat);
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const khoanPhaiThu = { id: 123 };
        spyOn(khoanPhaiThuService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ khoanPhaiThu });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: khoanPhaiThu }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(khoanPhaiThuService.update).toHaveBeenCalledWith(khoanPhaiThu);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const khoanPhaiThu = new KhoanPhaiThu();
        spyOn(khoanPhaiThuService, 'create').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ khoanPhaiThu });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: khoanPhaiThu }));
        saveSubject.complete();

        // THEN
        expect(khoanPhaiThuService.create).toHaveBeenCalledWith(khoanPhaiThu);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject();
        const khoanPhaiThu = { id: 123 };
        spyOn(khoanPhaiThuService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ khoanPhaiThu });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(khoanPhaiThuService.update).toHaveBeenCalledWith(khoanPhaiThu);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });

    describe('Tracking relationships identifiers', () => {
      describe('trackKhachHangById', () => {
        it('Should return tracked KhachHang primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackKhachHangById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });

      describe('trackMaKeToanById', () => {
        it('Should return tracked MaKeToan primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackMaKeToanById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });

      describe('trackPhongDuocDatById', () => {
        it('Should return tracked PhongDuocDat primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackPhongDuocDatById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });
    });
  });
});
