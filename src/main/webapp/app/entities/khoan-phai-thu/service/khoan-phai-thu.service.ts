import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as dayjs from 'dayjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IKhoanPhaiThu, getKhoanPhaiThuIdentifier } from '../khoan-phai-thu.model';

export type EntityResponseType = HttpResponse<IKhoanPhaiThu>;
export type EntityArrayResponseType = HttpResponse<IKhoanPhaiThu[]>;

@Injectable({ providedIn: 'root' })
export class KhoanPhaiThuService {
  public resourceUrl = this.applicationConfigService.getEndpointFor('api/khoan-phai-thus');

  constructor(protected http: HttpClient, private applicationConfigService: ApplicationConfigService) {}

  create(khoanPhaiThu: IKhoanPhaiThu): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(khoanPhaiThu);
    return this.http
      .post<IKhoanPhaiThu>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(khoanPhaiThu: IKhoanPhaiThu): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(khoanPhaiThu);
    return this.http
      .put<IKhoanPhaiThu>(`${this.resourceUrl}/${getKhoanPhaiThuIdentifier(khoanPhaiThu) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(khoanPhaiThu: IKhoanPhaiThu): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(khoanPhaiThu);
    return this.http
      .patch<IKhoanPhaiThu>(`${this.resourceUrl}/${getKhoanPhaiThuIdentifier(khoanPhaiThu) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IKhoanPhaiThu>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IKhoanPhaiThu[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addKhoanPhaiThuToCollectionIfMissing(
    khoanPhaiThuCollection: IKhoanPhaiThu[],
    ...khoanPhaiThusToCheck: (IKhoanPhaiThu | null | undefined)[]
  ): IKhoanPhaiThu[] {
    const khoanPhaiThus: IKhoanPhaiThu[] = khoanPhaiThusToCheck.filter(isPresent);
    if (khoanPhaiThus.length > 0) {
      const khoanPhaiThuCollectionIdentifiers = khoanPhaiThuCollection.map(
        khoanPhaiThuItem => getKhoanPhaiThuIdentifier(khoanPhaiThuItem)!
      );
      const khoanPhaiThusToAdd = khoanPhaiThus.filter(khoanPhaiThuItem => {
        const khoanPhaiThuIdentifier = getKhoanPhaiThuIdentifier(khoanPhaiThuItem);
        if (khoanPhaiThuIdentifier == null || khoanPhaiThuCollectionIdentifiers.includes(khoanPhaiThuIdentifier)) {
          return false;
        }
        khoanPhaiThuCollectionIdentifiers.push(khoanPhaiThuIdentifier);
        return true;
      });
      return [...khoanPhaiThusToAdd, ...khoanPhaiThuCollection];
    }
    return khoanPhaiThuCollection;
  }

  protected convertDateFromClient(khoanPhaiThu: IKhoanPhaiThu): IKhoanPhaiThu {
    return Object.assign({}, khoanPhaiThu, {
      ngayTao: khoanPhaiThu.ngayTao?.isValid() ? khoanPhaiThu.ngayTao.toJSON() : undefined,
      ngayPhaiThu: khoanPhaiThu.ngayPhaiThu?.isValid() ? khoanPhaiThu.ngayPhaiThu.toJSON() : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.ngayTao = res.body.ngayTao ? dayjs(res.body.ngayTao) : undefined;
      res.body.ngayPhaiThu = res.body.ngayPhaiThu ? dayjs(res.body.ngayPhaiThu) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((khoanPhaiThu: IKhoanPhaiThu) => {
        khoanPhaiThu.ngayTao = khoanPhaiThu.ngayTao ? dayjs(khoanPhaiThu.ngayTao) : undefined;
        khoanPhaiThu.ngayPhaiThu = khoanPhaiThu.ngayPhaiThu ? dayjs(khoanPhaiThu.ngayPhaiThu) : undefined;
      });
    }
    return res;
  }
}
