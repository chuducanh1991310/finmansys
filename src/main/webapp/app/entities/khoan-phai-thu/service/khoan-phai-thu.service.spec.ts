import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as dayjs from 'dayjs';

import { DATE_TIME_FORMAT } from 'app/config/input.constants';
import { IKhoanPhaiThu, KhoanPhaiThu } from '../khoan-phai-thu.model';

import { KhoanPhaiThuService } from './khoan-phai-thu.service';

describe('Service Tests', () => {
  describe('KhoanPhaiThu Service', () => {
    let service: KhoanPhaiThuService;
    let httpMock: HttpTestingController;
    let elemDefault: IKhoanPhaiThu;
    let expectedResult: IKhoanPhaiThu | IKhoanPhaiThu[] | boolean | null;
    let currentDate: dayjs.Dayjs;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      service = TestBed.inject(KhoanPhaiThuService);
      httpMock = TestBed.inject(HttpTestingController);
      currentDate = dayjs();

      elemDefault = {
        id: 0,
        soTien: 0,
        ngayTao: currentDate,
        ngayPhaiThu: currentDate,
        ghiChu: 'AAAAAAA',
      };
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            ngayTao: currentDate.format(DATE_TIME_FORMAT),
            ngayPhaiThu: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a KhoanPhaiThu', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            ngayTao: currentDate.format(DATE_TIME_FORMAT),
            ngayPhaiThu: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            ngayTao: currentDate,
            ngayPhaiThu: currentDate,
          },
          returnedFromService
        );

        service.create(new KhoanPhaiThu()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a KhoanPhaiThu', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            soTien: 1,
            ngayTao: currentDate.format(DATE_TIME_FORMAT),
            ngayPhaiThu: currentDate.format(DATE_TIME_FORMAT),
            ghiChu: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            ngayTao: currentDate,
            ngayPhaiThu: currentDate,
          },
          returnedFromService
        );

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should partial update a KhoanPhaiThu', () => {
        const patchObject = Object.assign(
          {
            soTien: 1,
            ngayTao: currentDate.format(DATE_TIME_FORMAT),
          },
          new KhoanPhaiThu()
        );

        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign(
          {
            ngayTao: currentDate,
            ngayPhaiThu: currentDate,
          },
          returnedFromService
        );

        service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PATCH' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of KhoanPhaiThu', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            soTien: 1,
            ngayTao: currentDate.format(DATE_TIME_FORMAT),
            ngayPhaiThu: currentDate.format(DATE_TIME_FORMAT),
            ghiChu: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            ngayTao: currentDate,
            ngayPhaiThu: currentDate,
          },
          returnedFromService
        );

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a KhoanPhaiThu', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });

      describe('addKhoanPhaiThuToCollectionIfMissing', () => {
        it('should add a KhoanPhaiThu to an empty array', () => {
          const khoanPhaiThu: IKhoanPhaiThu = { id: 123 };
          expectedResult = service.addKhoanPhaiThuToCollectionIfMissing([], khoanPhaiThu);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(khoanPhaiThu);
        });

        it('should not add a KhoanPhaiThu to an array that contains it', () => {
          const khoanPhaiThu: IKhoanPhaiThu = { id: 123 };
          const khoanPhaiThuCollection: IKhoanPhaiThu[] = [
            {
              ...khoanPhaiThu,
            },
            { id: 456 },
          ];
          expectedResult = service.addKhoanPhaiThuToCollectionIfMissing(khoanPhaiThuCollection, khoanPhaiThu);
          expect(expectedResult).toHaveLength(2);
        });

        it("should add a KhoanPhaiThu to an array that doesn't contain it", () => {
          const khoanPhaiThu: IKhoanPhaiThu = { id: 123 };
          const khoanPhaiThuCollection: IKhoanPhaiThu[] = [{ id: 456 }];
          expectedResult = service.addKhoanPhaiThuToCollectionIfMissing(khoanPhaiThuCollection, khoanPhaiThu);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(khoanPhaiThu);
        });

        it('should add only unique KhoanPhaiThu to an array', () => {
          const khoanPhaiThuArray: IKhoanPhaiThu[] = [{ id: 123 }, { id: 456 }, { id: 27844 }];
          const khoanPhaiThuCollection: IKhoanPhaiThu[] = [{ id: 123 }];
          expectedResult = service.addKhoanPhaiThuToCollectionIfMissing(khoanPhaiThuCollection, ...khoanPhaiThuArray);
          expect(expectedResult).toHaveLength(3);
        });

        it('should accept varargs', () => {
          const khoanPhaiThu: IKhoanPhaiThu = { id: 123 };
          const khoanPhaiThu2: IKhoanPhaiThu = { id: 456 };
          expectedResult = service.addKhoanPhaiThuToCollectionIfMissing([], khoanPhaiThu, khoanPhaiThu2);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(khoanPhaiThu);
          expect(expectedResult).toContain(khoanPhaiThu2);
        });

        it('should accept null and undefined values', () => {
          const khoanPhaiThu: IKhoanPhaiThu = { id: 123 };
          expectedResult = service.addKhoanPhaiThuToCollectionIfMissing([], null, khoanPhaiThu, undefined);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(khoanPhaiThu);
        });
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
