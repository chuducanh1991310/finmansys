import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IKhachSan } from '../khach-san.model';
import { KhachSanService } from '../service/khach-san.service';

@Component({
  templateUrl: './khach-san-delete-dialog.component.html',
})
export class KhachSanDeleteDialogComponent {
  khachSan?: IKhachSan;

  constructor(protected khachSanService: KhachSanService, public activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.khachSanService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
