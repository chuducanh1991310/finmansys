import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { IKhachSan, KhachSan } from '../khach-san.model';
import { KhachSanService } from '../service/khach-san.service';

@Component({
  selector: 'jhi-khach-san-update',
  templateUrl: './khach-san-update.component.html',
})
export class KhachSanUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    ten: [null, [Validators.required, Validators.maxLength(20)]],
    diaChi: [],
    email: [],
  });

  constructor(protected khachSanService: KhachSanService, protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ khachSan }) => {
      this.updateForm(khachSan);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const khachSan = this.createFromForm();
    if (khachSan.id !== undefined) {
      this.subscribeToSaveResponse(this.khachSanService.update(khachSan));
    } else {
      this.subscribeToSaveResponse(this.khachSanService.create(khachSan));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IKhachSan>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(khachSan: IKhachSan): void {
    this.editForm.patchValue({
      id: khachSan.id,
      ten: khachSan.ten,
      diaChi: khachSan.diaChi,
      email: khachSan.email,
    });
  }

  protected createFromForm(): IKhachSan {
    return {
      ...new KhachSan(),
      id: this.editForm.get(['id'])!.value,
      ten: this.editForm.get(['ten'])!.value,
      diaChi: this.editForm.get(['diaChi'])!.value,
      email: this.editForm.get(['email'])!.value,
    };
  }
}
