jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { KhachHangService } from '../service/khach-hang.service';
import { IKhachHang, KhachHang } from '../khach-hang.model';

import { KhachHangUpdateComponent } from './khach-hang-update.component';

describe('Component Tests', () => {
  describe('KhachHang Management Update Component', () => {
    let comp: KhachHangUpdateComponent;
    let fixture: ComponentFixture<KhachHangUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let khachHangService: KhachHangService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [KhachHangUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(KhachHangUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(KhachHangUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      khachHangService = TestBed.inject(KhachHangService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should update editForm', () => {
        const khachHang: IKhachHang = { id: 456 };

        activatedRoute.data = of({ khachHang });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(khachHang));
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const khachHang = { id: 123 };
        spyOn(khachHangService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ khachHang });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: khachHang }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(khachHangService.update).toHaveBeenCalledWith(khachHang);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const khachHang = new KhachHang();
        spyOn(khachHangService, 'create').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ khachHang });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: khachHang }));
        saveSubject.complete();

        // THEN
        expect(khachHangService.create).toHaveBeenCalledWith(khachHang);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject();
        const khachHang = { id: 123 };
        spyOn(khachHangService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ khachHang });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(khachHangService.update).toHaveBeenCalledWith(khachHang);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });
  });
});
