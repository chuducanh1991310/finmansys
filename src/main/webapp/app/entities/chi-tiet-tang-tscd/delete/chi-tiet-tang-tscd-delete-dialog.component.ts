import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IChiTietTangTSCD } from '../chi-tiet-tang-tscd.model';
import { ChiTietTangTSCDService } from '../service/chi-tiet-tang-tscd.service';

@Component({
  templateUrl: './chi-tiet-tang-tscd-delete-dialog.component.html',
})
export class ChiTietTangTSCDDeleteDialogComponent {
  chiTietTangTSCD?: IChiTietTangTSCD;

  constructor(protected chiTietTangTSCDService: ChiTietTangTSCDService, public activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.chiTietTangTSCDService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
