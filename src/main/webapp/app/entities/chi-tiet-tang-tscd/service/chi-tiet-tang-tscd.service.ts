import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as dayjs from 'dayjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IChiTietTangTSCD, getChiTietTangTSCDIdentifier } from '../chi-tiet-tang-tscd.model';

export type EntityResponseType = HttpResponse<IChiTietTangTSCD>;
export type EntityArrayResponseType = HttpResponse<IChiTietTangTSCD[]>;

@Injectable({ providedIn: 'root' })
export class ChiTietTangTSCDService {
  public resourceUrl = this.applicationConfigService.getEndpointFor('api/chi-tiet-tang-tscds');

  constructor(protected http: HttpClient, private applicationConfigService: ApplicationConfigService) {}

  create(chiTietTangTSCD: IChiTietTangTSCD): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(chiTietTangTSCD);
    return this.http
      .post<IChiTietTangTSCD>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(chiTietTangTSCD: IChiTietTangTSCD): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(chiTietTangTSCD);
    return this.http
      .put<IChiTietTangTSCD>(`${this.resourceUrl}/${getChiTietTangTSCDIdentifier(chiTietTangTSCD) as number}`, copy, {
        observe: 'response',
      })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(chiTietTangTSCD: IChiTietTangTSCD): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(chiTietTangTSCD);
    return this.http
      .patch<IChiTietTangTSCD>(`${this.resourceUrl}/${getChiTietTangTSCDIdentifier(chiTietTangTSCD) as number}`, copy, {
        observe: 'response',
      })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IChiTietTangTSCD>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IChiTietTangTSCD[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addChiTietTangTSCDToCollectionIfMissing(
    chiTietTangTSCDCollection: IChiTietTangTSCD[],
    ...chiTietTangTSCDSToCheck: (IChiTietTangTSCD | null | undefined)[]
  ): IChiTietTangTSCD[] {
    const chiTietTangTSCDS: IChiTietTangTSCD[] = chiTietTangTSCDSToCheck.filter(isPresent);
    if (chiTietTangTSCDS.length > 0) {
      const chiTietTangTSCDCollectionIdentifiers = chiTietTangTSCDCollection.map(
        chiTietTangTSCDItem => getChiTietTangTSCDIdentifier(chiTietTangTSCDItem)!
      );
      const chiTietTangTSCDSToAdd = chiTietTangTSCDS.filter(chiTietTangTSCDItem => {
        const chiTietTangTSCDIdentifier = getChiTietTangTSCDIdentifier(chiTietTangTSCDItem);
        if (chiTietTangTSCDIdentifier == null || chiTietTangTSCDCollectionIdentifiers.includes(chiTietTangTSCDIdentifier)) {
          return false;
        }
        chiTietTangTSCDCollectionIdentifiers.push(chiTietTangTSCDIdentifier);
        return true;
      });
      return [...chiTietTangTSCDSToAdd, ...chiTietTangTSCDCollection];
    }
    return chiTietTangTSCDCollection;
  }

  protected convertDateFromClient(chiTietTangTSCD: IChiTietTangTSCD): IChiTietTangTSCD {
    return Object.assign({}, chiTietTangTSCD, {
      ngayThuMua: chiTietTangTSCD.ngayThuMua?.isValid() ? chiTietTangTSCD.ngayThuMua.toJSON() : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.ngayThuMua = res.body.ngayThuMua ? dayjs(res.body.ngayThuMua) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((chiTietTangTSCD: IChiTietTangTSCD) => {
        chiTietTangTSCD.ngayThuMua = chiTietTangTSCD.ngayThuMua ? dayjs(chiTietTangTSCD.ngayThuMua) : undefined;
      });
    }
    return res;
  }
}
