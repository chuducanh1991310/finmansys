import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IChiTietTangTSCD, ChiTietTangTSCD } from '../chi-tiet-tang-tscd.model';
import { ChiTietTangTSCDService } from '../service/chi-tiet-tang-tscd.service';

@Injectable({ providedIn: 'root' })
export class ChiTietTangTSCDRoutingResolveService implements Resolve<IChiTietTangTSCD> {
  constructor(protected service: ChiTietTangTSCDService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IChiTietTangTSCD> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((chiTietTangTSCD: HttpResponse<ChiTietTangTSCD>) => {
          if (chiTietTangTSCD.body) {
            return of(chiTietTangTSCD.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new ChiTietTangTSCD());
  }
}
