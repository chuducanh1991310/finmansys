import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IChiTietTangTSCD } from '../chi-tiet-tang-tscd.model';

@Component({
  selector: 'jhi-chi-tiet-tang-tscd-detail',
  templateUrl: './chi-tiet-tang-tscd-detail.component.html',
})
export class ChiTietTangTSCDDetailComponent implements OnInit {
  chiTietTangTSCD: IChiTietTangTSCD | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ chiTietTangTSCD }) => {
      this.chiTietTangTSCD = chiTietTangTSCD;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
