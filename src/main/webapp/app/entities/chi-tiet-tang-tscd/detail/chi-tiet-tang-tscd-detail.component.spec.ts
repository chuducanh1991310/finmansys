import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ChiTietTangTSCDDetailComponent } from './chi-tiet-tang-tscd-detail.component';

describe('Component Tests', () => {
  describe('ChiTietTangTSCD Management Detail Component', () => {
    let comp: ChiTietTangTSCDDetailComponent;
    let fixture: ComponentFixture<ChiTietTangTSCDDetailComponent>;

    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [ChiTietTangTSCDDetailComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: { data: of({ chiTietTangTSCD: { id: 123 } }) },
          },
        ],
      })
        .overrideTemplate(ChiTietTangTSCDDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ChiTietTangTSCDDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load chiTietTangTSCD on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.chiTietTangTSCD).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
