import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';
import { ChiTietTangTSCDComponent } from './list/chi-tiet-tang-tscd.component';
import { ChiTietTangTSCDDetailComponent } from './detail/chi-tiet-tang-tscd-detail.component';
import { ChiTietTangTSCDUpdateComponent } from './update/chi-tiet-tang-tscd-update.component';
import { ChiTietTangTSCDDeleteDialogComponent } from './delete/chi-tiet-tang-tscd-delete-dialog.component';
import { ChiTietTangTSCDRoutingModule } from './route/chi-tiet-tang-tscd-routing.module';

@NgModule({
  imports: [SharedModule, ChiTietTangTSCDRoutingModule],
  declarations: [
    ChiTietTangTSCDComponent,
    ChiTietTangTSCDDetailComponent,
    ChiTietTangTSCDUpdateComponent,
    ChiTietTangTSCDDeleteDialogComponent,
  ],
  entryComponents: [ChiTietTangTSCDDeleteDialogComponent],
})
export class ChiTietTangTSCDModule {}
