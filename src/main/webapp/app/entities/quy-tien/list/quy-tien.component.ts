import { Component, OnInit } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { combineLatest } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IQuyTien } from '../quy-tien.model';

import { ITEMS_PER_PAGE } from 'app/config/pagination.constants';
import { QuyTienService } from '../service/quy-tien.service';
import { QuyTienDeleteDialogComponent } from '../delete/quy-tien-delete-dialog.component';

@Component({
  selector: 'jhi-quy-tien',
  templateUrl: './quy-tien.component.html',
})
export class QuyTienComponent implements OnInit {
  quyTiens?: IQuyTien[];
  isLoading = false;
  totalItems = 0;
  itemsPerPage = ITEMS_PER_PAGE;
  page?: number;
  predicate!: string;
  ascending!: boolean;
  ngbPaginationPage = 1;

  constructor(
    protected quyTienService: QuyTienService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    protected modalService: NgbModal
  ) {}

  loadPage(page?: number, dontNavigate?: boolean): void {
    this.isLoading = true;
    const pageToLoad: number = page ?? this.page ?? 1;

    this.quyTienService
      .query({
        page: pageToLoad - 1,
        size: this.itemsPerPage,
        sort: this.sort(),
        'ten.contains': 'A',
      })
      .subscribe(
        (res: HttpResponse<IQuyTien[]>) => {
          this.isLoading = false;
          this.onSuccess(res.body, res.headers, pageToLoad, !dontNavigate);
        },
        () => {
          this.isLoading = false;
          this.onError();
        }
      );
  }

  getSoTienHienTai(quyTien: IQuyTien): number {
    let soTienThu = 0;
    let soTienChi = 0;
    if (quyTien.phieuThus) {
      for (const phieuThu of quyTien.phieuThus) {
        if (phieuThu.soTien) {
          soTienThu = soTienThu + phieuThu.soTien;
        }
      }
    }
    if (quyTien.phieuChis) {
      for (const phieuChi of quyTien.phieuChis) {
        if (phieuChi.soTien) {
          soTienChi = soTienChi + phieuChi.soTien;
        }
      }
    }
    if (quyTien.soTien) {
      return quyTien.soTien + soTienThu - soTienChi;
    } else {
      return 0;
    }
  }

  ngOnInit(): void {
    this.handleNavigation();
  }

  trackId(index: number, item: IQuyTien): number {
    return item.id!;
  }

  delete(quyTien: IQuyTien): void {
    const modalRef = this.modalService.open(QuyTienDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.quyTien = quyTien;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadPage();
      }
    });
  }

  protected sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected handleNavigation(): void {
    combineLatest([this.activatedRoute.data, this.activatedRoute.queryParamMap]).subscribe(([data, params]) => {
      const page = params.get('page');
      const pageNumber = page !== null ? +page : 1;
      const sort = (params.get('sort') ?? data['defaultSort']).split(',');
      const predicate = sort[0];
      const ascending = sort[1] === 'asc';
      if (pageNumber !== this.page || predicate !== this.predicate || ascending !== this.ascending) {
        this.predicate = predicate;
        this.ascending = ascending;
        this.loadPage(pageNumber, true);
      }
    });
  }

  protected onSuccess(data: IQuyTien[] | null, headers: HttpHeaders, page: number, navigate: boolean): void {
    this.totalItems = Number(headers.get('X-Total-Count'));
    this.page = page;
    if (navigate) {
      this.router.navigate(['/quy-tien'], {
        queryParams: {
          page: this.page,
          size: this.itemsPerPage,
          sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc'),
        },
      });
    }
    this.quyTiens = data ?? [];
    this.ngbPaginationPage = this.page;
  }

  protected onError(): void {
    this.ngbPaginationPage = this.page ?? 1;
  }
}
