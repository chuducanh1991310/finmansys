import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { QuyTienDetailComponent } from './quy-tien-detail.component';

describe('Component Tests', () => {
  describe('QuyTien Management Detail Component', () => {
    let comp: QuyTienDetailComponent;
    let fixture: ComponentFixture<QuyTienDetailComponent>;

    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [QuyTienDetailComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: { data: of({ quyTien: { id: 123 } }) },
          },
        ],
      })
        .overrideTemplate(QuyTienDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(QuyTienDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load quyTien on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.quyTien).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
