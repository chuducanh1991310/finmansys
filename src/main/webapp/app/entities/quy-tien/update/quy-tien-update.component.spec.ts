jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { QuyTienService } from '../service/quy-tien.service';
import { IQuyTien, QuyTien } from '../quy-tien.model';
import { ILoaiQuy } from 'app/entities/loai-quy/loai-quy.model';
import { LoaiQuyService } from 'app/entities/loai-quy/service/loai-quy.service';
import { IMaKeToan } from 'app/entities/ma-ke-toan/ma-ke-toan.model';
import { MaKeToanService } from 'app/entities/ma-ke-toan/service/ma-ke-toan.service';
import { IKhachSan } from 'app/entities/khach-san/khach-san.model';
import { KhachSanService } from 'app/entities/khach-san/service/khach-san.service';

import { QuyTienUpdateComponent } from './quy-tien-update.component';

describe('Component Tests', () => {
  describe('QuyTien Management Update Component', () => {
    let comp: QuyTienUpdateComponent;
    let fixture: ComponentFixture<QuyTienUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let quyTienService: QuyTienService;
    let loaiQuyService: LoaiQuyService;
    let maKeToanService: MaKeToanService;
    let khachSanService: KhachSanService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [QuyTienUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(QuyTienUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(QuyTienUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      quyTienService = TestBed.inject(QuyTienService);
      loaiQuyService = TestBed.inject(LoaiQuyService);
      maKeToanService = TestBed.inject(MaKeToanService);
      khachSanService = TestBed.inject(KhachSanService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should call LoaiQuy query and add missing value', () => {
        const quyTien: IQuyTien = { id: 456 };
        const loaiQuy: ILoaiQuy = { id: 93200 };
        quyTien.loaiQuy = loaiQuy;

        const loaiQuyCollection: ILoaiQuy[] = [{ id: 67608 }];
        spyOn(loaiQuyService, 'query').and.returnValue(of(new HttpResponse({ body: loaiQuyCollection })));
        const additionalLoaiQuies = [loaiQuy];
        const expectedCollection: ILoaiQuy[] = [...additionalLoaiQuies, ...loaiQuyCollection];
        spyOn(loaiQuyService, 'addLoaiQuyToCollectionIfMissing').and.returnValue(expectedCollection);

        activatedRoute.data = of({ quyTien });
        comp.ngOnInit();

        expect(loaiQuyService.query).toHaveBeenCalled();
        expect(loaiQuyService.addLoaiQuyToCollectionIfMissing).toHaveBeenCalledWith(loaiQuyCollection, ...additionalLoaiQuies);
        expect(comp.loaiQuiesSharedCollection).toEqual(expectedCollection);
      });

      it('Should call MaKeToan query and add missing value', () => {
        const quyTien: IQuyTien = { id: 456 };
        const maKeToan: IMaKeToan = { id: 53263 };
        quyTien.maKeToan = maKeToan;

        const maKeToanCollection: IMaKeToan[] = [{ id: 10718 }];
        spyOn(maKeToanService, 'query').and.returnValue(of(new HttpResponse({ body: maKeToanCollection })));
        const additionalMaKeToans = [maKeToan];
        const expectedCollection: IMaKeToan[] = [...additionalMaKeToans, ...maKeToanCollection];
        spyOn(maKeToanService, 'addMaKeToanToCollectionIfMissing').and.returnValue(expectedCollection);

        activatedRoute.data = of({ quyTien });
        comp.ngOnInit();

        expect(maKeToanService.query).toHaveBeenCalled();
        expect(maKeToanService.addMaKeToanToCollectionIfMissing).toHaveBeenCalledWith(maKeToanCollection, ...additionalMaKeToans);
        expect(comp.maKeToansSharedCollection).toEqual(expectedCollection);
      });

      it('Should call KhachSan query and add missing value', () => {
        const quyTien: IQuyTien = { id: 456 };
        const khachSan: IKhachSan = { id: 33491 };
        quyTien.khachSan = khachSan;

        const khachSanCollection: IKhachSan[] = [{ id: 59911 }];
        spyOn(khachSanService, 'query').and.returnValue(of(new HttpResponse({ body: khachSanCollection })));
        const additionalKhachSans = [khachSan];
        const expectedCollection: IKhachSan[] = [...additionalKhachSans, ...khachSanCollection];
        spyOn(khachSanService, 'addKhachSanToCollectionIfMissing').and.returnValue(expectedCollection);

        activatedRoute.data = of({ quyTien });
        comp.ngOnInit();

        expect(khachSanService.query).toHaveBeenCalled();
        expect(khachSanService.addKhachSanToCollectionIfMissing).toHaveBeenCalledWith(khachSanCollection, ...additionalKhachSans);
        expect(comp.khachSansSharedCollection).toEqual(expectedCollection);
      });

      it('Should update editForm', () => {
        const quyTien: IQuyTien = { id: 456 };
        const loaiQuy: ILoaiQuy = { id: 83808 };
        quyTien.loaiQuy = loaiQuy;
        const maKeToan: IMaKeToan = { id: 25204 };
        quyTien.maKeToan = maKeToan;
        const khachSan: IKhachSan = { id: 20098 };
        quyTien.khachSan = khachSan;

        activatedRoute.data = of({ quyTien });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(quyTien));
        expect(comp.loaiQuiesSharedCollection).toContain(loaiQuy);
        expect(comp.maKeToansSharedCollection).toContain(maKeToan);
        expect(comp.khachSansSharedCollection).toContain(khachSan);
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const quyTien = { id: 123 };
        spyOn(quyTienService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ quyTien });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: quyTien }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(quyTienService.update).toHaveBeenCalledWith(quyTien);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const quyTien = new QuyTien();
        spyOn(quyTienService, 'create').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ quyTien });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: quyTien }));
        saveSubject.complete();

        // THEN
        expect(quyTienService.create).toHaveBeenCalledWith(quyTien);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject();
        const quyTien = { id: 123 };
        spyOn(quyTienService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ quyTien });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(quyTienService.update).toHaveBeenCalledWith(quyTien);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });

    describe('Tracking relationships identifiers', () => {
      describe('trackLoaiQuyById', () => {
        it('Should return tracked LoaiQuy primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackLoaiQuyById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });

      describe('trackMaKeToanById', () => {
        it('Should return tracked MaKeToan primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackMaKeToanById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });

      describe('trackKhachSanById', () => {
        it('Should return tracked KhachSan primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackKhachSanById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });
    });
  });
});
