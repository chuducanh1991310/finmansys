import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IKhoanMucChiPhi } from '../khoan-muc-chi-phi.model';
import { KhoanMucChiPhiService } from '../service/khoan-muc-chi-phi.service';

@Component({
  templateUrl: './khoan-muc-chi-phi-delete-dialog.component.html',
})
export class KhoanMucChiPhiDeleteDialogComponent {
  khoanMucChiPhi?: IKhoanMucChiPhi;

  constructor(protected khoanMucChiPhiService: KhoanMucChiPhiService, public activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.khoanMucChiPhiService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
