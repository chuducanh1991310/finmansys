import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';
import { MaKeToanComponent } from './list/ma-ke-toan.component';
import { MaKeToanDetailComponent } from './detail/ma-ke-toan-detail.component';
import { MaKeToanUpdateComponent } from './update/ma-ke-toan-update.component';
import { MaKeToanDeleteDialogComponent } from './delete/ma-ke-toan-delete-dialog.component';
import { MaKeToanRoutingModule } from './route/ma-ke-toan-routing.module';

@NgModule({
  imports: [SharedModule, MaKeToanRoutingModule],
  declarations: [MaKeToanComponent, MaKeToanDetailComponent, MaKeToanUpdateComponent, MaKeToanDeleteDialogComponent],
  entryComponents: [MaKeToanDeleteDialogComponent],
})
export class MaKeToanModule {}
