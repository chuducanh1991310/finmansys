import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IMaKeToan, getMaKeToanIdentifier } from '../ma-ke-toan.model';

export type EntityResponseType = HttpResponse<IMaKeToan>;
export type EntityArrayResponseType = HttpResponse<IMaKeToan[]>;

@Injectable({ providedIn: 'root' })
export class MaKeToanService {
  public resourceUrl = this.applicationConfigService.getEndpointFor('api/ma-ke-toans');

  constructor(protected http: HttpClient, private applicationConfigService: ApplicationConfigService) {}

  create(maKeToan: IMaKeToan): Observable<EntityResponseType> {
    return this.http.post<IMaKeToan>(this.resourceUrl, maKeToan, { observe: 'response' });
  }

  update(maKeToan: IMaKeToan): Observable<EntityResponseType> {
    return this.http.put<IMaKeToan>(`${this.resourceUrl}/${getMaKeToanIdentifier(maKeToan) as number}`, maKeToan, { observe: 'response' });
  }

  partialUpdate(maKeToan: IMaKeToan): Observable<EntityResponseType> {
    return this.http.patch<IMaKeToan>(`${this.resourceUrl}/${getMaKeToanIdentifier(maKeToan) as number}`, maKeToan, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IMaKeToan>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IMaKeToan[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addMaKeToanToCollectionIfMissing(maKeToanCollection: IMaKeToan[], ...maKeToansToCheck: (IMaKeToan | null | undefined)[]): IMaKeToan[] {
    const maKeToans: IMaKeToan[] = maKeToansToCheck.filter(isPresent);
    if (maKeToans.length > 0) {
      const maKeToanCollectionIdentifiers = maKeToanCollection.map(maKeToanItem => getMaKeToanIdentifier(maKeToanItem)!);
      const maKeToansToAdd = maKeToans.filter(maKeToanItem => {
        const maKeToanIdentifier = getMaKeToanIdentifier(maKeToanItem);
        if (maKeToanIdentifier == null || maKeToanCollectionIdentifiers.includes(maKeToanIdentifier)) {
          return false;
        }
        maKeToanCollectionIdentifiers.push(maKeToanIdentifier);
        return true;
      });
      return [...maKeToansToAdd, ...maKeToanCollection];
    }
    return maKeToanCollection;
  }
}
