import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IChiTietGiamTSCD } from '../chi-tiet-giam-tscd.model';
import { ChiTietGiamTSCDService } from '../service/chi-tiet-giam-tscd.service';

@Component({
  templateUrl: './chi-tiet-giam-tscd-delete-dialog.component.html',
})
export class ChiTietGiamTSCDDeleteDialogComponent {
  chiTietGiamTSCD?: IChiTietGiamTSCD;

  constructor(protected chiTietGiamTSCDService: ChiTietGiamTSCDService, public activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.chiTietGiamTSCDService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
