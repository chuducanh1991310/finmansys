import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ChiTietGiamTSCDComponent } from '../list/chi-tiet-giam-tscd.component';
import { ChiTietGiamTSCDDetailComponent } from '../detail/chi-tiet-giam-tscd-detail.component';
import { ChiTietGiamTSCDUpdateComponent } from '../update/chi-tiet-giam-tscd-update.component';
import { ChiTietGiamTSCDRoutingResolveService } from './chi-tiet-giam-tscd-routing-resolve.service';

const chiTietGiamTSCDRoute: Routes = [
  {
    path: '',
    component: ChiTietGiamTSCDComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: ChiTietGiamTSCDDetailComponent,
    resolve: {
      chiTietGiamTSCD: ChiTietGiamTSCDRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ChiTietGiamTSCDUpdateComponent,
    resolve: {
      chiTietGiamTSCD: ChiTietGiamTSCDRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ChiTietGiamTSCDUpdateComponent,
    resolve: {
      chiTietGiamTSCD: ChiTietGiamTSCDRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(chiTietGiamTSCDRoute)],
  exports: [RouterModule],
})
export class ChiTietGiamTSCDRoutingModule {}
