import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import * as dayjs from 'dayjs';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';

import { IChiTietGiamTSCD, ChiTietGiamTSCD } from '../chi-tiet-giam-tscd.model';
import { ChiTietGiamTSCDService } from '../service/chi-tiet-giam-tscd.service';
import { ITaiSanCD } from 'app/entities/tai-san-cd/tai-san-cd.model';
import { TaiSanCDService } from 'app/entities/tai-san-cd/service/tai-san-cd.service';

@Component({
  selector: 'jhi-chi-tiet-giam-tscd-update',
  templateUrl: './chi-tiet-giam-tscd-update.component.html',
})
export class ChiTietGiamTSCDUpdateComponent implements OnInit {
  isSaving = false;

  taiSanCDSSharedCollection: ITaiSanCD[] = [];

  editForm = this.fb.group({
    id: [],
    ngayMua: [null, [Validators.required]],
    soLuong: [null, [Validators.required, Validators.min(1)]],
    khauHao: [null, [Validators.min(0)]],
    soTienThanhLy: [null, [Validators.required, Validators.min(1)]],
    chiPhiMua: [null, [Validators.required, Validators.min(1)]],
    taiSanCD: [null, Validators.required],
  });

  constructor(
    protected chiTietGiamTSCDService: ChiTietGiamTSCDService,
    protected taiSanCDService: TaiSanCDService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ chiTietGiamTSCD }) => {
      if (chiTietGiamTSCD.id === undefined) {
        const today = dayjs().startOf('day');
        chiTietGiamTSCD.ngayMua = today;
      }

      this.updateForm(chiTietGiamTSCD);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const chiTietGiamTSCD = this.createFromForm();
    if (chiTietGiamTSCD.id !== undefined) {
      this.subscribeToSaveResponse(this.chiTietGiamTSCDService.update(chiTietGiamTSCD));
    } else {
      this.subscribeToSaveResponse(this.chiTietGiamTSCDService.create(chiTietGiamTSCD));
    }
  }

  trackTaiSanCDById(index: number, item: ITaiSanCD): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IChiTietGiamTSCD>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(chiTietGiamTSCD: IChiTietGiamTSCD): void {
    this.editForm.patchValue({
      id: chiTietGiamTSCD.id,
      ngayMua: chiTietGiamTSCD.ngayMua ? chiTietGiamTSCD.ngayMua.format(DATE_TIME_FORMAT) : null,
      soLuong: chiTietGiamTSCD.soLuong,
      khauHao: chiTietGiamTSCD.khauHao,
      soTienThanhLy: chiTietGiamTSCD.soTienThanhLy,
      chiPhiMua: chiTietGiamTSCD.chiPhiMua,
      taiSanCD: chiTietGiamTSCD.taiSanCD,
    });

    this.taiSanCDSSharedCollection = this.taiSanCDService.addTaiSanCDToCollectionIfMissing(
      this.taiSanCDSSharedCollection,
      chiTietGiamTSCD.taiSanCD
    );
  }

  protected loadRelationshipsOptions(): void {
    this.taiSanCDService
      .query()
      .pipe(map((res: HttpResponse<ITaiSanCD[]>) => res.body ?? []))
      .pipe(
        map((taiSanCDS: ITaiSanCD[]) =>
          this.taiSanCDService.addTaiSanCDToCollectionIfMissing(taiSanCDS, this.editForm.get('taiSanCD')!.value)
        )
      )
      .subscribe((taiSanCDS: ITaiSanCD[]) => (this.taiSanCDSSharedCollection = taiSanCDS));
  }

  protected createFromForm(): IChiTietGiamTSCD {
    return {
      ...new ChiTietGiamTSCD(),
      id: this.editForm.get(['id'])!.value,
      ngayMua: this.editForm.get(['ngayMua'])!.value ? dayjs(this.editForm.get(['ngayMua'])!.value, DATE_TIME_FORMAT) : undefined,
      soLuong: this.editForm.get(['soLuong'])!.value,
      khauHao: this.editForm.get(['khauHao'])!.value,
      soTienThanhLy: this.editForm.get(['soTienThanhLy'])!.value,
      chiPhiMua: this.editForm.get(['chiPhiMua'])!.value,
      taiSanCD: this.editForm.get(['taiSanCD'])!.value,
    };
  }
}
