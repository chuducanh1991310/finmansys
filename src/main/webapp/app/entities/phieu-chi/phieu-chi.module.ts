import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';
import { PhieuChiComponent } from './list/phieu-chi.component';
import { PhieuChiDetailComponent } from './detail/phieu-chi-detail.component';
import { PhieuChiUpdateComponent } from './update/phieu-chi-update.component';
import { PhieuChiDeleteDialogComponent } from './delete/phieu-chi-delete-dialog.component';
import { PhieuChiRoutingModule } from './route/phieu-chi-routing.module';

@NgModule({
  imports: [SharedModule, PhieuChiRoutingModule],
  declarations: [PhieuChiComponent, PhieuChiDetailComponent, PhieuChiUpdateComponent, PhieuChiDeleteDialogComponent],
  entryComponents: [PhieuChiDeleteDialogComponent],
})
export class PhieuChiModule {}
