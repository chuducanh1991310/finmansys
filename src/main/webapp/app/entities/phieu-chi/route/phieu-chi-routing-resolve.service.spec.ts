jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { IPhieuChi, PhieuChi } from '../phieu-chi.model';
import { PhieuChiService } from '../service/phieu-chi.service';

import { PhieuChiRoutingResolveService } from './phieu-chi-routing-resolve.service';

describe('Service Tests', () => {
  describe('PhieuChi routing resolve service', () => {
    let mockRouter: Router;
    let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
    let routingResolveService: PhieuChiRoutingResolveService;
    let service: PhieuChiService;
    let resultPhieuChi: IPhieuChi | undefined;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        providers: [Router, ActivatedRouteSnapshot],
      });
      mockRouter = TestBed.inject(Router);
      mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
      routingResolveService = TestBed.inject(PhieuChiRoutingResolveService);
      service = TestBed.inject(PhieuChiService);
      resultPhieuChi = undefined;
    });

    describe('resolve', () => {
      it('should return IPhieuChi returned by find', () => {
        // GIVEN
        service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultPhieuChi = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultPhieuChi).toEqual({ id: 123 });
      });

      it('should return new IPhieuChi if id is not provided', () => {
        // GIVEN
        service.find = jest.fn();
        mockActivatedRouteSnapshot.params = {};

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultPhieuChi = result;
        });

        // THEN
        expect(service.find).not.toBeCalled();
        expect(resultPhieuChi).toEqual(new PhieuChi());
      });

      it('should route to 404 page if data not found in server', () => {
        // GIVEN
        spyOn(service, 'find').and.returnValue(of(new HttpResponse({ body: null })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultPhieuChi = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultPhieuChi).toEqual(undefined);
        expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
      });
    });
  });
});
