import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IKhoanPhaiTra } from '../khoan-phai-tra.model';

@Component({
  selector: 'jhi-khoan-phai-tra-detail',
  templateUrl: './khoan-phai-tra-detail.component.html',
})
export class KhoanPhaiTraDetailComponent implements OnInit {
  khoanPhaiTra: IKhoanPhaiTra | null = null;

  soTienDaTra = 0;

  soTienConThieu = 0;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ khoanPhaiTra }) => {
      this.khoanPhaiTra = khoanPhaiTra;
      for (const phieuChi of khoanPhaiTra.dsPhieuChis) {
        this.soTienDaTra = this.soTienDaTra + Number(phieuChi.soTien);
      }
      this.soTienConThieu = khoanPhaiTra.soTien - this.soTienDaTra;
      if (this.soTienConThieu < 0) {
        this.soTienConThieu = 0;
      }
    });
  }

  previousState(): void {
    window.history.back();
  }
}
