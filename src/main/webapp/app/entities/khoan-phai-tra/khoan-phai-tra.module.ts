import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';
import { KhoanPhaiTraComponent } from './list/khoan-phai-tra.component';
import { KhoanPhaiTraDetailComponent } from './detail/khoan-phai-tra-detail.component';
import { KhoanPhaiTraUpdateComponent } from './update/khoan-phai-tra-update.component';
import { KhoanPhaiTraDeleteDialogComponent } from './delete/khoan-phai-tra-delete-dialog.component';
import { KhoanPhaiTraRoutingModule } from './route/khoan-phai-tra-routing.module';

@NgModule({
  imports: [SharedModule, KhoanPhaiTraRoutingModule],
  declarations: [KhoanPhaiTraComponent, KhoanPhaiTraDetailComponent, KhoanPhaiTraUpdateComponent, KhoanPhaiTraDeleteDialogComponent],
  entryComponents: [KhoanPhaiTraDeleteDialogComponent],
})
export class KhoanPhaiTraModule {}
