jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { KhoanPhaiTraService } from '../service/khoan-phai-tra.service';
import { IKhoanPhaiTra, KhoanPhaiTra } from '../khoan-phai-tra.model';
import { IKhachHang } from 'app/entities/khach-hang/khach-hang.model';
import { KhachHangService } from 'app/entities/khach-hang/service/khach-hang.service';
import { IMaKeToan } from 'app/entities/ma-ke-toan/ma-ke-toan.model';
import { MaKeToanService } from 'app/entities/ma-ke-toan/service/ma-ke-toan.service';

import { KhoanPhaiTraUpdateComponent } from './khoan-phai-tra-update.component';

describe('Component Tests', () => {
  describe('KhoanPhaiTra Management Update Component', () => {
    let comp: KhoanPhaiTraUpdateComponent;
    let fixture: ComponentFixture<KhoanPhaiTraUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let khoanPhaiTraService: KhoanPhaiTraService;
    let khachHangService: KhachHangService;
    let maKeToanService: MaKeToanService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [KhoanPhaiTraUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(KhoanPhaiTraUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(KhoanPhaiTraUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      khoanPhaiTraService = TestBed.inject(KhoanPhaiTraService);
      khachHangService = TestBed.inject(KhachHangService);
      maKeToanService = TestBed.inject(MaKeToanService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should call KhachHang query and add missing value', () => {
        const khoanPhaiTra: IKhoanPhaiTra = { id: 456 };
        const khachHang: IKhachHang = { id: 84461 };
        khoanPhaiTra.khachHang = khachHang;

        const khachHangCollection: IKhachHang[] = [{ id: 82421 }];
        spyOn(khachHangService, 'query').and.returnValue(of(new HttpResponse({ body: khachHangCollection })));
        const additionalKhachHangs = [khachHang];
        const expectedCollection: IKhachHang[] = [...additionalKhachHangs, ...khachHangCollection];
        spyOn(khachHangService, 'addKhachHangToCollectionIfMissing').and.returnValue(expectedCollection);

        activatedRoute.data = of({ khoanPhaiTra });
        comp.ngOnInit();

        expect(khachHangService.query).toHaveBeenCalled();
        expect(khachHangService.addKhachHangToCollectionIfMissing).toHaveBeenCalledWith(khachHangCollection, ...additionalKhachHangs);
        expect(comp.khachHangsSharedCollection).toEqual(expectedCollection);
      });

      it('Should call MaKeToan query and add missing value', () => {
        const khoanPhaiTra: IKhoanPhaiTra = { id: 456 };
        const maKeToan: IMaKeToan = { id: 94073 };
        khoanPhaiTra.maKeToan = maKeToan;

        const maKeToanCollection: IMaKeToan[] = [{ id: 24722 }];
        spyOn(maKeToanService, 'query').and.returnValue(of(new HttpResponse({ body: maKeToanCollection })));
        const additionalMaKeToans = [maKeToan];
        const expectedCollection: IMaKeToan[] = [...additionalMaKeToans, ...maKeToanCollection];
        spyOn(maKeToanService, 'addMaKeToanToCollectionIfMissing').and.returnValue(expectedCollection);

        activatedRoute.data = of({ khoanPhaiTra });
        comp.ngOnInit();

        expect(maKeToanService.query).toHaveBeenCalled();
        expect(maKeToanService.addMaKeToanToCollectionIfMissing).toHaveBeenCalledWith(maKeToanCollection, ...additionalMaKeToans);
        expect(comp.maKeToansSharedCollection).toEqual(expectedCollection);
      });

      it('Should update editForm', () => {
        const khoanPhaiTra: IKhoanPhaiTra = { id: 456 };
        const khachHang: IKhachHang = { id: 55115 };
        khoanPhaiTra.khachHang = khachHang;
        const maKeToan: IMaKeToan = { id: 66712 };
        khoanPhaiTra.maKeToan = maKeToan;

        activatedRoute.data = of({ khoanPhaiTra });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(khoanPhaiTra));
        expect(comp.khachHangsSharedCollection).toContain(khachHang);
        expect(comp.maKeToansSharedCollection).toContain(maKeToan);
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const khoanPhaiTra = { id: 123 };
        spyOn(khoanPhaiTraService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ khoanPhaiTra });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: khoanPhaiTra }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(khoanPhaiTraService.update).toHaveBeenCalledWith(khoanPhaiTra);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const khoanPhaiTra = new KhoanPhaiTra();
        spyOn(khoanPhaiTraService, 'create').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ khoanPhaiTra });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: khoanPhaiTra }));
        saveSubject.complete();

        // THEN
        expect(khoanPhaiTraService.create).toHaveBeenCalledWith(khoanPhaiTra);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject();
        const khoanPhaiTra = { id: 123 };
        spyOn(khoanPhaiTraService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ khoanPhaiTra });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(khoanPhaiTraService.update).toHaveBeenCalledWith(khoanPhaiTra);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });

    describe('Tracking relationships identifiers', () => {
      describe('trackKhachHangById', () => {
        it('Should return tracked KhachHang primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackKhachHangById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });

      describe('trackMaKeToanById', () => {
        it('Should return tracked MaKeToan primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackMaKeToanById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });
    });
  });
});
