import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IToanTu } from '../toan-tu.model';
import { ToanTuService } from '../service/toan-tu.service';

@Component({
  templateUrl: './toan-tu-delete-dialog.component.html',
})
export class ToanTuDeleteDialogComponent {
  toanTu?: IToanTu;

  constructor(protected toanTuService: ToanTuService, public activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.toanTuService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
