import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ILoaiQuy } from '../loai-quy.model';

@Component({
  selector: 'jhi-loai-quy-detail',
  templateUrl: './loai-quy-detail.component.html',
})
export class LoaiQuyDetailComponent implements OnInit {
  loaiQuy: ILoaiQuy | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ loaiQuy }) => {
      this.loaiQuy = loaiQuy;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
