import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IPhong, Phong } from '../phong.model';
import { PhongService } from '../service/phong.service';

@Injectable({ providedIn: 'root' })
export class PhongRoutingResolveService implements Resolve<IPhong> {
  constructor(protected service: PhongService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IPhong> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((phong: HttpResponse<Phong>) => {
          if (phong.body) {
            return of(phong.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Phong());
  }
}
