import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IPhong } from '../phong.model';

@Component({
  selector: 'jhi-phong-detail',
  templateUrl: './phong-detail.component.html',
})
export class PhongDetailComponent implements OnInit {
  phong: IPhong | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ phong }) => {
      this.phong = phong;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
