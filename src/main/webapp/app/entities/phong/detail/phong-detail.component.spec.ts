import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { PhongDetailComponent } from './phong-detail.component';

describe('Component Tests', () => {
  describe('Phong Management Detail Component', () => {
    let comp: PhongDetailComponent;
    let fixture: ComponentFixture<PhongDetailComponent>;

    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [PhongDetailComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: { data: of({ phong: { id: 123 } }) },
          },
        ],
      })
        .overrideTemplate(PhongDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(PhongDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load phong on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.phong).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
