import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';
import { PhongComponent } from './list/phong.component';
import { PhongDetailComponent } from './detail/phong-detail.component';
import { PhongUpdateComponent } from './update/phong-update.component';
import { PhongDeleteDialogComponent } from './delete/phong-delete-dialog.component';
import { PhongRoutingModule } from './route/phong-routing.module';

@NgModule({
  imports: [SharedModule, PhongRoutingModule],
  declarations: [PhongComponent, PhongDetailComponent, PhongUpdateComponent, PhongDeleteDialogComponent],
  entryComponents: [PhongDeleteDialogComponent],
})
export class PhongModule {}
