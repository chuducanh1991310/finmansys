import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { INganSachNam } from '../ngan-sach-nam.model';
import { NganSachNamService } from '../service/ngan-sach-nam.service';

@Component({
  templateUrl: './ngan-sach-nam-delete-dialog.component.html',
})
export class NganSachNamDeleteDialogComponent {
  nganSachNam?: INganSachNam;

  constructor(protected nganSachNamService: NganSachNamService, public activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.nganSachNamService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
