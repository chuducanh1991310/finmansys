import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HoachToanComponent } from './hoach-toan.component';

describe('HoachToanComponent', () => {
  let component: HoachToanComponent;
  let fixture: ComponentFixture<HoachToanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HoachToanComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HoachToanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
