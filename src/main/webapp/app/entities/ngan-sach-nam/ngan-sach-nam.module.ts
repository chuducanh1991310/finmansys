import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';
import { NganSachNamComponent } from './list/ngan-sach-nam.component';
import { NganSachNamDetailComponent } from './detail/ngan-sach-nam-detail.component';
import { NganSachNamUpdateComponent } from './update/ngan-sach-nam-update.component';
import { NganSachNamDeleteDialogComponent } from './delete/ngan-sach-nam-delete-dialog.component';
import { NganSachNamRoutingModule } from './route/ngan-sach-nam-routing.module';
import { HoachToanComponent } from './hoach-toan/hoach-toan.component';

@NgModule({
  imports: [SharedModule, NganSachNamRoutingModule],
  declarations: [
    NganSachNamComponent,
    NganSachNamDetailComponent,
    NganSachNamUpdateComponent,
    NganSachNamDeleteDialogComponent,
    HoachToanComponent,
  ],
  entryComponents: [NganSachNamDeleteDialogComponent],
})
export class NganSachNamModule {}
