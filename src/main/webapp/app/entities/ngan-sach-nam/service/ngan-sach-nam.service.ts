import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { INganSachNam, getNganSachNamIdentifier } from '../ngan-sach-nam.model';
import { IKhoanMucChiPhi } from 'app/entities/khoan-muc-chi-phi/khoan-muc-chi-phi.model';

export type EntityResponseType = HttpResponse<INganSachNam>;
export type EntityArrayResponseType = HttpResponse<INganSachNam[]>;

@Injectable({ providedIn: 'root' })
export class NganSachNamService {
  public resourceUrl = this.applicationConfigService.getEndpointFor('api/ngan-sach-nams');

  constructor(protected http: HttpClient, private applicationConfigService: ApplicationConfigService) {}

  create(nganSachNam: INganSachNam): Observable<EntityResponseType> {
    return this.http.post<INganSachNam>(this.resourceUrl, nganSachNam, { observe: 'response' });
  }

  update(nganSachNam: INganSachNam): Observable<EntityResponseType> {
    return this.http.put<INganSachNam>(`${this.resourceUrl}/${getNganSachNamIdentifier(nganSachNam) as number}`, nganSachNam, {
      observe: 'response',
    });
  }

  partialUpdate(nganSachNam: INganSachNam): Observable<EntityResponseType> {
    return this.http.patch<INganSachNam>(`${this.resourceUrl}/${getNganSachNamIdentifier(nganSachNam) as number}`, nganSachNam, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<INganSachNam>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<INganSachNam[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  getChiTieu(id: number): Observable<EntityResponseType> {
    return this.http.get<IKhoanMucChiPhi>(`${this.resourceUrl}/chi-tieu/${id}`, { observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addNganSachNamToCollectionIfMissing(
    nganSachNamCollection: INganSachNam[],
    ...nganSachNamsToCheck: (INganSachNam | null | undefined)[]
  ): INganSachNam[] {
    const nganSachNams: INganSachNam[] = nganSachNamsToCheck.filter(isPresent);
    if (nganSachNams.length > 0) {
      const nganSachNamCollectionIdentifiers = nganSachNamCollection.map(nganSachNamItem => getNganSachNamIdentifier(nganSachNamItem)!);
      const nganSachNamsToAdd = nganSachNams.filter(nganSachNamItem => {
        const nganSachNamIdentifier = getNganSachNamIdentifier(nganSachNamItem);
        if (nganSachNamIdentifier == null || nganSachNamCollectionIdentifiers.includes(nganSachNamIdentifier)) {
          return false;
        }
        nganSachNamCollectionIdentifiers.push(nganSachNamIdentifier);
        return true;
      });
      return [...nganSachNamsToAdd, ...nganSachNamCollection];
    }
    return nganSachNamCollection;
  }
}
