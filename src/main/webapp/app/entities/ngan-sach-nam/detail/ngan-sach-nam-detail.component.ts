import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { INganSachNam } from '../ngan-sach-nam.model';
import { HttpResponse } from '@angular/common/http';
import { IKhoanMucChiPhi } from 'app/entities/khoan-muc-chi-phi/khoan-muc-chi-phi.model';
import { INganSachThang } from 'app/entities/ngan-sach-thang/ngan-sach-thang.model';
import { KhoanMucChiPhiService } from 'app/entities/khoan-muc-chi-phi/service/khoan-muc-chi-phi.service';

@Component({
  selector: 'jhi-ngan-sach-nam-detail',
  templateUrl: './ngan-sach-nam-detail.component.html',
})
export class NganSachNamDetailComponent implements OnInit {
  nganSachNam: INganSachNam | null = null;

  listMonth = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];

  khoanMucChiPhis: IKhoanMucChiPhi[] = [];

  nganSachThangs: INganSachThang[] = [];

  constructor(protected activatedRoute: ActivatedRoute, protected khoanMucChiPhiService: KhoanMucChiPhiService) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ nganSachNam }) => {
      this.nganSachNam = nganSachNam;
      this.nganSachThangs = nganSachNam.nganSachThangs;
      this.khoanMucChiPhiService.query().subscribe(
        (res: HttpResponse<IKhoanMucChiPhi[]>) => {
          this.khoanMucChiPhis = res.body ?? [];
        },
        () => {
          this.khoanMucChiPhis = [];
        }
      );
    });
  }

  getMoney(thang: number, khoanMucChiPhi: IKhoanMucChiPhi): number {
    for (const nganSachThang of this.nganSachThangs) {
      if (nganSachThang.thang === thang && nganSachThang.khoanMucChiPhi?.id === khoanMucChiPhi.id) {
        return nganSachThang.soTien ?? 0;
      }
    }
    return 0;
  }

  getTotal(khoanMucChiPhi: IKhoanMucChiPhi): number {
    let total: number;
    total = 0;
    for (const nganSachThang of this.nganSachThangs) {
      if (nganSachThang.khoanMucChiPhi?.id === khoanMucChiPhi.id) {
        total = total + Number(nganSachThang.soTien ?? 0);
      }
    }
    return total;
  }

  previousState(): void {
    window.history.back();
  }
}
