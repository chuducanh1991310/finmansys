import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ILoaiTaiSan } from '../loai-tai-san.model';

@Component({
  selector: 'jhi-loai-tai-san-detail',
  templateUrl: './loai-tai-san-detail.component.html',
})
export class LoaiTaiSanDetailComponent implements OnInit {
  loaiTaiSan: ILoaiTaiSan | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ loaiTaiSan }) => {
      this.loaiTaiSan = loaiTaiSan;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
