import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';
import { LoaiTaiSanComponent } from './list/loai-tai-san.component';
import { LoaiTaiSanDetailComponent } from './detail/loai-tai-san-detail.component';
import { LoaiTaiSanUpdateComponent } from './update/loai-tai-san-update.component';
import { LoaiTaiSanDeleteDialogComponent } from './delete/loai-tai-san-delete-dialog.component';
import { LoaiTaiSanRoutingModule } from './route/loai-tai-san-routing.module';

@NgModule({
  imports: [SharedModule, LoaiTaiSanRoutingModule],
  declarations: [LoaiTaiSanComponent, LoaiTaiSanDetailComponent, LoaiTaiSanUpdateComponent, LoaiTaiSanDeleteDialogComponent],
  entryComponents: [LoaiTaiSanDeleteDialogComponent],
})
export class LoaiTaiSanModule {}
