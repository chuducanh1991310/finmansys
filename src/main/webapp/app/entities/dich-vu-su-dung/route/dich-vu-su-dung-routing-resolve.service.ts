import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IDichVuSuDung, DichVuSuDung } from '../dich-vu-su-dung.model';
import { DichVuSuDungService } from '../service/dich-vu-su-dung.service';

@Injectable({ providedIn: 'root' })
export class DichVuSuDungRoutingResolveService implements Resolve<IDichVuSuDung> {
  constructor(protected service: DichVuSuDungService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IDichVuSuDung> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((dichVuSuDung: HttpResponse<DichVuSuDung>) => {
          if (dichVuSuDung.body) {
            return of(dichVuSuDung.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new DichVuSuDung());
  }
}
