import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IDichVuSuDung } from '../dich-vu-su-dung.model';
import { DichVuSuDungService } from '../service/dich-vu-su-dung.service';

@Component({
  templateUrl: './dich-vu-su-dung-delete-dialog.component.html',
})
export class DichVuSuDungDeleteDialogComponent {
  dichVuSuDung?: IDichVuSuDung;

  constructor(protected dichVuSuDungService: DichVuSuDungService, public activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.dichVuSuDungService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
