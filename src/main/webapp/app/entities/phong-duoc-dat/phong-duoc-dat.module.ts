import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';
import { PhongDuocDatComponent } from './list/phong-duoc-dat.component';
import { PhongDuocDatDetailComponent } from './detail/phong-duoc-dat-detail.component';
import { PhongDuocDatUpdateComponent } from './update/phong-duoc-dat-update.component';
import { PhongDuocDatDeleteDialogComponent } from './delete/phong-duoc-dat-delete-dialog.component';
import { PhongDuocDatRoutingModule } from './route/phong-duoc-dat-routing.module';

@NgModule({
  imports: [SharedModule, PhongDuocDatRoutingModule],
  declarations: [PhongDuocDatComponent, PhongDuocDatDetailComponent, PhongDuocDatUpdateComponent, PhongDuocDatDeleteDialogComponent],
  entryComponents: [PhongDuocDatDeleteDialogComponent],
})
export class PhongDuocDatModule {}
