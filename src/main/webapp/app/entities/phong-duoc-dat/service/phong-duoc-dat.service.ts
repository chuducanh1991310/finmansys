import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as dayjs from 'dayjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IPhongDuocDat, getPhongDuocDatIdentifier } from '../phong-duoc-dat.model';

export type EntityResponseType = HttpResponse<IPhongDuocDat>;
export type EntityArrayResponseType = HttpResponse<IPhongDuocDat[]>;

@Injectable({ providedIn: 'root' })
export class PhongDuocDatService {
  public resourceUrl = this.applicationConfigService.getEndpointFor('api/phong-duoc-dats');

  constructor(protected http: HttpClient, private applicationConfigService: ApplicationConfigService) {}

  create(phongDuocDat: IPhongDuocDat): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(phongDuocDat);
    return this.http
      .post<IPhongDuocDat>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(phongDuocDat: IPhongDuocDat): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(phongDuocDat);
    return this.http
      .put<IPhongDuocDat>(`${this.resourceUrl}/${getPhongDuocDatIdentifier(phongDuocDat) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(phongDuocDat: IPhongDuocDat): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(phongDuocDat);
    return this.http
      .patch<IPhongDuocDat>(`${this.resourceUrl}/${getPhongDuocDatIdentifier(phongDuocDat) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IPhongDuocDat>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IPhongDuocDat[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addPhongDuocDatToCollectionIfMissing(
    phongDuocDatCollection: IPhongDuocDat[],
    ...phongDuocDatsToCheck: (IPhongDuocDat | null | undefined)[]
  ): IPhongDuocDat[] {
    const phongDuocDats: IPhongDuocDat[] = phongDuocDatsToCheck.filter(isPresent);
    if (phongDuocDats.length > 0) {
      const phongDuocDatCollectionIdentifiers = phongDuocDatCollection.map(
        phongDuocDatItem => getPhongDuocDatIdentifier(phongDuocDatItem)!
      );
      const phongDuocDatsToAdd = phongDuocDats.filter(phongDuocDatItem => {
        const phongDuocDatIdentifier = getPhongDuocDatIdentifier(phongDuocDatItem);
        if (phongDuocDatIdentifier == null || phongDuocDatCollectionIdentifiers.includes(phongDuocDatIdentifier)) {
          return false;
        }
        phongDuocDatCollectionIdentifiers.push(phongDuocDatIdentifier);
        return true;
      });
      return [...phongDuocDatsToAdd, ...phongDuocDatCollection];
    }
    return phongDuocDatCollection;
  }

  protected convertDateFromClient(phongDuocDat: IPhongDuocDat): IPhongDuocDat {
    return Object.assign({}, phongDuocDat, {
      ngayDat: phongDuocDat.ngayDat?.isValid() ? phongDuocDat.ngayDat.toJSON() : undefined,
      checkIn: phongDuocDat.checkIn?.isValid() ? phongDuocDat.checkIn.toJSON() : undefined,
      checkOut: phongDuocDat.checkOut?.isValid() ? phongDuocDat.checkOut.toJSON() : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.ngayDat = res.body.ngayDat ? dayjs(res.body.ngayDat) : undefined;
      res.body.checkIn = res.body.checkIn ? dayjs(res.body.checkIn) : undefined;
      res.body.checkOut = res.body.checkOut ? dayjs(res.body.checkOut) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((phongDuocDat: IPhongDuocDat) => {
        phongDuocDat.ngayDat = phongDuocDat.ngayDat ? dayjs(phongDuocDat.ngayDat) : undefined;
        phongDuocDat.checkIn = phongDuocDat.checkIn ? dayjs(phongDuocDat.checkIn) : undefined;
        phongDuocDat.checkOut = phongDuocDat.checkOut ? dayjs(phongDuocDat.checkOut) : undefined;
      });
    }
    return res;
  }
}
