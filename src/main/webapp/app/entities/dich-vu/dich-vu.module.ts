import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';
import { DichVuComponent } from './list/dich-vu.component';
import { DichVuDetailComponent } from './detail/dich-vu-detail.component';
import { DichVuUpdateComponent } from './update/dich-vu-update.component';
import { DichVuDeleteDialogComponent } from './delete/dich-vu-delete-dialog.component';
import { DichVuRoutingModule } from './route/dich-vu-routing.module';

@NgModule({
  imports: [SharedModule, DichVuRoutingModule],
  declarations: [DichVuComponent, DichVuDetailComponent, DichVuUpdateComponent, DichVuDeleteDialogComponent],
  entryComponents: [DichVuDeleteDialogComponent],
})
export class DichVuModule {}
