jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { IDichVu, DichVu } from '../dich-vu.model';
import { DichVuService } from '../service/dich-vu.service';

import { DichVuRoutingResolveService } from './dich-vu-routing-resolve.service';

describe('Service Tests', () => {
  describe('DichVu routing resolve service', () => {
    let mockRouter: Router;
    let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
    let routingResolveService: DichVuRoutingResolveService;
    let service: DichVuService;
    let resultDichVu: IDichVu | undefined;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        providers: [Router, ActivatedRouteSnapshot],
      });
      mockRouter = TestBed.inject(Router);
      mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
      routingResolveService = TestBed.inject(DichVuRoutingResolveService);
      service = TestBed.inject(DichVuService);
      resultDichVu = undefined;
    });

    describe('resolve', () => {
      it('should return IDichVu returned by find', () => {
        // GIVEN
        service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultDichVu = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultDichVu).toEqual({ id: 123 });
      });

      it('should return new IDichVu if id is not provided', () => {
        // GIVEN
        service.find = jest.fn();
        mockActivatedRouteSnapshot.params = {};

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultDichVu = result;
        });

        // THEN
        expect(service.find).not.toBeCalled();
        expect(resultDichVu).toEqual(new DichVu());
      });

      it('should route to 404 page if data not found in server', () => {
        // GIVEN
        spyOn(service, 'find').and.returnValue(of(new HttpResponse({ body: null })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultDichVu = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultDichVu).toEqual(undefined);
        expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
      });
    });
  });
});
