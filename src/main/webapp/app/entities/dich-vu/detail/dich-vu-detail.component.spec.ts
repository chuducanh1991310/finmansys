import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { DichVuDetailComponent } from './dich-vu-detail.component';

describe('Component Tests', () => {
  describe('DichVu Management Detail Component', () => {
    let comp: DichVuDetailComponent;
    let fixture: ComponentFixture<DichVuDetailComponent>;

    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [DichVuDetailComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: { data: of({ dichVu: { id: 123 } }) },
          },
        ],
      })
        .overrideTemplate(DichVuDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(DichVuDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load dichVu on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.dichVu).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
