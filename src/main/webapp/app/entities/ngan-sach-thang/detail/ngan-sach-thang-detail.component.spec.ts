import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { NganSachThangDetailComponent } from './ngan-sach-thang-detail.component';

describe('Component Tests', () => {
  describe('NganSachThang Management Detail Component', () => {
    let comp: NganSachThangDetailComponent;
    let fixture: ComponentFixture<NganSachThangDetailComponent>;

    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [NganSachThangDetailComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: { data: of({ nganSachThang: { id: 123 } }) },
          },
        ],
      })
        .overrideTemplate(NganSachThangDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(NganSachThangDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load nganSachThang on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.nganSachThang).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
