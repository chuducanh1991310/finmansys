import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IPhongDat } from '../phong-dat.model';
import { PhongDatService } from '../service/phong-dat.service';

@Component({
  templateUrl: './phong-dat-delete-dialog.component.html',
})
export class PhongDatDeleteDialogComponent {
  phongDat?: IPhongDat;

  constructor(protected phongDatService: PhongDatService, public activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.phongDatService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
