import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as dayjs from 'dayjs';

import { DATE_TIME_FORMAT } from 'app/config/input.constants';
import { IPhongDat, PhongDat } from '../phong-dat.model';

import { PhongDatService } from './phong-dat.service';

describe('Service Tests', () => {
  describe('PhongDat Service', () => {
    let service: PhongDatService;
    let httpMock: HttpTestingController;
    let elemDefault: IPhongDat;
    let expectedResult: IPhongDat | IPhongDat[] | boolean | null;
    let currentDate: dayjs.Dayjs;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      service = TestBed.inject(PhongDatService);
      httpMock = TestBed.inject(HttpTestingController);
      currentDate = dayjs();

      elemDefault = {
        id: 0,
        ngayDat: currentDate,
      };
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            ngayDat: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a PhongDat', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            ngayDat: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            ngayDat: currentDate,
          },
          returnedFromService
        );

        service.create(new PhongDat()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a PhongDat', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            ngayDat: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            ngayDat: currentDate,
          },
          returnedFromService
        );

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should partial update a PhongDat', () => {
        const patchObject = Object.assign(
          {
            ngayDat: currentDate.format(DATE_TIME_FORMAT),
          },
          new PhongDat()
        );

        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign(
          {
            ngayDat: currentDate,
          },
          returnedFromService
        );

        service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PATCH' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of PhongDat', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            ngayDat: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            ngayDat: currentDate,
          },
          returnedFromService
        );

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a PhongDat', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });

      describe('addPhongDatToCollectionIfMissing', () => {
        it('should add a PhongDat to an empty array', () => {
          const phongDat: IPhongDat = { id: 123 };
          expectedResult = service.addPhongDatToCollectionIfMissing([], phongDat);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(phongDat);
        });

        it('should not add a PhongDat to an array that contains it', () => {
          const phongDat: IPhongDat = { id: 123 };
          const phongDatCollection: IPhongDat[] = [
            {
              ...phongDat,
            },
            { id: 456 },
          ];
          expectedResult = service.addPhongDatToCollectionIfMissing(phongDatCollection, phongDat);
          expect(expectedResult).toHaveLength(2);
        });

        it("should add a PhongDat to an array that doesn't contain it", () => {
          const phongDat: IPhongDat = { id: 123 };
          const phongDatCollection: IPhongDat[] = [{ id: 456 }];
          expectedResult = service.addPhongDatToCollectionIfMissing(phongDatCollection, phongDat);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(phongDat);
        });

        it('should add only unique PhongDat to an array', () => {
          const phongDatArray: IPhongDat[] = [{ id: 123 }, { id: 456 }, { id: 14390 }];
          const phongDatCollection: IPhongDat[] = [{ id: 123 }];
          expectedResult = service.addPhongDatToCollectionIfMissing(phongDatCollection, ...phongDatArray);
          expect(expectedResult).toHaveLength(3);
        });

        it('should accept varargs', () => {
          const phongDat: IPhongDat = { id: 123 };
          const phongDat2: IPhongDat = { id: 456 };
          expectedResult = service.addPhongDatToCollectionIfMissing([], phongDat, phongDat2);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(phongDat);
          expect(expectedResult).toContain(phongDat2);
        });

        it('should accept null and undefined values', () => {
          const phongDat: IPhongDat = { id: 123 };
          expectedResult = service.addPhongDatToCollectionIfMissing([], null, phongDat, undefined);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(phongDat);
        });
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
