import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { ITaiSanCD } from '../tai-san-cd.model';
import { TaiSanCDService } from '../service/tai-san-cd.service';

@Component({
  templateUrl: './tai-san-cd-delete-dialog.component.html',
})
export class TaiSanCDDeleteDialogComponent {
  taiSanCD?: ITaiSanCD;

  constructor(protected taiSanCDService: TaiSanCDService, public activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.taiSanCDService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
