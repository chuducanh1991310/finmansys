import { IKhachSan } from 'app/entities/khach-san/khach-san.model';
import { ILoaiTaiSan } from 'app/entities/loai-tai-san/loai-tai-san.model';

export interface ITaiSanCD {
  id?: number;
  ten?: string;
  donViTuoiTho?: string;
  thoiGianSuDung?: number;
  hinhAnhContentType?: string | null;
  hinhAnh?: string | null;
  khachSan?: IKhachSan;
  loaiTaiSan?: ILoaiTaiSan;
}

export class TaiSanCD implements ITaiSanCD {
  constructor(
    public id?: number,
    public ten?: string,
    public donViTuoiTho?: string,
    public thoiGianSuDung?: number,
    public hinhAnhContentType?: string | null,
    public hinhAnh?: string | null,
    public khachSan?: IKhachSan,
    public loaiTaiSan?: ILoaiTaiSan
  ) {}
}

export function getTaiSanCDIdentifier(taiSanCD: ITaiSanCD): number | undefined {
  return taiSanCD.id;
}
