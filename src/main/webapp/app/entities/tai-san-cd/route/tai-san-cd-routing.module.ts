import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { TaiSanCDComponent } from '../list/tai-san-cd.component';
import { TaiSanCDDetailComponent } from '../detail/tai-san-cd-detail.component';
import { TaiSanCDUpdateComponent } from '../update/tai-san-cd-update.component';
import { TaiSanCDRoutingResolveService } from './tai-san-cd-routing-resolve.service';

const taiSanCDRoute: Routes = [
  {
    path: '',
    component: TaiSanCDComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: TaiSanCDDetailComponent,
    resolve: {
      taiSanCD: TaiSanCDRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: TaiSanCDUpdateComponent,
    resolve: {
      taiSanCD: TaiSanCDRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: TaiSanCDUpdateComponent,
    resolve: {
      taiSanCD: TaiSanCDRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(taiSanCDRoute)],
  exports: [RouterModule],
})
export class TaiSanCDRoutingModule {}
