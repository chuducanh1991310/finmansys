import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { CongThucComponent } from '../list/cong-thuc.component';
import { CongThucDetailComponent } from '../detail/cong-thuc-detail.component';
import { CongThucUpdateComponent } from '../update/cong-thuc-update.component';
import { CongThucRoutingResolveService } from './cong-thuc-routing-resolve.service';

const congThucRoute: Routes = [
  {
    path: '',
    component: CongThucComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: CongThucDetailComponent,
    resolve: {
      congThuc: CongThucRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: CongThucUpdateComponent,
    resolve: {
      congThuc: CongThucRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: CongThucUpdateComponent,
    resolve: {
      congThuc: CongThucRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(congThucRoute)],
  exports: [RouterModule],
})
export class CongThucRoutingModule {}
