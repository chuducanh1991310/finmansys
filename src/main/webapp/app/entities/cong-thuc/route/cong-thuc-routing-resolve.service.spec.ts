jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { ICongThuc, CongThuc } from '../cong-thuc.model';
import { CongThucService } from '../service/cong-thuc.service';

import { CongThucRoutingResolveService } from './cong-thuc-routing-resolve.service';

describe('Service Tests', () => {
  describe('CongThuc routing resolve service', () => {
    let mockRouter: Router;
    let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
    let routingResolveService: CongThucRoutingResolveService;
    let service: CongThucService;
    let resultCongThuc: ICongThuc | undefined;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        providers: [Router, ActivatedRouteSnapshot],
      });
      mockRouter = TestBed.inject(Router);
      mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
      routingResolveService = TestBed.inject(CongThucRoutingResolveService);
      service = TestBed.inject(CongThucService);
      resultCongThuc = undefined;
    });

    describe('resolve', () => {
      it('should return ICongThuc returned by find', () => {
        // GIVEN
        service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultCongThuc = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultCongThuc).toEqual({ id: 123 });
      });

      it('should return new ICongThuc if id is not provided', () => {
        // GIVEN
        service.find = jest.fn();
        mockActivatedRouteSnapshot.params = {};

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultCongThuc = result;
        });

        // THEN
        expect(service.find).not.toBeCalled();
        expect(resultCongThuc).toEqual(new CongThuc());
      });

      it('should route to 404 page if data not found in server', () => {
        // GIVEN
        spyOn(service, 'find').and.returnValue(of(new HttpResponse({ body: null })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultCongThuc = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultCongThuc).toEqual(undefined);
        expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
      });
    });
  });
});
