import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { ICongThuc } from '../cong-thuc.model';
import { CongThucService } from '../service/cong-thuc.service';

@Component({
  templateUrl: './cong-thuc-delete-dialog.component.html',
})
export class CongThucDeleteDialogComponent {
  congThuc?: ICongThuc;

  constructor(protected congThucService: CongThucService, public activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.congThucService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
