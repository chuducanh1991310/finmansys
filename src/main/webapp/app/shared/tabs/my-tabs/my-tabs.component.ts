import { AfterContentInit, Component, ContentChildren, QueryList } from '@angular/core';
import { MyTabComponent } from 'app/shared/tabs/my-tab/my-tab.component';

@Component({
  selector: 'jhi-my-tabs',
  templateUrl: './my-tabs.component.html',
  styleUrls: ['./my-tabs.component.scss'],
})
export class MyTabsComponent implements AfterContentInit {
  @ContentChildren(MyTabComponent) tabs: QueryList<MyTabComponent> | undefined;

  // contentChildren are set
  ngAfterContentInit(): void {
    // get all active tabs
    if (this.tabs) {
      const activeTabs = this.tabs.filter(tab => tab.active);

      // if there is no active tab set, activate the first
      if (activeTabs.length === 0) {
        this.selectTab(this.tabs.first);
      }
    }
  }

  selectTab(tabComponent: MyTabComponent): void {
    // deactivate all tabs
    if (this.tabs) {
      this.tabs.toArray().forEach(tab => (tab.active = false));

      // activate the tab the user has clicked on.
      tabComponent.active = true;
    }
  }
}
